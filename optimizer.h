#ifndef OPTIMIZER_H_
#define OPTIMIZER_H_

#define COST_INFO(X) struct cost_info X = { .r = 0, .t = 0, .l = 0, .m = 0, .a = 0, .f = 0 };

struct case_info {
	int num_terms;
	float *p_vals;
};

struct cost_info {
	int r;
	int t;
	int l;
	int m;
	int a;
	int f;
};

struct subset {
	int n;
	int b;
	float p;
	float c;
	struct subset *L;
	struct subset *R;
};

void get_plan(void);
void and_cost(struct subset *sub);
void parse_config_file(FILE *config_file);
void parse_query_file(FILE *query_file);
void free_cases(void);
void and_plans(struct subset subsets[], int num_subsets);
int c_metric_test(struct subset *sub1, struct subset *sub2);
int d_metric_test(struct subset *sub1, struct subset *sub2);
struct subset *leftmost_child(struct subset *parent_sub);
int gen_subsets_helper(float *p_vals, float subset[], int subset_idx, struct subset subsets[], int subsets_idx, int start, int end, int k);
void combined_plans(struct subset subsets[], int num_subsets);
float combined_cost(struct subset *s1, struct subset *s2);
void init_subset(struct subset *sub);
int get_sub_idx(struct subset subsets[], int num_subsets, struct subset *sub);
void derive_optimal_plan(struct subset subsets[], int num_subsets,  int num_terms);
void print_terms(int idx, int num_terms);
void replace_subset_union(struct subset subsets[], struct subset *sL, struct subset *sR, int sL_idx, int sR_idx, float new_cost);
int get_num_terms(int bitmap);
float fcost(struct subset *sub);
void subset_dfs(struct subset *curr_subset, struct subset *and_terms[], int *and_term_idx);

/********************************* DEBUGGING FUNCTIONS **********************************/

void print_cases(void);
void print_subsets(struct subset subsets[], int num_subsets);
void print_subset(struct subset subsets[]);

#endif /* OPTIMIZER_H_ */
