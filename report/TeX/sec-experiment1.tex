\par
The goal of the first experiment was to determine how the algorithm's plans and costs estimates change as a function of increasing/decreasing selectivity, as well as to determine how performance changes as a function of increasing/decreasing selectivity. We had the optimization algorithm produce plans for 7 cases with different selectivities (the plans can be found in the Experiment 1 section and optimizer output subsection of the Appendix). Each case has 4 basic terms and is homogenous (all terms have the same selectivities). The cases are shown below:
\begin{verbatim}
Case 1
0.0 0.0 0.0 0.0

Case 2
0.1 0.1 0.1 0.1

Case 3 
0.3 0.3 0.3 0.3 

Case 4
0.5 0.5 0.5 0.5 

Case 5
0.7 0.7 0.7 0.7 

Case 6
0.9 0.9 0.9 0.9 

Case7
1.0 1.0 1.0 1.0 
\end{verbatim}
The individual selectivities of the basic terms of a case and the overall selectivity of a case are both important to determining the optimal algorithm for the case. Therefore, both should be considered when evaluating the associated plans. The overall selectivity of a case is given by the product of all its selectivities, i.e.\\
\[p_{1} * p_{2} * ... p_{n},\ \ where\ p_{i}\ is\ the\ selectivity\ of\ basic\ term\ i\ and n\ is\ the\ number\ of\ terms\]\\
It is important to note that the the tool used profile the plans refers to overall selectivity as determined above as theoretical selectivity and refers to the actual selectivity (how often the plan evaluates to true) as overall selectivity. Going forward, we will adhere to this terminology. The theoretical selectivities of the cases are as follows:\\
\begin{verbatim}
Case 1: 0.0, Case 2: 0.0001, Case 3: 0.0081, 
Case 4: 0.063, Case 5: 0.24, Case 6: 0.67, Case 7: 1.0

\end{verbatim}
All of the plans produced by the optimization algorithm were profiled as discussed in the introduction (the raw output of the profiler can be found in the Experiment 1 section and branch\_mispred (raw) subsection of the Appendix, additionally a tabulation of the output can be found in the Experiment 1 section and branch\_mispred (tabular) subsection).\\
\par
Figure 1 shows empirical CPU cycles per record also called empirical cost versus theoretical CPU cycles per record also called theoretical cost (empirical cost is the cost determined by the profiler and the theoretical cost is the cost determined by the optimization algorithm). The design of the optimization algorithm dictates that it should be producing accurate cost estimates if it is given accurate parameters. If it underestimates or overestimates the costs of plans in a biased manner it may not be choosing the optimal plan. As can be seen in figure 1, the theoretical and empirical costs of the plans generally increases as selectivity increases. This is logical because increasing selectivity, means that the functions are less selective and evaluate to true more often. Every time a branching plan evaluates to true it has to execute additional code, which has an associated cost of course. The optimization algorithm over-estimated the cost of the plans it produces for cases 1 - 6. The exception being case 2, whose estimated cost was 9.55, which is nearly identical to empirical cost of 9.07. The optimization algorithm greatly underestimated the cost of the plan associated with case 7. The theoretical cost of case 7 was 25, while the empirical cost was 37.02. The theoretical costs of cases 5, 6, and 7, which are all full no-branch plans, are the same (25.0), but their empirical costs are not. In fact, their empirical costs increase with increasing selectivity. We briefly digress from our analysis below to provide background information necessary to explain the aforementioned observations and trends in the theoretical and empirical costs.\\\\ 
\begin{center}
\includegraphics[scale=0.5]{images/figure_1.png}\\
\footnotesize
figure 1. Cost\\
\normalsize
\end{center}
\par
The theoretical underpinnings of the optimization algorithm ensure that it produces the optimal plan for the parameters it is given and the parameters it is given represent the most important costs associated with executing a plan (cost of accessing an array element, cost of executing an if test, cost of performing a logical and, cost of branch misprediction, cost of writing an answer, and cost of applying a function). The reason that the optimization algorithm does not always produce an accurate estimation of cost most likely the result of inaccurate parameters. Some of the parameters, such as the cost of branch misprediction, are averages and therefore do not always accurately reflect their true cost when the plan is being executing. For example, if the branch misprediction penalty parameter was lower than it should be then the optimization algorithm may favor branching plans, especially \&\&-plans more than it should. Conversely, if the branch misprediction penalty parameter was higher than expected the optimization algorithm may favor \&-plans and no-branch plans more than it should.\\
\par
Returning to our analysis, it is likely that the branch misprediction penalty parameter, and potentially other parameters, was higher than it should be. This would explain why the optimization algorithm over-estimated the cost of many of of the branching plans. The optimization algorithm calculates the cost of all no-branch plans with the same number of terms to be the same. But, given that the empirical costs of the no-branch plans (cases 5, 6, and 7) increase with increasing selectivity and the cost of plan 7 is greatly underestimated by optimization algorithm, there may be a a flaw in its cost model. One hypothesis is that the processor is optimized to reduce the cost of adding a zero to an integer (i.e. instead of adding a zero it does nothing). Note that this is only a hypothesis and was unable to be clearly substantiated through Intel documentation. Recall from above that the higher the selectivity of a plan the more likely it is to evaluate to one. Conversely, the lower the selectivity of a plan the more likely it is evaluate to zero. If the processor is optimized to handle zero adds, we would expect the cost of the no-branch plans with lower selectivity to have a smaller empirical cost and plans with higher selectivity to have a larger empirical cost, which is exactly what the data showed.\\
\par
Figure 2 shows branch misses per record versus theoretical selectivity. It can be seen that the branch misses are low for case 1 where the individual selectivity is 0.1 and the theoretical selectivity is 0. The branch misses rise for cases 2 and 3, peaking at case 4, where the individual selectivity is 0.5 and the theoretical selectivity is 0.063. The branch misses are extremely low for cases 5, 6, and 7, which are the no-branch plans. The fact that the branch misses is highest for the case where the individual selectivity is 0.5 is logical. Of course the optimizer will not choose a plan that uses \&\& on a basic term with selectivity of 0.5 because that is the worse case for branch prediction, but the optimal plan for this case still leads to a branch on a term (\&-term) that has a selectivity relatively close 0.5, 0.25 (see the Appendix for the plan). Because a term with a selectivity of 0.25 prevents the inner code from being executed relatively often, it is desirable over using the no-branch optimization on an \&-term consisting of all basic terms.\\
\begin{center}
\includegraphics[scale=0.5]{images/figure_2.png}\\
\footnotesize
figure 2. Branch Misses
\normalsize
\end{center}
\par
Figure 3 shows processing time per record versus theoretical selectivity. The graph is somewhat bi-modal, although the processing time is much greater for the extremes closer to 1 than they are for the extremes closer to 0. The peak around 0.05 is due to case 4. It likely has a longer processing time compared to the cases around it due to its relatively high number of branch misses. The peak after about 0.7 is due to the no-branch plans. The reason that their processing time increases with increasing selectivity is of course directly related to the reason their cost increases with increasing selectivity. As we hypothesized above it may be due to the fact that no-branch plans with higher selectivities are more likely to evaluate to one than plans with lower selectivities.\\
\begin{center}
\includegraphics[scale=0.5]{images/figure_3.png}\\
\footnotesize
figure 3. Processing Time\\
\normalsize
\end{center}