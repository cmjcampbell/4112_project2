	.file	"branch_mispred.c"
	.text
.Ltext0:
	.p2align 4,,15
	.globl	perf_event_open
	.type	perf_event_open, @function
perf_event_open:
.LFB89:
	.file 1 "branch_mispred.c"
	.loc 1 39 0
	.cfi_startproc
.LVL0:
	subq	$8, %rsp
.LCFI0:
	.cfi_def_cfa_offset 16
	.loc 1 40 0
	movq	%r8, %r9
	xorl	%eax, %eax
	movl	%ecx, %r8d
.LVL1:
	movl	%edx, %ecx
.LVL2:
	movl	%esi, %edx
.LVL3:
	movq	%rdi, %rsi
.LVL4:
	movl	$298, %edi
.LVL5:
	call	syscall
.LVL6:
	.loc 1 41 0
	addq	$8, %rsp
.LCFI1:
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE89:
	.size	perf_event_open, .-perf_event_open
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"Failed to get time stamp"
	.text
	.p2align 4,,15
	.globl	get_timestamp
	.type	get_timestamp, @function
get_timestamp:
.LFB91:
	.loc 1 211 0
	.cfi_startproc
.LVL7:
	subq	$40, %rsp
.LCFI2:
	.cfi_def_cfa_offset 48
	.loc 1 214 0
	xorl	%esi, %esi
	leaq	16(%rsp), %rdi
	.loc 1 211 0
	movsd	%xmm0, 8(%rsp)
	.loc 1 214 0
	call	gettimeofday
.LVL8:
	testl	%eax, %eax
	jne	.L3
	.loc 1 215 0
	cvtsi2sdq	24(%rsp), %xmm2
	mulsd	.LC1(%rip), %xmm2
	cvtsi2sdq	16(%rsp), %xmm1
	addsd	%xmm2, %xmm1
	subsd	8(%rsp), %xmm1
	.loc 1 220 0
	addq	$40, %rsp
	.cfi_remember_state
.LCFI3:
	.cfi_def_cfa_offset 8
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L3:
.LCFI4:
	.cfi_restore_state
.LVL9:
.LBB56:
.LBB57:
.LBB58:
.LBB59:
	.file 2 "/usr/include/x86_64-linux-gnu/bits/stdio2.h"
	.loc 2 105 0
	movl	$.LC2, %esi
	movl	$1, %edi
	xorl	%eax, %eax
	call	__printf_chk
	movsd	.LC0(%rip), %xmm1
.LBE59:
.LBE58:
.LBE57:
.LBE56:
	.loc 1 220 0
	addq	$40, %rsp
.LCFI5:
	.cfi_def_cfa_offset 8
	movapd	%xmm1, %xmm0
	ret
	.cfi_endproc
.LFE91:
	.size	get_timestamp, .-get_timestamp
	.p2align 4,,15
	.globl	createData
	.type	createData, @function
createData:
.LFB92:
	.loc 1 228 0
	.cfi_startproc
.LVL10:
	pushq	%r12
.LCFI6:
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	movq	%rdi, %r12
	pushq	%rbp
.LCFI7:
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
	leaq	3000(%rdi), %rbp
	pushq	%rbx
.LCFI8:
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
.LCFI9:
	.cfi_def_cfa_offset 48
	.loc 1 228 0
	movsd	%xmm0, 8(%rsp)
.LVL11:
	.p2align 4,,10
	.p2align 3
.L7:
	.loc 1 233 0
	call	rand
.LVL12:
	cvtsi2sd	%eax, %xmm1
	.loc 1 235 0
	movsd	8(%rsp), %xmm0
	.loc 1 233 0
	mulsd	.LC3(%rip), %xmm1
	.loc 1 235 0
	ucomisd	%xmm1, %xmm0
	.loc 1 233 0
	seta	(%rbx)
	.loc 1 239 0
	addq	$1, %rbx
.LVL13:
	.loc 1 232 0
	cmpq	%rbp, %rbx
	jne	.L7
	.loc 1 241 0
	movb	$0, 3000(%r12)
	.loc 1 243 0
	addq	$16, %rsp
.LCFI10:
	.cfi_def_cfa_offset 32
	movq	%rbx, %rax
.LVL14:
	popq	%rbx
.LCFI11:
	.cfi_def_cfa_offset 24
.LVL15:
	popq	%rbp
.LCFI12:
	.cfi_def_cfa_offset 16
	popq	%r12
.LCFI13:
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE92:
	.size	createData, .-createData
	.p2align 4,,15
	.globl	createOffsets
	.type	createOffsets, @function
createOffsets:
.LFB93:
	.loc 1 251 0
	.cfi_startproc
.LVL16:
	pushq	%rbp
.LCFI14:
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rbp
	pushq	%rbx
.LCFI15:
	.cfi_def_cfa_offset 24
	.cfi_offset 3, -24
	xorl	%ebx, %ebx
	subq	$8, %rsp
.LCFI16:
	.cfi_def_cfa_offset 32
.LVL17:
	.p2align 4,,10
	.p2align 3
.L10:
.LBB60:
	.loc 1 254 0 discriminator 2
	call	rand
.LVL18:
	cvtsi2sd	%eax, %xmm0
	mulsd	.LC3(%rip), %xmm0
	.loc 1 255 0 discriminator 2
	mulsd	.LC4(%rip), %xmm0
	cvttsd2si	%xmm0, %eax
.LVL19:
	movl	%eax, 0(%rbp,%rbx)
	addq	$4, %rbx
.LBE60:
	.loc 1 253 0 discriminator 2
	cmpq	$400000000, %rbx
	jne	.L10
	.loc 1 258 0
	addq	$8, %rsp
.LCFI17:
	.cfi_def_cfa_offset 24
	xorl	%eax, %eax
	popq	%rbx
.LCFI18:
	.cfi_def_cfa_offset 16
	popq	%rbp
.LCFI19:
	.cfi_def_cfa_offset 8
.LVL20:
	ret
	.cfi_endproc
.LFE93:
	.size	createOffsets, .-createOffsets
	.section	.rodata.str1.1
.LC5:
	.string	"Opening performance counter"
.LC6:
	.string	"argument error!"
.LC7:
	.string	"Loop start!"
.LC9:
	.string	"branch_mispred.c"
.LC10:
	.string	"rc"
.LC11:
	.string	"Loop stop!"
.LC12:
	.string	"Elapsed time: %.9lf seconds\n"
.LC13:
	.string	"CPU Cycles:           %lu \n"
.LC14:
	.string	"Instructions:         %lu \n"
.LC15:
	.string	"IPC:                  %lf\n"
.LC16:
	.string	"Branch misses:        %lu \n"
.LC17:
	.string	"Branch instructions:  %lu \n"
.LC19:
	.string	"Branch mispred. rate: %lf%%\n"
.LC21:
	.string	"overall selectivity = %10.9f\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC22:
	.string	"theoretical selectivity = %10.9f\n"
	.section	.rodata.str1.1
.LC23:
	.string	"w"
.LC24:
	.string	"/dev/null"
.LC25:
	.string	"%d "
	.section	.text.startup,"ax",@progbits
	.p2align 4,,15
	.globl	main
	.type	main, @function
main:
.LFB90:
	.loc 1 52 0
	.cfi_startproc
.LVL21:
	pushq	%r15
.LCFI20:
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	pushq	%r14
.LCFI21:
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	pushq	%r13
.LCFI22:
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	movl	%edi, %r13d
	pushq	%r12
.LCFI23:
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbp
.LCFI24:
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	pushq	%rbx
.LCFI25:
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	subq	$3144, %rsp
.LCFI26:
	.cfi_def_cfa_offset 3200
	.loc 1 64 0
	movl	$0, attr(%rip)
	.loc 1 66 0
	andb	$-2, attr+40(%rip)
	.loc 1 52 0
	movq	%fs:40, %rax
	movq	%rax, 3128(%rsp)
	xorl	%eax, %eax
.LVL22:
	.loc 1 65 0
	movq	$0, attr+8(%rip)
	.loc 1 67 0
	call	getpid
.LVL23:
	movl	$-1, %ecx
	xorl	%r8d, %r8d
	movl	%eax, %esi
	movl	%ecx, %edx
	movl	$attr, %edi
	call	perf_event_open
	.loc 1 68 0
	testl	%eax, %eax
	.loc 1 67 0
	movl	%eax, %ebp
.LVL24:
	.loc 1 68 0
	js	.L45
.LVL25:
.L13:
	.loc 1 74 0
	andb	$-2, attr+112(%rip)
	.loc 1 72 0
	movl	$0, attr+72(%rip)
	.loc 1 73 0
	movq	$5, attr+80(%rip)
	.loc 1 75 0
	call	getpid
	movl	$-1, %ecx
	xorl	%r8d, %r8d
	movl	%eax, %esi
	movl	%ecx, %edx
	movl	$attr+72, %edi
	call	perf_event_open
	.loc 1 76 0
	testl	%eax, %eax
	.loc 1 75 0
	movl	%eax, %ebx
.LVL26:
	.loc 1 76 0
	js	.L46
.LVL27:
.L14:
	.loc 1 82 0
	andb	$-2, attr+184(%rip)
	.loc 1 80 0
	movl	$0, attr+144(%rip)
	.loc 1 81 0
	movq	$4, attr+152(%rip)
	.loc 1 83 0
	call	getpid
	movl	$-1, %ecx
	xorl	%r8d, %r8d
	movl	%eax, %esi
	movl	%ecx, %edx
	movl	$attr+144, %edi
	call	perf_event_open
	.loc 1 84 0
	testl	%eax, %eax
	.loc 1 83 0
	movl	%eax, %r15d
.LVL28:
	.loc 1 84 0
	js	.L47
.LVL29:
.L15:
	.loc 1 90 0
	andb	$-2, attr+256(%rip)
	.loc 1 88 0
	movl	$0, attr+216(%rip)
	.loc 1 89 0
	movq	$1, attr+224(%rip)
	.loc 1 91 0
	call	getpid
	movl	$-1, %ecx
	xorl	%r8d, %r8d
	movl	%eax, %esi
	movl	%ecx, %edx
	movl	$attr+216, %edi
	call	perf_event_open
	.loc 1 92 0
	testl	%eax, %eax
	.loc 1 91 0
	movl	%eax, 28(%rsp)
.LVL30:
	.loc 1 92 0
	js	.L48
.LVL31:
.L16:
	.loc 1 97 0
	cmpl	$2, %r13d
	jne	.L49
.LVL32:
.LBB61:
.LBB62:
	.file 3 "/usr/include/stdlib.h"
	.loc 3 281 0
	movq	8(%r12), %rdi
	xorl	%esi, %esi
	call	strtod
.LVL33:
.LBE62:
.LBE61:
	.loc 1 106 0
	movl	$400000000, %edi
.LBB64:
.LBB63:
	.loc 3 281 0
	movsd	%xmm0, 32(%rsp)
.LBE63:
.LBE64:
	.loc 1 106 0
	call	malloc
	.loc 1 107 0
	movl	$400000000, %edi
	.loc 1 106 0
	movq	%rax, %r14
.LVL34:
	.loc 1 107 0
	call	malloc
.LVL35:
	.loc 1 110 0
	xorl	%edi, %edi
	.loc 1 107 0
	movq	%rax, %r13
.LVL36:
	.loc 1 110 0
	call	time
.LVL37:
	.loc 1 111 0
	movl	%eax, %edi
	call	srand
.LVL38:
	.loc 1 114 0
	movsd	32(%rsp), %xmm0
	leaq	112(%rsp), %rdi
	call	createData
	.loc 1 117 0
	movq	%r14, %rdi
	call	createOffsets
.LVL39:
	.loc 2 105 0
	movl	$.LC7, %edi
	call	puts
	.loc 1 121 0
	movq	stdout(%rip), %rdi
	call	fflush
	.loc 1 122 0
	xorpd	%xmm0, %xmm0
	call	get_timestamp
	movsd	%xmm0, 40(%rsp)
.LVL40:
	.loc 1 125 0
#APP
# 125 "branch_mispred.c" 1
	nop;
# 0 "" 2
.LVL41:
#NO_APP
.LBB65:
.LBB66:
	.file 4 "/usr/include/x86_64-linux-gnu/bits/unistd.h"
	.loc 4 45 0
	leaq	48(%rsp), %rsi
	movl	$8, %edx
	movl	%ebp, %edi
	call	read
.LVL42:
.LBE66:
.LBE65:
	.loc 1 126 0
	testl	%eax, %eax
	je	.L50
.LVL43:
.LBB67:
.LBB68:
	.loc 1 127 0
	leaq	56(%rsp), %rsi
	.loc 4 45 0
	movl	$8, %edx
	movl	%ebx, %edi
	call	read
.LVL44:
.LBE68:
.LBE67:
	.loc 1 127 0
	testl	%eax, %eax
	je	.L51
.LVL45:
.LBB69:
.LBB70:
	.loc 1 128 0
	leaq	64(%rsp), %rsi
	.loc 4 45 0
	movl	$8, %edx
	movl	%r15d, %edi
	call	read
.LVL46:
.LBE70:
.LBE69:
	.loc 1 128 0
	testl	%eax, %eax
	je	.L52
.LVL47:
.LBB71:
.LBB72:
	.loc 4 45 0
	movl	28(%rsp), %edi
	.loc 1 129 0
	leaq	72(%rsp), %rsi
	.loc 4 45 0
	movl	$8, %edx
	call	read
.LVL48:
.LBE72:
.LBE71:
	.loc 1 129 0
	testl	%eax, %eax
	je	.L53
	.loc 1 130 0
#APP
# 130 "branch_mispred.c" 1
	nop;
# 0 "" 2
#NO_APP
	xorl	%eax, %eax
.LVL49:
	.loc 1 55 0
	xorl	%r12d, %r12d
.LVL50:
	.p2align 4,,10
	.p2align 3
.L23:
	.loc 1 145 0
	movslq	%r12d, %rdx
	movl	%eax, 0(%r13,%rdx,4)
	.loc 1 146 0
	movslq	(%r14,%rax,4), %rdx
	addq	$1, %rax
	movsbl	112(%rsp,%rdx), %edx
	addl	%edx, %r12d
.LVL51:
	.loc 1 136 0
	cmpq	$100000000, %rax
	jne	.L23
	.loc 1 162 0
#APP
# 162 "branch_mispred.c" 1
	nop;
# 0 "" 2
.LVL52:
#NO_APP
.LBB73:
.LBB74:
	.loc 4 45 0
	leaq	80(%rsp), %rsi
	movl	$8, %edx
	movl	%ebp, %edi
	call	read
.LVL53:
.LBE74:
.LBE73:
	.loc 1 163 0
	testl	%eax, %eax
	je	.L54
.LVL54:
.LBB75:
.LBB76:
	.loc 1 164 0
	leaq	88(%rsp), %rsi
	.loc 4 45 0
	movl	$8, %edx
	movl	%ebx, %edi
	call	read
.LVL55:
.LBE76:
.LBE75:
	.loc 1 164 0
	testl	%eax, %eax
	je	.L55
.LVL56:
.LBB77:
.LBB78:
	.loc 1 165 0
	leaq	96(%rsp), %rsi
	.loc 4 45 0
	movl	$8, %edx
	movl	%r15d, %edi
	call	read
.LVL57:
.LBE78:
.LBE77:
	.loc 1 165 0
	testl	%eax, %eax
	je	.L56
.LVL58:
.LBB79:
.LBB80:
	.loc 4 45 0
	movl	28(%rsp), %edi
	.loc 1 166 0
	leaq	104(%rsp), %rsi
	.loc 4 45 0
	movl	$8, %edx
	call	read
.LVL59:
.LBE80:
.LBE79:
	.loc 1 166 0
	testl	%eax, %eax
	je	.L57
	.loc 1 168 0
#APP
# 168 "branch_mispred.c" 1
	nop;
# 0 "" 2
	.loc 1 171 0
#NO_APP
	movl	%ebp, %edi
	call	close
.LVL60:
	.loc 1 172 0
	movl	%ebx, %edi
	call	close
	.loc 1 173 0
	movl	%r15d, %edi
	call	close
	.loc 1 174 0
	movl	28(%rsp), %edi
	call	close
	.loc 1 176 0
	movsd	40(%rsp), %xmm0
	call	get_timestamp
.LVL61:
	.loc 2 105 0
	movl	$.LC11, %edi
	movsd	%xmm0, (%rsp)
	call	puts
.LVL62:
.LBB81:
.LBB82:
	movsd	(%rsp), %xmm0
	movl	$.LC12, %esi
	movl	$1, %edi
	movl	$1, %eax
	call	__printf_chk
.LVL63:
.LBE82:
.LBE81:
	.loc 1 180 0
	movq	80(%rsp), %rdx
	subq	48(%rsp), %rdx
.LBB83:
.LBB84:
	.loc 2 105 0
	movl	$.LC13, %esi
	movl	$1, %edi
	xorl	%eax, %eax
	call	__printf_chk
.LVL64:
.LBE84:
.LBE83:
	.loc 1 181 0
	movq	104(%rsp), %rdx
	subq	72(%rsp), %rdx
.LBB85:
.LBB86:
	.loc 2 105 0
	xorl	%eax, %eax
	movl	$.LC14, %esi
	movl	$1, %edi
	call	__printf_chk
.LVL65:
.LBE86:
.LBE85:
	.loc 1 182 0
	movq	104(%rsp), %rax
	testq	%rax, %rax
	js	.L28
	cvtsi2sdq	%rax, %xmm0
.L29:
	movq	72(%rsp), %rax
	testq	%rax, %rax
	js	.L30
	cvtsi2sdq	%rax, %xmm1
.L31:
	movq	80(%rsp), %rax
	subq	48(%rsp), %rax
	subsd	%xmm1, %xmm0
	js	.L32
	cvtsi2sdq	%rax, %xmm1
.L33:
	divsd	%xmm1, %xmm0
.LBB87:
.LBB88:
	.loc 2 105 0
	movl	$.LC15, %esi
	movl	$1, %edi
	movl	$1, %eax
	call	__printf_chk
.LVL66:
.LBE88:
.LBE87:
	.loc 1 183 0
	movq	88(%rsp), %rdx
	subq	56(%rsp), %rdx
.LBB89:
.LBB90:
	.loc 2 105 0
	movl	$.LC16, %esi
	movl	$1, %edi
	xorl	%eax, %eax
	call	__printf_chk
.LVL67:
.LBE90:
.LBE89:
	.loc 1 184 0
	movq	96(%rsp), %rdx
	subq	64(%rsp), %rdx
.LBB91:
.LBB92:
	.loc 2 105 0
	xorl	%eax, %eax
	movl	$.LC17, %esi
	movl	$1, %edi
	call	__printf_chk
.LVL68:
.LBE92:
.LBE91:
	.loc 1 185 0
	movq	88(%rsp), %rax
	testq	%rax, %rax
	js	.L34
	cvtsi2sdq	%rax, %xmm0
.L35:
	movq	56(%rsp), %rax
	testq	%rax, %rax
	js	.L36
	cvtsi2sdq	%rax, %xmm1
.L37:
	subsd	%xmm1, %xmm0
	movq	96(%rsp), %rax
	subq	64(%rsp), %rax
	mulsd	.LC18(%rip), %xmm0
	js	.L38
	cvtsi2sdq	%rax, %xmm1
.L39:
	divsd	%xmm1, %xmm0
.LBB93:
.LBB94:
	.loc 2 105 0
	movl	$.LC19, %esi
	movl	$1, %edi
	movl	$1, %eax
	call	__printf_chk
.LVL69:
	movl	$10, %edi
	call	putchar
.LVL70:
.LBE94:
.LBE93:
	.loc 1 187 0
	cvtsi2sd	%r12d, %xmm0
.LBB95:
.LBB96:
	.loc 2 105 0
	movl	$.LC21, %esi
	movl	$1, %edi
	movl	$1, %eax
.LBE96:
.LBE95:
	.loc 1 187 0
	divsd	.LC20(%rip), %xmm0
.LBB98:
.LBB97:
	.loc 2 105 0
	call	__printf_chk
.LVL71:
.LBE97:
.LBE98:
.LBB99:
.LBB100:
	movsd	32(%rsp), %xmm0
	movl	$.LC22, %esi
	movl	$1, %edi
	movl	$1, %eax
	call	__printf_chk
.LBE100:
.LBE99:
	.loc 1 192 0
	movl	$.LC23, %esi
	movl	$.LC24, %edi
	call	fopen
	.loc 1 193 0
	testl	%r12d, %r12d
	.loc 1 192 0
	movq	%rax, %rbp
.LVL72:
	.loc 1 193 0
	jle	.L40
	xorl	%ebx, %ebx
.LVL73:
	.p2align 4,,10
	.p2align 3
.L41:
.LBB101:
.LBB102:
	.loc 2 98 0 discriminator 2
	movl	0(%r13,%rbx,4), %ecx
	xorl	%eax, %eax
	movl	$.LC25, %edx
	movl	$1, %esi
	movq	%rbp, %rdi
	addq	$1, %rbx
	call	__fprintf_chk
.LBE102:
.LBE101:
	.loc 1 193 0 discriminator 2
	cmpl	%ebx, %r12d
	jg	.L41
.LVL74:
.L40:
	.loc 1 196 0
	movq	%rbp, %rdi
	call	fclose
	.loc 1 199 0
	movq	%r14, %rdi
	call	free
	.loc 1 201 0
	movq	%r13, %rdi
	call	free
	.loc 1 203 0
	xorl	%eax, %eax
.LVL75:
.L18:
	.loc 1 204 0
	movq	3128(%rsp), %rdx
	xorq	%fs:40, %rdx
	jne	.L58
	addq	$3144, %rsp
	.cfi_remember_state
.LCFI27:
	.cfi_def_cfa_offset 56
	popq	%rbx
.LCFI28:
	.cfi_def_cfa_offset 48
	popq	%rbp
.LCFI29:
	.cfi_def_cfa_offset 40
	popq	%r12
.LCFI30:
	.cfi_def_cfa_offset 32
	popq	%r13
.LCFI31:
	.cfi_def_cfa_offset 24
	popq	%r14
.LCFI32:
	.cfi_def_cfa_offset 16
	popq	%r15
.LCFI33:
	.cfi_def_cfa_offset 8
.LVL76:
	ret
.LVL77:
.L28:
.LCFI34:
	.cfi_restore_state
	.loc 1 182 0
	movq	%rax, %rdx
	andl	$1, %eax
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L29
.L30:
	movq	%rax, %rdx
	andl	$1, %eax
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L31
.L32:
	movq	%rax, %rdx
	andl	$1, %eax
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L33
.LVL78:
.L34:
	.loc 1 185 0
	movq	%rax, %rdx
	andl	$1, %eax
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L35
.L36:
	movq	%rax, %rdx
	andl	$1, %eax
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L37
.L38:
	movq	%rax, %rdx
	andl	$1, %eax
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L39
.LVL79:
.L46:
	.loc 1 77 0
	movl	$.LC5, %edi
	call	perror
	jmp	.L14
.LVL80:
.L48:
	.loc 1 93 0
	movl	$.LC5, %edi
	call	perror
.LVL81:
	.p2align 4,,3
	jmp	.L16
.LVL82:
.L45:
	.loc 1 69 0
	movl	$.LC5, %edi
	call	perror
	.p2align 4,,3
	jmp	.L13
.LVL83:
.L47:
	.loc 1 85 0
	movl	$.LC5, %edi
	call	perror
.LVL84:
	.p2align 4,,3
	jmp	.L15
.LVL85:
.L49:
	.loc 2 105 0
	movl	$.LC6, %edi
	call	puts
	.loc 1 99 0
	orl	$-1, %eax
	jmp	.L18
.LVL86:
.L51:
	.loc 1 127 0 discriminator 1
	movl	$__PRETTY_FUNCTION__.4757, %ecx
	movl	$127, %edx
	movl	$.LC9, %esi
	movl	$.LC10, %edi
	call	__assert_fail
.LVL87:
.L55:
	.loc 1 164 0 discriminator 1
	movl	$__PRETTY_FUNCTION__.4757, %ecx
	movl	$164, %edx
	movl	$.LC9, %esi
	movl	$.LC10, %edi
	call	__assert_fail
.LVL88:
.L57:
	.loc 1 166 0 discriminator 1
	movl	$__PRETTY_FUNCTION__.4757, %ecx
	movl	$166, %edx
	movl	$.LC9, %esi
	movl	$.LC10, %edi
	call	__assert_fail
.LVL89:
.L58:
	.loc 1 204 0
	call	__stack_chk_fail
.LVL90:
.L54:
	.loc 1 163 0 discriminator 1
	movl	$__PRETTY_FUNCTION__.4757, %ecx
	movl	$163, %edx
	movl	$.LC9, %esi
	movl	$.LC10, %edi
	call	__assert_fail
.LVL91:
.L50:
	.loc 1 126 0 discriminator 1
	movl	$__PRETTY_FUNCTION__.4757, %ecx
	movl	$126, %edx
	movl	$.LC9, %esi
	movl	$.LC10, %edi
	call	__assert_fail
.LVL92:
.L56:
	.loc 1 165 0 discriminator 1
	movl	$__PRETTY_FUNCTION__.4757, %ecx
	movl	$165, %edx
	movl	$.LC9, %esi
	movl	$.LC10, %edi
	call	__assert_fail
.LVL93:
.L53:
	.loc 1 129 0 discriminator 1
	movl	$__PRETTY_FUNCTION__.4757, %ecx
	movl	$129, %edx
	movl	$.LC9, %esi
	movl	$.LC10, %edi
	call	__assert_fail
.LVL94:
.L52:
	.loc 1 128 0 discriminator 1
	movl	$__PRETTY_FUNCTION__.4757, %ecx
	movl	$128, %edx
	movl	$.LC9, %esi
	movl	$.LC10, %edi
	call	__assert_fail
.LVL95:
	.cfi_endproc
.LFE90:
	.size	main, .-main
	.comm	attr,288,32
	.section	.rodata
	.type	__PRETTY_FUNCTION__.4757, @object
	.size	__PRETTY_FUNCTION__.4757, 5
__PRETTY_FUNCTION__.4757:
	.string	"main"
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	-1074790400
	.align 8
.LC1:
	.long	2696277389
	.long	1051772663
	.align 8
.LC3:
	.long	0
	.long	1040187392
	.align 8
.LC4:
	.long	0
	.long	1084715008
	.align 8
.LC18:
	.long	0
	.long	1079574528
	.align 8
.LC20:
	.long	0
	.long	1100470148
	.text
.Letext0:
	.file 5 "/usr/lib/gcc/x86_64-linux-gnu/4.6/include/stddef.h"
	.file 6 "/usr/include/x86_64-linux-gnu/bits/types.h"
	.file 7 "/usr/include/stdio.h"
	.file 8 "/usr/include/libio.h"
	.file 9 "/usr/include/x86_64-linux-gnu/sys/types.h"
	.file 10 "/usr/include/x86_64-linux-gnu/bits/time.h"
	.file 11 "/usr/include/stdint.h"
	.file 12 "/lib/modules/3.2.0-77-generic/build/include/asm-generic/int-ll64.h"
	.file 13 "/lib/modules/3.2.0-77-generic/build/include/linux/perf_event.h"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.long	0xdf3
	.value	0x2
	.long	.Ldebug_abbrev0
	.byte	0x8
	.uleb128 0x1
	.long	.LASF147
	.byte	0x1
	.long	.LASF148
	.long	.LASF149
	.quad	0
	.quad	0
	.long	.Ldebug_ranges0+0x60
	.long	.Ldebug_line0
	.uleb128 0x2
	.long	.LASF7
	.byte	0x5
	.byte	0xd4
	.long	0x3c
	.uleb128 0x3
	.byte	0x8
	.byte	0x7
	.long	.LASF0
	.uleb128 0x3
	.byte	0x1
	.byte	0x8
	.long	.LASF1
	.uleb128 0x3
	.byte	0x2
	.byte	0x7
	.long	.LASF2
	.uleb128 0x3
	.byte	0x4
	.byte	0x7
	.long	.LASF3
	.uleb128 0x3
	.byte	0x1
	.byte	0x6
	.long	.LASF4
	.uleb128 0x3
	.byte	0x2
	.byte	0x5
	.long	.LASF5
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.string	"int"
	.uleb128 0x3
	.byte	0x8
	.byte	0x5
	.long	.LASF6
	.uleb128 0x2
	.long	.LASF8
	.byte	0x6
	.byte	0x8d
	.long	0x6d
	.uleb128 0x2
	.long	.LASF9
	.byte	0x6
	.byte	0x8e
	.long	0x6d
	.uleb128 0x2
	.long	.LASF10
	.byte	0x6
	.byte	0x8f
	.long	0x66
	.uleb128 0x2
	.long	.LASF11
	.byte	0x6
	.byte	0x95
	.long	0x6d
	.uleb128 0x2
	.long	.LASF12
	.byte	0x6
	.byte	0x97
	.long	0x6d
	.uleb128 0x5
	.byte	0x8
	.uleb128 0x2
	.long	.LASF13
	.byte	0x6
	.byte	0xb4
	.long	0x6d
	.uleb128 0x6
	.byte	0x8
	.long	0xbe
	.uleb128 0x3
	.byte	0x1
	.byte	0x6
	.long	.LASF14
	.uleb128 0x2
	.long	.LASF15
	.byte	0x7
	.byte	0x31
	.long	0xd0
	.uleb128 0x7
	.long	.LASF45
	.byte	0xd8
	.byte	0x8
	.value	0x111
	.long	0x29d
	.uleb128 0x8
	.long	.LASF16
	.byte	0x8
	.value	0x112
	.long	0x66
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x8
	.long	.LASF17
	.byte	0x8
	.value	0x117
	.long	0xb8
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x8
	.long	.LASF18
	.byte	0x8
	.value	0x118
	.long	0xb8
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x8
	.long	.LASF19
	.byte	0x8
	.value	0x119
	.long	0xb8
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x8
	.long	.LASF20
	.byte	0x8
	.value	0x11a
	.long	0xb8
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x8
	.long	.LASF21
	.byte	0x8
	.value	0x11b
	.long	0xb8
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x8
	.long	.LASF22
	.byte	0x8
	.value	0x11c
	.long	0xb8
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x8
	.long	.LASF23
	.byte	0x8
	.value	0x11d
	.long	0xb8
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0x8
	.long	.LASF24
	.byte	0x8
	.value	0x11e
	.long	0xb8
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0x8
	.long	.LASF25
	.byte	0x8
	.value	0x120
	.long	0xb8
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0x8
	.long	.LASF26
	.byte	0x8
	.value	0x121
	.long	0xb8
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x8
	.long	.LASF27
	.byte	0x8
	.value	0x122
	.long	0xb8
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x8
	.long	.LASF28
	.byte	0x8
	.value	0x124
	.long	0x2db
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0x8
	.long	.LASF29
	.byte	0x8
	.value	0x126
	.long	0x2e1
	.byte	0x2
	.byte	0x23
	.uleb128 0x68
	.uleb128 0x8
	.long	.LASF30
	.byte	0x8
	.value	0x128
	.long	0x66
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.uleb128 0x8
	.long	.LASF31
	.byte	0x8
	.value	0x12c
	.long	0x66
	.byte	0x2
	.byte	0x23
	.uleb128 0x74
	.uleb128 0x8
	.long	.LASF32
	.byte	0x8
	.value	0x12e
	.long	0x74
	.byte	0x2
	.byte	0x23
	.uleb128 0x78
	.uleb128 0x8
	.long	.LASF33
	.byte	0x8
	.value	0x132
	.long	0x4a
	.byte	0x3
	.byte	0x23
	.uleb128 0x80
	.uleb128 0x8
	.long	.LASF34
	.byte	0x8
	.value	0x133
	.long	0x58
	.byte	0x3
	.byte	0x23
	.uleb128 0x82
	.uleb128 0x8
	.long	.LASF35
	.byte	0x8
	.value	0x134
	.long	0x2e7
	.byte	0x3
	.byte	0x23
	.uleb128 0x83
	.uleb128 0x8
	.long	.LASF36
	.byte	0x8
	.value	0x138
	.long	0x2f7
	.byte	0x3
	.byte	0x23
	.uleb128 0x88
	.uleb128 0x8
	.long	.LASF37
	.byte	0x8
	.value	0x141
	.long	0x7f
	.byte	0x3
	.byte	0x23
	.uleb128 0x90
	.uleb128 0x8
	.long	.LASF38
	.byte	0x8
	.value	0x14a
	.long	0xab
	.byte	0x3
	.byte	0x23
	.uleb128 0x98
	.uleb128 0x8
	.long	.LASF39
	.byte	0x8
	.value	0x14b
	.long	0xab
	.byte	0x3
	.byte	0x23
	.uleb128 0xa0
	.uleb128 0x8
	.long	.LASF40
	.byte	0x8
	.value	0x14c
	.long	0xab
	.byte	0x3
	.byte	0x23
	.uleb128 0xa8
	.uleb128 0x8
	.long	.LASF41
	.byte	0x8
	.value	0x14d
	.long	0xab
	.byte	0x3
	.byte	0x23
	.uleb128 0xb0
	.uleb128 0x8
	.long	.LASF42
	.byte	0x8
	.value	0x14e
	.long	0x31
	.byte	0x3
	.byte	0x23
	.uleb128 0xb8
	.uleb128 0x8
	.long	.LASF43
	.byte	0x8
	.value	0x150
	.long	0x66
	.byte	0x3
	.byte	0x23
	.uleb128 0xc0
	.uleb128 0x8
	.long	.LASF44
	.byte	0x8
	.value	0x152
	.long	0x2fd
	.byte	0x3
	.byte	0x23
	.uleb128 0xc4
	.byte	0
	.uleb128 0x9
	.long	.LASF150
	.byte	0x8
	.byte	0xb6
	.uleb128 0xa
	.long	.LASF46
	.byte	0x18
	.byte	0x8
	.byte	0xbc
	.long	0x2db
	.uleb128 0xb
	.long	.LASF47
	.byte	0x8
	.byte	0xbd
	.long	0x2db
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.long	.LASF48
	.byte	0x8
	.byte	0xbe
	.long	0x2e1
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xb
	.long	.LASF49
	.byte	0x8
	.byte	0xc2
	.long	0x66
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0x2a4
	.uleb128 0x6
	.byte	0x8
	.long	0xd0
	.uleb128 0xc
	.long	0xbe
	.long	0x2f7
	.uleb128 0xd
	.long	0x3c
	.byte	0
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0x29d
	.uleb128 0xc
	.long	0xbe
	.long	0x30d
	.uleb128 0xd
	.long	0x3c
	.byte	0x13
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0x313
	.uleb128 0xe
	.long	0xbe
	.uleb128 0x2
	.long	.LASF50
	.byte	0x7
	.byte	0x67
	.long	0xad
	.uleb128 0x6
	.byte	0x8
	.long	0x66
	.uleb128 0x3
	.byte	0x8
	.byte	0x5
	.long	.LASF51
	.uleb128 0x2
	.long	.LASF52
	.byte	0x9
	.byte	0x63
	.long	0x8a
	.uleb128 0xa
	.long	.LASF53
	.byte	0x10
	.byte	0xa
	.byte	0x1f
	.long	0x364
	.uleb128 0xb
	.long	.LASF54
	.byte	0xa
	.byte	0x21
	.long	0x95
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.long	.LASF55
	.byte	0xa
	.byte	0x22
	.long	0xa0
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.byte	0x7
	.long	.LASF56
	.uleb128 0x2
	.long	.LASF57
	.byte	0xb
	.byte	0x38
	.long	0x3c
	.uleb128 0x2
	.long	.LASF58
	.byte	0xc
	.byte	0x1a
	.long	0x51
	.uleb128 0x2
	.long	.LASF59
	.byte	0xc
	.byte	0x1e
	.long	0x364
	.uleb128 0xf
	.long	.LASF67
	.byte	0x4
	.byte	0xd
	.byte	0x1c
	.long	0x3c3
	.uleb128 0x10
	.long	.LASF60
	.sleb128 0
	.uleb128 0x10
	.long	.LASF61
	.sleb128 1
	.uleb128 0x10
	.long	.LASF62
	.sleb128 2
	.uleb128 0x10
	.long	.LASF63
	.sleb128 3
	.uleb128 0x10
	.long	.LASF64
	.sleb128 4
	.uleb128 0x10
	.long	.LASF65
	.sleb128 5
	.uleb128 0x10
	.long	.LASF66
	.sleb128 6
	.byte	0
	.uleb128 0xf
	.long	.LASF68
	.byte	0x4
	.byte	0xd
	.byte	0x2c
	.long	0x40c
	.uleb128 0x10
	.long	.LASF69
	.sleb128 0
	.uleb128 0x10
	.long	.LASF70
	.sleb128 1
	.uleb128 0x10
	.long	.LASF71
	.sleb128 2
	.uleb128 0x10
	.long	.LASF72
	.sleb128 3
	.uleb128 0x10
	.long	.LASF73
	.sleb128 4
	.uleb128 0x10
	.long	.LASF74
	.sleb128 5
	.uleb128 0x10
	.long	.LASF75
	.sleb128 6
	.uleb128 0x10
	.long	.LASF76
	.sleb128 7
	.uleb128 0x10
	.long	.LASF77
	.sleb128 8
	.uleb128 0x10
	.long	.LASF78
	.sleb128 9
	.byte	0
	.uleb128 0x11
	.byte	0x8
	.byte	0xd
	.byte	0xba
	.long	0x42b
	.uleb128 0x12
	.long	.LASF79
	.byte	0xd
	.byte	0xbb
	.long	0x381
	.uleb128 0x12
	.long	.LASF80
	.byte	0xd
	.byte	0xbc
	.long	0x381
	.byte	0
	.uleb128 0x11
	.byte	0x4
	.byte	0xd
	.byte	0xe4
	.long	0x44a
	.uleb128 0x12
	.long	.LASF81
	.byte	0xd
	.byte	0xe5
	.long	0x376
	.uleb128 0x12
	.long	.LASF82
	.byte	0xd
	.byte	0xe6
	.long	0x376
	.byte	0
	.uleb128 0x11
	.byte	0x8
	.byte	0xd
	.byte	0xea
	.long	0x469
	.uleb128 0x12
	.long	.LASF83
	.byte	0xd
	.byte	0xeb
	.long	0x381
	.uleb128 0x12
	.long	.LASF84
	.byte	0xd
	.byte	0xec
	.long	0x381
	.byte	0
	.uleb128 0x11
	.byte	0x8
	.byte	0xd
	.byte	0xee
	.long	0x488
	.uleb128 0x12
	.long	.LASF85
	.byte	0xd
	.byte	0xef
	.long	0x381
	.uleb128 0x12
	.long	.LASF86
	.byte	0xd
	.byte	0xf0
	.long	0x381
	.byte	0
	.uleb128 0xa
	.long	.LASF87
	.byte	0x48
	.byte	0xd
	.byte	0xa9
	.long	0x66e
	.uleb128 0xb
	.long	.LASF88
	.byte	0xd
	.byte	0xae
	.long	0x376
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.long	.LASF89
	.byte	0xd
	.byte	0xb3
	.long	0x376
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.long	.LASF90
	.byte	0xd
	.byte	0xb8
	.long	0x381
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x13
	.long	0x40c
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xb
	.long	.LASF91
	.byte	0xd
	.byte	0xbf
	.long	0x381
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xb
	.long	.LASF92
	.byte	0xd
	.byte	0xc0
	.long	0x381
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x14
	.long	.LASF93
	.byte	0xd
	.byte	0xc2
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x3f
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF94
	.byte	0xd
	.byte	0xc3
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x3e
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF95
	.byte	0xd
	.byte	0xc4
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x3d
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF96
	.byte	0xd
	.byte	0xc5
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x3c
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF97
	.byte	0xd
	.byte	0xc6
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x3b
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF98
	.byte	0xd
	.byte	0xc7
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x3a
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF99
	.byte	0xd
	.byte	0xc8
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x39
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF100
	.byte	0xd
	.byte	0xc9
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x38
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF101
	.byte	0xd
	.byte	0xca
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x37
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF102
	.byte	0xd
	.byte	0xcb
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x36
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF103
	.byte	0xd
	.byte	0xcc
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x35
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF104
	.byte	0xd
	.byte	0xcd
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x34
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF105
	.byte	0xd
	.byte	0xce
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF106
	.byte	0xd
	.byte	0xcf
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x32
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF107
	.byte	0xd
	.byte	0xd0
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x31
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF108
	.byte	0xd
	.byte	0xdb
	.long	0x381
	.byte	0x8
	.byte	0x2
	.byte	0x2f
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF109
	.byte	0xd
	.byte	0xdc
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x2e
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF110
	.byte	0xd
	.byte	0xdd
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x2d
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF111
	.byte	0xd
	.byte	0xdf
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x2c
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF112
	.byte	0xd
	.byte	0xe0
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x2b
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF113
	.byte	0xd
	.byte	0xe2
	.long	0x381
	.byte	0x8
	.byte	0x2b
	.byte	0
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x13
	.long	0x42b
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0xb
	.long	.LASF114
	.byte	0xd
	.byte	0xe9
	.long	0x376
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0x13
	.long	0x44a
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0x13
	.long	0x469
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.byte	0
	.uleb128 0x15
	.byte	0x1
	.long	.LASF121
	.byte	0x2
	.byte	0x67
	.byte	0x1
	.long	0x66
	.byte	0x3
	.byte	0x1
	.long	0x68e
	.uleb128 0x16
	.long	.LASF115
	.byte	0x2
	.byte	0x67
	.long	0x30d
	.uleb128 0x17
	.byte	0
	.uleb128 0x18
	.byte	0x1
	.long	.LASF118
	.byte	0x1
	.byte	0xd3
	.byte	0x1
	.long	0x6b6
	.byte	0x1
	.long	0x6b6
	.uleb128 0x16
	.long	.LASF116
	.byte	0x1
	.byte	0xd3
	.long	0x6b6
	.uleb128 0x19
	.string	"tp"
	.byte	0x1
	.byte	0xd4
	.long	0x33b
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.byte	0x4
	.long	.LASF117
	.uleb128 0x1a
	.byte	0x1
	.long	.LASF119
	.byte	0x3
	.value	0x117
	.byte	0x1
	.long	0x6b6
	.byte	0x3
	.long	0x6dd
	.uleb128 0x1b
	.long	.LASF120
	.byte	0x3
	.value	0x117
	.long	0x30d
	.byte	0
	.uleb128 0x15
	.byte	0x1
	.long	.LASF122
	.byte	0x4
	.byte	0x23
	.byte	0x1
	.long	0x318
	.byte	0x3
	.byte	0x1
	.long	0x712
	.uleb128 0x16
	.long	.LASF123
	.byte	0x4
	.byte	0x23
	.long	0x66
	.uleb128 0x16
	.long	.LASF124
	.byte	0x4
	.byte	0x23
	.long	0xab
	.uleb128 0x16
	.long	.LASF125
	.byte	0x4
	.byte	0x23
	.long	0x31
	.byte	0
	.uleb128 0x15
	.byte	0x1
	.long	.LASF126
	.byte	0x2
	.byte	0x60
	.byte	0x1
	.long	0x66
	.byte	0x3
	.byte	0x1
	.long	0x73d
	.uleb128 0x16
	.long	.LASF127
	.byte	0x2
	.byte	0x60
	.long	0x73d
	.uleb128 0x16
	.long	.LASF115
	.byte	0x2
	.byte	0x60
	.long	0x30d
	.uleb128 0x17
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0xc5
	.uleb128 0x1c
	.byte	0x1
	.long	.LASF129
	.byte	0x1
	.byte	0x25
	.byte	0x1
	.long	0x66
	.quad	.LFB89
	.quad	.LFE89
	.long	.LLST0
	.long	0x7b3
	.uleb128 0x1d
	.string	"hw"
	.byte	0x1
	.byte	0x25
	.long	0x7b3
	.long	.LLST1
	.uleb128 0x1d
	.string	"pid"
	.byte	0x1
	.byte	0x26
	.long	0x330
	.long	.LLST2
	.uleb128 0x1d
	.string	"cpu"
	.byte	0x1
	.byte	0x26
	.long	0x66
	.long	.LLST3
	.uleb128 0x1d
	.string	"grp"
	.byte	0x1
	.byte	0x26
	.long	0x66
	.long	.LLST4
	.uleb128 0x1e
	.long	.LASF128
	.byte	0x1
	.byte	0x26
	.long	0x3c
	.long	.LLST5
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0x488
	.uleb128 0x1f
	.long	0x68e
	.quad	.LFB91
	.quad	.LFE91
	.long	.LLST6
	.long	0x844
	.uleb128 0x20
	.long	0x6a0
	.long	.LLST7
	.uleb128 0x21
	.long	0x6ab
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x22
	.long	0x68e
	.quad	.LBB56
	.quad	.LBE56
	.byte	0x1
	.byte	0xd3
	.uleb128 0x23
	.quad	.LBB57
	.quad	.LBE57
	.uleb128 0x24
	.long	0x6ab
	.uleb128 0x25
	.long	0x6a0
	.uleb128 0x22
	.long	0x66e
	.quad	.LBB58
	.quad	.LBE58
	.byte	0x1
	.byte	0xd9
	.uleb128 0x26
	.long	0x681
	.byte	0xa
	.byte	0x3
	.quad	.LC2
	.byte	0x9f
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x1c
	.byte	0x1
	.long	.LASF130
	.byte	0x1
	.byte	0xe4
	.byte	0x1
	.long	0xb8
	.quad	.LFB92
	.quad	.LFE92
	.long	.LLST8
	.long	0x8a2
	.uleb128 0x1e
	.long	.LASF131
	.byte	0x1
	.byte	0xe4
	.long	0x6b6
	.long	.LLST9
	.uleb128 0x1e
	.long	.LASF132
	.byte	0x1
	.byte	0xe4
	.long	0xb8
	.long	.LLST10
	.uleb128 0x27
	.string	"i"
	.byte	0x1
	.byte	0xe5
	.long	0x66
	.long	.LLST11
	.uleb128 0x27
	.string	"r"
	.byte	0x1
	.byte	0xe6
	.long	0x6b6
	.long	.LLST12
	.byte	0
	.uleb128 0x1c
	.byte	0x1
	.long	.LASF133
	.byte	0x1
	.byte	0xfb
	.byte	0x1
	.long	0x66
	.quad	.LFB93
	.quad	.LFE93
	.long	.LLST13
	.long	0x8ff
	.uleb128 0x1d
	.string	"off"
	.byte	0x1
	.byte	0xfb
	.long	0x323
	.long	.LLST14
	.uleb128 0x27
	.string	"i"
	.byte	0x1
	.byte	0xfc
	.long	0x66
	.long	.LLST15
	.uleb128 0x23
	.quad	.LBB60
	.quad	.LBE60
	.uleb128 0x19
	.string	"r"
	.byte	0x1
	.byte	0xfe
	.long	0x6b6
	.byte	0
	.byte	0
	.uleb128 0x1c
	.byte	0x1
	.long	.LASF134
	.byte	0x1
	.byte	0x34
	.byte	0x1
	.long	0x66
	.quad	.LFB90
	.quad	.LFE90
	.long	.LLST16
	.long	0xd6a
	.uleb128 0x1e
	.long	.LASF135
	.byte	0x1
	.byte	0x34
	.long	0x66
	.long	.LLST17
	.uleb128 0x1e
	.long	.LASF136
	.byte	0x1
	.byte	0x34
	.long	0xd6a
	.long	.LLST18
	.uleb128 0x28
	.string	"t1"
	.byte	0x1
	.byte	0x35
	.long	0xd70
	.byte	0x3
	.byte	0x91
	.sleb128 -3088
	.uleb128 0x28
	.string	"s1"
	.byte	0x1
	.byte	0x36
	.long	0x6b6
	.byte	0x2
	.byte	0x77
	.sleb128 32
	.uleb128 0x27
	.string	"i"
	.byte	0x1
	.byte	0x37
	.long	0x66
	.long	.LLST19
	.uleb128 0x27
	.string	"j"
	.byte	0x1
	.byte	0x37
	.long	0x66
	.long	.LLST20
	.uleb128 0x29
	.long	.LASF137
	.byte	0x1
	.byte	0x39
	.long	0x6b6
	.long	.LLST21
	.uleb128 0x29
	.long	.LASF138
	.byte	0x1
	.byte	0x39
	.long	0x6b6
	.long	.LLST22
	.uleb128 0x27
	.string	"fp"
	.byte	0x1
	.byte	0x3a
	.long	0x73d
	.long	.LLST23
	.uleb128 0x27
	.string	"o1"
	.byte	0x1
	.byte	0x3b
	.long	0x323
	.long	.LLST24
	.uleb128 0x29
	.long	.LASF139
	.byte	0x1
	.byte	0x3b
	.long	0x323
	.long	.LLST25
	.uleb128 0x2a
	.long	.LASF140
	.byte	0x1
	.byte	0x3d
	.long	0xd81
	.byte	0x3
	.byte	0x91
	.sleb128 -3152
	.uleb128 0x2a
	.long	.LASF141
	.byte	0x1
	.byte	0x3d
	.long	0xd81
	.byte	0x3
	.byte	0x91
	.sleb128 -3120
	.uleb128 0x27
	.string	"fd"
	.byte	0x1
	.byte	0x3e
	.long	0xd91
	.long	.LLST26
	.uleb128 0x27
	.string	"rc"
	.byte	0x1
	.byte	0x3e
	.long	0x66
	.long	.LLST27
	.uleb128 0x29
	.long	.LASF142
	.byte	0x1
	.byte	0x6e
	.long	0x51
	.long	.LLST28
	.uleb128 0x2b
	.long	.LASF143
	.long	0xdb1
	.byte	0x1
	.byte	0x9
	.byte	0x3
	.quad	__PRETTY_FUNCTION__.4757
	.uleb128 0x2c
	.long	0x6bd
	.quad	.LBB61
	.long	.Ldebug_ranges0+0
	.byte	0x1
	.byte	0x67
	.long	0xa3e
	.uleb128 0x20
	.long	0x6d0
	.long	.LLST29
	.byte	0
	.uleb128 0x2d
	.long	0x6dd
	.quad	.LBB65
	.quad	.LBE65
	.byte	0x1
	.byte	0x7e
	.long	0xa75
	.uleb128 0x20
	.long	0x706
	.long	.LLST30
	.uleb128 0x20
	.long	0x6fb
	.long	.LLST31
	.uleb128 0x20
	.long	0x6f0
	.long	.LLST32
	.byte	0
	.uleb128 0x2d
	.long	0x6dd
	.quad	.LBB67
	.quad	.LBE67
	.byte	0x1
	.byte	0x7f
	.long	0xaac
	.uleb128 0x20
	.long	0x706
	.long	.LLST33
	.uleb128 0x20
	.long	0x6fb
	.long	.LLST34
	.uleb128 0x20
	.long	0x6f0
	.long	.LLST35
	.byte	0
	.uleb128 0x2d
	.long	0x6dd
	.quad	.LBB69
	.quad	.LBE69
	.byte	0x1
	.byte	0x80
	.long	0xae3
	.uleb128 0x20
	.long	0x706
	.long	.LLST36
	.uleb128 0x20
	.long	0x6fb
	.long	.LLST37
	.uleb128 0x20
	.long	0x6f0
	.long	.LLST38
	.byte	0
	.uleb128 0x2d
	.long	0x6dd
	.quad	.LBB71
	.quad	.LBE71
	.byte	0x1
	.byte	0x81
	.long	0xb1a
	.uleb128 0x20
	.long	0x706
	.long	.LLST39
	.uleb128 0x20
	.long	0x6fb
	.long	.LLST40
	.uleb128 0x20
	.long	0x6f0
	.long	.LLST41
	.byte	0
	.uleb128 0x2d
	.long	0x6dd
	.quad	.LBB73
	.quad	.LBE73
	.byte	0x1
	.byte	0xa3
	.long	0xb51
	.uleb128 0x20
	.long	0x706
	.long	.LLST42
	.uleb128 0x20
	.long	0x6fb
	.long	.LLST43
	.uleb128 0x20
	.long	0x6f0
	.long	.LLST44
	.byte	0
	.uleb128 0x2d
	.long	0x6dd
	.quad	.LBB75
	.quad	.LBE75
	.byte	0x1
	.byte	0xa4
	.long	0xb88
	.uleb128 0x20
	.long	0x706
	.long	.LLST45
	.uleb128 0x20
	.long	0x6fb
	.long	.LLST46
	.uleb128 0x20
	.long	0x6f0
	.long	.LLST47
	.byte	0
	.uleb128 0x2d
	.long	0x6dd
	.quad	.LBB77
	.quad	.LBE77
	.byte	0x1
	.byte	0xa5
	.long	0xbbf
	.uleb128 0x20
	.long	0x706
	.long	.LLST48
	.uleb128 0x20
	.long	0x6fb
	.long	.LLST49
	.uleb128 0x20
	.long	0x6f0
	.long	.LLST50
	.byte	0
	.uleb128 0x2d
	.long	0x6dd
	.quad	.LBB79
	.quad	.LBE79
	.byte	0x1
	.byte	0xa6
	.long	0xbf6
	.uleb128 0x20
	.long	0x706
	.long	.LLST51
	.uleb128 0x20
	.long	0x6fb
	.long	.LLST52
	.uleb128 0x20
	.long	0x6f0
	.long	.LLST53
	.byte	0
	.uleb128 0x2d
	.long	0x66e
	.quad	.LBB81
	.quad	.LBE81
	.byte	0x1
	.byte	0xb3
	.long	0xc1b
	.uleb128 0x20
	.long	0x681
	.long	.LLST54
	.byte	0
	.uleb128 0x2d
	.long	0x66e
	.quad	.LBB83
	.quad	.LBE83
	.byte	0x1
	.byte	0xb4
	.long	0xc40
	.uleb128 0x20
	.long	0x681
	.long	.LLST55
	.byte	0
	.uleb128 0x2d
	.long	0x66e
	.quad	.LBB85
	.quad	.LBE85
	.byte	0x1
	.byte	0xb5
	.long	0xc65
	.uleb128 0x20
	.long	0x681
	.long	.LLST56
	.byte	0
	.uleb128 0x2d
	.long	0x66e
	.quad	.LBB87
	.quad	.LBE87
	.byte	0x1
	.byte	0xb6
	.long	0xc8a
	.uleb128 0x20
	.long	0x681
	.long	.LLST57
	.byte	0
	.uleb128 0x2d
	.long	0x66e
	.quad	.LBB89
	.quad	.LBE89
	.byte	0x1
	.byte	0xb7
	.long	0xcaf
	.uleb128 0x20
	.long	0x681
	.long	.LLST58
	.byte	0
	.uleb128 0x2d
	.long	0x66e
	.quad	.LBB91
	.quad	.LBE91
	.byte	0x1
	.byte	0xb8
	.long	0xcd4
	.uleb128 0x20
	.long	0x681
	.long	.LLST59
	.byte	0
	.uleb128 0x2d
	.long	0x66e
	.quad	.LBB93
	.quad	.LBE93
	.byte	0x1
	.byte	0xb9
	.long	0xcf9
	.uleb128 0x20
	.long	0x681
	.long	.LLST60
	.byte	0
	.uleb128 0x2c
	.long	0x66e
	.quad	.LBB95
	.long	.Ldebug_ranges0+0x30
	.byte	0x1
	.byte	0xbb
	.long	0xd1a
	.uleb128 0x20
	.long	0x681
	.long	.LLST61
	.byte	0
	.uleb128 0x2d
	.long	0x66e
	.quad	.LBB99
	.quad	.LBE99
	.byte	0x1
	.byte	0xbc
	.long	0xd3f
	.uleb128 0x20
	.long	0x681
	.long	.LLST62
	.byte	0
	.uleb128 0x22
	.long	0x712
	.quad	.LBB101
	.quad	.LBE101
	.byte	0x1
	.byte	0xc2
	.uleb128 0x20
	.long	0x730
	.long	.LLST63
	.uleb128 0x20
	.long	0x725
	.long	.LLST64
	.byte	0
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0xb8
	.uleb128 0xc
	.long	0xbe
	.long	0xd81
	.uleb128 0x2e
	.long	0x3c
	.value	0xbb8
	.byte	0
	.uleb128 0xc
	.long	0x36b
	.long	0xd91
	.uleb128 0xd
	.long	0x3c
	.byte	0x3
	.byte	0
	.uleb128 0xc
	.long	0x66
	.long	0xda1
	.uleb128 0xd
	.long	0x3c
	.byte	0x3
	.byte	0
	.uleb128 0xc
	.long	0xbe
	.long	0xdb1
	.uleb128 0xd
	.long	0x3c
	.byte	0x4
	.byte	0
	.uleb128 0xe
	.long	0xda1
	.uleb128 0x2f
	.long	.LASF144
	.byte	0x7
	.byte	0xa9
	.long	0x2e1
	.byte	0x1
	.byte	0x1
	.uleb128 0x2f
	.long	.LASF145
	.byte	0x7
	.byte	0xaa
	.long	0x2e1
	.byte	0x1
	.byte	0x1
	.uleb128 0xc
	.long	0x488
	.long	0xde0
	.uleb128 0xd
	.long	0x3c
	.byte	0x3
	.byte	0
	.uleb128 0x30
	.long	.LASF146
	.byte	0x1
	.byte	0x31
	.long	0xdd0
	.byte	0x1
	.byte	0x9
	.byte	0x3
	.quad	attr
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x4
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0xd
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xd
	.uleb128 0xb
	.uleb128 0xc
	.uleb128 0xb
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x34
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x34
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x34
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x26
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x27
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x28
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x29
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2a
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x2b
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x34
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x2c
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2d
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2e
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x2f
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x30
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
.LLST0:
	.quad	.LFB89
	.quad	.LCFI0
	.value	0x2
	.byte	0x77
	.sleb128 8
	.quad	.LCFI0
	.quad	.LCFI1
	.value	0x2
	.byte	0x77
	.sleb128 16
	.quad	.LCFI1
	.quad	.LFE89
	.value	0x2
	.byte	0x77
	.sleb128 8
	.quad	0
	.quad	0
.LLST1:
	.quad	.LVL0
	.quad	.LVL5
	.value	0x1
	.byte	0x55
	.quad	.LVL5
	.quad	.LVL6-1
	.value	0x1
	.byte	0x54
	.quad	0
	.quad	0
.LLST2:
	.quad	.LVL0
	.quad	.LVL4
	.value	0x1
	.byte	0x54
	.quad	.LVL4
	.quad	.LVL6-1
	.value	0x1
	.byte	0x51
	.quad	0
	.quad	0
.LLST3:
	.quad	.LVL0
	.quad	.LVL3
	.value	0x1
	.byte	0x51
	.quad	.LVL3
	.quad	.LVL6-1
	.value	0x1
	.byte	0x52
	.quad	0
	.quad	0
.LLST4:
	.quad	.LVL0
	.quad	.LVL2
	.value	0x1
	.byte	0x52
	.quad	.LVL2
	.quad	.LVL6-1
	.value	0x1
	.byte	0x58
	.quad	0
	.quad	0
.LLST5:
	.quad	.LVL0
	.quad	.LVL1
	.value	0x1
	.byte	0x58
	.quad	.LVL1
	.quad	.LVL6-1
	.value	0x1
	.byte	0x59
	.quad	0
	.quad	0
.LLST6:
	.quad	.LFB91
	.quad	.LCFI2
	.value	0x2
	.byte	0x77
	.sleb128 8
	.quad	.LCFI2
	.quad	.LCFI3
	.value	0x2
	.byte	0x77
	.sleb128 48
	.quad	.LCFI3
	.quad	.LCFI4
	.value	0x2
	.byte	0x77
	.sleb128 8
	.quad	.LCFI4
	.quad	.LCFI5
	.value	0x2
	.byte	0x77
	.sleb128 48
	.quad	.LCFI5
	.quad	.LFE91
	.value	0x2
	.byte	0x77
	.sleb128 8
	.quad	0
	.quad	0
.LLST7:
	.quad	.LVL7
	.quad	.LVL8-1
	.value	0x1
	.byte	0x61
	.quad	.LVL8-1
	.quad	.LFE91
	.value	0x2
	.byte	0x91
	.sleb128 -40
	.quad	0
	.quad	0
.LLST8:
	.quad	.LFB92
	.quad	.LCFI6
	.value	0x2
	.byte	0x77
	.sleb128 8
	.quad	.LCFI6
	.quad	.LCFI7
	.value	0x2
	.byte	0x77
	.sleb128 16
	.quad	.LCFI7
	.quad	.LCFI8
	.value	0x2
	.byte	0x77
	.sleb128 24
	.quad	.LCFI8
	.quad	.LCFI9
	.value	0x2
	.byte	0x77
	.sleb128 32
	.quad	.LCFI9
	.quad	.LCFI10
	.value	0x2
	.byte	0x77
	.sleb128 48
	.quad	.LCFI10
	.quad	.LCFI11
	.value	0x2
	.byte	0x77
	.sleb128 32
	.quad	.LCFI11
	.quad	.LCFI12
	.value	0x2
	.byte	0x77
	.sleb128 24
	.quad	.LCFI12
	.quad	.LCFI13
	.value	0x2
	.byte	0x77
	.sleb128 16
	.quad	.LCFI13
	.quad	.LFE92
	.value	0x2
	.byte	0x77
	.sleb128 8
	.quad	0
	.quad	0
.LLST9:
	.quad	.LVL10
	.quad	.LVL11
	.value	0x1
	.byte	0x61
	.quad	.LVL11
	.quad	.LFE92
	.value	0x2
	.byte	0x91
	.sleb128 -40
	.quad	0
	.quad	0
.LLST10:
	.quad	.LVL10
	.quad	.LVL11
	.value	0x1
	.byte	0x55
	.quad	.LVL11
	.quad	.LVL15
	.value	0x1
	.byte	0x53
	.quad	.LVL15
	.quad	.LFE92
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LLST11:
	.quad	.LVL10
	.quad	.LVL11
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	0
	.quad	0
.LLST12:
	.quad	.LVL14
	.quad	.LFE92
	.value	0x1
	.byte	0x62
	.quad	0
	.quad	0
.LLST13:
	.quad	.LFB93
	.quad	.LCFI14
	.value	0x2
	.byte	0x77
	.sleb128 8
	.quad	.LCFI14
	.quad	.LCFI15
	.value	0x2
	.byte	0x77
	.sleb128 16
	.quad	.LCFI15
	.quad	.LCFI16
	.value	0x2
	.byte	0x77
	.sleb128 24
	.quad	.LCFI16
	.quad	.LCFI17
	.value	0x2
	.byte	0x77
	.sleb128 32
	.quad	.LCFI17
	.quad	.LCFI18
	.value	0x2
	.byte	0x77
	.sleb128 24
	.quad	.LCFI18
	.quad	.LCFI19
	.value	0x2
	.byte	0x77
	.sleb128 16
	.quad	.LCFI19
	.quad	.LFE93
	.value	0x2
	.byte	0x77
	.sleb128 8
	.quad	0
	.quad	0
.LLST14:
	.quad	.LVL16
	.quad	.LVL17
	.value	0x1
	.byte	0x55
	.quad	.LVL17
	.quad	.LVL20
	.value	0x1
	.byte	0x56
	.quad	0
	.quad	0
.LLST15:
	.quad	.LVL16
	.quad	.LVL17
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	0
	.quad	0
.LLST16:
	.quad	.LFB90
	.quad	.LCFI20
	.value	0x2
	.byte	0x77
	.sleb128 8
	.quad	.LCFI20
	.quad	.LCFI21
	.value	0x2
	.byte	0x77
	.sleb128 16
	.quad	.LCFI21
	.quad	.LCFI22
	.value	0x2
	.byte	0x77
	.sleb128 24
	.quad	.LCFI22
	.quad	.LCFI23
	.value	0x2
	.byte	0x77
	.sleb128 32
	.quad	.LCFI23
	.quad	.LCFI24
	.value	0x2
	.byte	0x77
	.sleb128 40
	.quad	.LCFI24
	.quad	.LCFI25
	.value	0x2
	.byte	0x77
	.sleb128 48
	.quad	.LCFI25
	.quad	.LCFI26
	.value	0x2
	.byte	0x77
	.sleb128 56
	.quad	.LCFI26
	.quad	.LCFI27
	.value	0x3
	.byte	0x77
	.sleb128 3200
	.quad	.LCFI27
	.quad	.LCFI28
	.value	0x2
	.byte	0x77
	.sleb128 56
	.quad	.LCFI28
	.quad	.LCFI29
	.value	0x2
	.byte	0x77
	.sleb128 48
	.quad	.LCFI29
	.quad	.LCFI30
	.value	0x2
	.byte	0x77
	.sleb128 40
	.quad	.LCFI30
	.quad	.LCFI31
	.value	0x2
	.byte	0x77
	.sleb128 32
	.quad	.LCFI31
	.quad	.LCFI32
	.value	0x2
	.byte	0x77
	.sleb128 24
	.quad	.LCFI32
	.quad	.LCFI33
	.value	0x2
	.byte	0x77
	.sleb128 16
	.quad	.LCFI33
	.quad	.LCFI34
	.value	0x2
	.byte	0x77
	.sleb128 8
	.quad	.LCFI34
	.quad	.LFE90
	.value	0x3
	.byte	0x77
	.sleb128 3200
	.quad	0
	.quad	0
.LLST17:
	.quad	.LVL21
	.quad	.LVL23-1
	.value	0x1
	.byte	0x55
	.quad	.LVL23-1
	.quad	.LVL36
	.value	0x1
	.byte	0x5d
	.quad	.LVL79
	.quad	.LVL86
	.value	0x1
	.byte	0x5d
	.quad	0
	.quad	0
.LLST18:
	.quad	.LVL21
	.quad	.LVL23-1
	.value	0x1
	.byte	0x54
	.quad	.LVL23-1
	.quad	.LVL50
	.value	0x1
	.byte	0x5c
	.quad	.LVL79
	.quad	.LVL87
	.value	0x1
	.byte	0x5c
	.quad	.LVL91
	.quad	.LVL92
	.value	0x1
	.byte	0x5c
	.quad	.LVL93
	.quad	.LFE90
	.value	0x1
	.byte	0x5c
	.quad	0
	.quad	0
.LLST19:
	.quad	.LVL22
	.quad	.LVL50
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL51
	.quad	.LVL53-1
	.value	0x3
	.byte	0x70
	.sleb128 0
	.byte	0x9f
	.quad	.LVL72
	.quad	.LVL73
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL79
	.quad	.LVL87
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL91
	.quad	.LVL92
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL93
	.quad	.LFE90
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	0
	.quad	0
.LLST20:
	.quad	.LVL22
	.quad	.LVL50
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL51
	.quad	.LVL75
	.value	0x1
	.byte	0x5c
	.quad	.LVL77
	.quad	.LVL79
	.value	0x1
	.byte	0x5c
	.quad	.LVL79
	.quad	.LVL87
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL87
	.quad	.LVL89
	.value	0x1
	.byte	0x5c
	.quad	.LVL90
	.quad	.LVL91
	.value	0x1
	.byte	0x5c
	.quad	.LVL91
	.quad	.LVL92
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL92
	.quad	.LVL93
	.value	0x1
	.byte	0x5c
	.quad	.LVL93
	.quad	.LFE90
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	0
	.quad	0
.LLST21:
	.quad	.LVL40
	.quad	.LVL42-1
	.value	0x1
	.byte	0x61
	.quad	.LVL42-1
	.quad	.LVL75
	.value	0x3
	.byte	0x91
	.sleb128 -3160
	.quad	.LVL77
	.quad	.LVL79
	.value	0x3
	.byte	0x91
	.sleb128 -3160
	.quad	.LVL86
	.quad	.LVL89
	.value	0x3
	.byte	0x91
	.sleb128 -3160
	.quad	.LVL90
	.quad	.LFE90
	.value	0x3
	.byte	0x91
	.sleb128 -3160
	.quad	0
	.quad	0
.LLST22:
	.quad	.LVL61
	.quad	.LVL62-1
	.value	0x1
	.byte	0x61
	.quad	0
	.quad	0
.LLST23:
	.quad	.LVL72
	.quad	.LVL73
	.value	0x1
	.byte	0x50
	.quad	.LVL73
	.quad	.LVL75
	.value	0x1
	.byte	0x56
	.quad	0
	.quad	0
.LLST24:
	.quad	.LVL34
	.quad	.LVL35-1
	.value	0x1
	.byte	0x50
	.quad	.LVL35-1
	.quad	.LVL75
	.value	0x1
	.byte	0x5e
	.quad	.LVL77
	.quad	.LVL79
	.value	0x1
	.byte	0x5e
	.quad	.LVL86
	.quad	.LVL89
	.value	0x1
	.byte	0x5e
	.quad	.LVL90
	.quad	.LFE90
	.value	0x1
	.byte	0x5e
	.quad	0
	.quad	0
.LLST25:
	.quad	.LVL36
	.quad	.LVL37-1
	.value	0x1
	.byte	0x50
	.quad	.LVL37-1
	.quad	.LVL75
	.value	0x1
	.byte	0x5d
	.quad	.LVL77
	.quad	.LVL79
	.value	0x1
	.byte	0x5d
	.quad	.LVL86
	.quad	.LVL89
	.value	0x1
	.byte	0x5d
	.quad	.LVL90
	.quad	.LFE90
	.value	0x1
	.byte	0x5d
	.quad	0
	.quad	0
.LLST26:
	.quad	.LVL24
	.quad	.LVL25
	.value	0x5
	.byte	0x50
	.byte	0x93
	.uleb128 0x4
	.byte	0x93
	.uleb128 0xc
	.quad	.LVL25
	.quad	.LVL26
	.value	0x5
	.byte	0x56
	.byte	0x93
	.uleb128 0x4
	.byte	0x93
	.uleb128 0xc
	.quad	.LVL26
	.quad	.LVL27
	.value	0x8
	.byte	0x56
	.byte	0x93
	.uleb128 0x4
	.byte	0x50
	.byte	0x93
	.uleb128 0x4
	.byte	0x93
	.uleb128 0x8
	.quad	.LVL27
	.quad	.LVL28
	.value	0x8
	.byte	0x56
	.byte	0x93
	.uleb128 0x4
	.byte	0x53
	.byte	0x93
	.uleb128 0x4
	.byte	0x93
	.uleb128 0x8
	.quad	.LVL28
	.quad	.LVL29
	.value	0xb
	.byte	0x56
	.byte	0x93
	.uleb128 0x4
	.byte	0x53
	.byte	0x93
	.uleb128 0x4
	.byte	0x50
	.byte	0x93
	.uleb128 0x4
	.byte	0x93
	.uleb128 0x4
	.quad	.LVL29
	.quad	.LVL30
	.value	0xb
	.byte	0x56
	.byte	0x93
	.uleb128 0x4
	.byte	0x53
	.byte	0x93
	.uleb128 0x4
	.byte	0x5f
	.byte	0x93
	.uleb128 0x4
	.byte	0x93
	.uleb128 0x4
	.quad	.LVL30
	.quad	.LVL31
	.value	0xc
	.byte	0x56
	.byte	0x93
	.uleb128 0x4
	.byte	0x53
	.byte	0x93
	.uleb128 0x4
	.byte	0x5f
	.byte	0x93
	.uleb128 0x4
	.byte	0x50
	.byte	0x93
	.uleb128 0x4
	.quad	.LVL31
	.quad	.LVL72
	.value	0xe
	.byte	0x56
	.byte	0x93
	.uleb128 0x4
	.byte	0x53
	.byte	0x93
	.uleb128 0x4
	.byte	0x5f
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -3172
	.byte	0x93
	.uleb128 0x4
	.quad	.LVL72
	.quad	.LVL73
	.value	0xd
	.byte	0x93
	.uleb128 0x4
	.byte	0x53
	.byte	0x93
	.uleb128 0x4
	.byte	0x5f
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -3172
	.byte	0x93
	.uleb128 0x4
	.quad	.LVL73
	.quad	.LVL76
	.value	0xa
	.byte	0x93
	.uleb128 0x8
	.byte	0x5f
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -3172
	.byte	0x93
	.uleb128 0x4
	.quad	.LVL76
	.quad	.LVL77
	.value	0x7
	.byte	0x93
	.uleb128 0xc
	.byte	0x91
	.sleb128 -3172
	.byte	0x93
	.uleb128 0x4
	.quad	.LVL77
	.quad	.LVL79
	.value	0xe
	.byte	0x56
	.byte	0x93
	.uleb128 0x4
	.byte	0x53
	.byte	0x93
	.uleb128 0x4
	.byte	0x5f
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -3172
	.byte	0x93
	.uleb128 0x4
	.quad	.LVL79
	.quad	.LVL80
	.value	0x8
	.byte	0x56
	.byte	0x93
	.uleb128 0x4
	.byte	0x53
	.byte	0x93
	.uleb128 0x4
	.byte	0x93
	.uleb128 0x8
	.quad	.LVL80
	.quad	.LVL81-1
	.value	0xc
	.byte	0x56
	.byte	0x93
	.uleb128 0x4
	.byte	0x53
	.byte	0x93
	.uleb128 0x4
	.byte	0x5f
	.byte	0x93
	.uleb128 0x4
	.byte	0x50
	.byte	0x93
	.uleb128 0x4
	.quad	.LVL81-1
	.quad	.LVL82
	.value	0xe
	.byte	0x56
	.byte	0x93
	.uleb128 0x4
	.byte	0x53
	.byte	0x93
	.uleb128 0x4
	.byte	0x5f
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -3172
	.byte	0x93
	.uleb128 0x4
	.quad	.LVL82
	.quad	.LVL83
	.value	0x5
	.byte	0x56
	.byte	0x93
	.uleb128 0x4
	.byte	0x93
	.uleb128 0xc
	.quad	.LVL83
	.quad	.LVL84-1
	.value	0xb
	.byte	0x56
	.byte	0x93
	.uleb128 0x4
	.byte	0x53
	.byte	0x93
	.uleb128 0x4
	.byte	0x50
	.byte	0x93
	.uleb128 0x4
	.byte	0x93
	.uleb128 0x4
	.quad	.LVL84-1
	.quad	.LVL85
	.value	0xb
	.byte	0x56
	.byte	0x93
	.uleb128 0x4
	.byte	0x53
	.byte	0x93
	.uleb128 0x4
	.byte	0x5f
	.byte	0x93
	.uleb128 0x4
	.byte	0x93
	.uleb128 0x4
	.quad	.LVL85
	.quad	.LVL89
	.value	0xe
	.byte	0x56
	.byte	0x93
	.uleb128 0x4
	.byte	0x53
	.byte	0x93
	.uleb128 0x4
	.byte	0x5f
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -3172
	.byte	0x93
	.uleb128 0x4
	.quad	.LVL89
	.quad	.LVL90
	.value	0xa
	.byte	0x93
	.uleb128 0x8
	.byte	0x5f
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -3172
	.byte	0x93
	.uleb128 0x4
	.quad	.LVL90
	.quad	.LFE90
	.value	0xe
	.byte	0x56
	.byte	0x93
	.uleb128 0x4
	.byte	0x53
	.byte	0x93
	.uleb128 0x4
	.byte	0x5f
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -3172
	.byte	0x93
	.uleb128 0x4
	.quad	0
	.quad	0
.LLST27:
	.quad	.LVL42
	.quad	.LVL44-1
	.value	0x1
	.byte	0x50
	.quad	.LVL44
	.quad	.LVL46-1
	.value	0x1
	.byte	0x50
	.quad	.LVL46
	.quad	.LVL48-1
	.value	0x1
	.byte	0x50
	.quad	.LVL48
	.quad	.LVL49
	.value	0x1
	.byte	0x50
	.quad	.LVL53
	.quad	.LVL55-1
	.value	0x1
	.byte	0x50
	.quad	.LVL55
	.quad	.LVL57-1
	.value	0x1
	.byte	0x50
	.quad	.LVL57
	.quad	.LVL59-1
	.value	0x1
	.byte	0x50
	.quad	.LVL59
	.quad	.LVL60-1
	.value	0x1
	.byte	0x50
	.quad	.LVL86
	.quad	.LVL87-1
	.value	0x1
	.byte	0x50
	.quad	.LVL87
	.quad	.LVL88-1
	.value	0x1
	.byte	0x50
	.quad	.LVL88
	.quad	.LVL89-1
	.value	0x1
	.byte	0x50
	.quad	.LVL90
	.quad	.LVL91-1
	.value	0x1
	.byte	0x50
	.quad	.LVL91
	.quad	.LVL92-1
	.value	0x1
	.byte	0x50
	.quad	.LVL92
	.quad	.LVL93-1
	.value	0x1
	.byte	0x50
	.quad	.LVL93
	.quad	.LVL94-1
	.value	0x1
	.byte	0x50
	.quad	.LVL94
	.quad	.LVL95-1
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LLST28:
	.quad	.LVL37
	.quad	.LVL38-1
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LLST29:
	.quad	.LVL32
	.quad	.LVL33-1
	.value	0x2
	.byte	0x7c
	.sleb128 8
	.quad	0
	.quad	0
.LLST30:
	.quad	.LVL41
	.quad	.LVL75
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL77
	.quad	.LVL79
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL86
	.quad	.LVL89
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL90
	.quad	.LFE90
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	0
	.quad	0
.LLST31:
	.quad	.LVL41
	.quad	.LVL75
	.value	0x4
	.byte	0x91
	.sleb128 -3152
	.byte	0x9f
	.quad	.LVL77
	.quad	.LVL79
	.value	0x4
	.byte	0x91
	.sleb128 -3152
	.byte	0x9f
	.quad	.LVL86
	.quad	.LVL89
	.value	0x4
	.byte	0x91
	.sleb128 -3152
	.byte	0x9f
	.quad	.LVL90
	.quad	.LFE90
	.value	0x4
	.byte	0x91
	.sleb128 -3152
	.byte	0x9f
	.quad	0
	.quad	0
.LLST32:
	.quad	.LVL41
	.quad	.LVL72
	.value	0x1
	.byte	0x56
	.quad	.LVL77
	.quad	.LVL79
	.value	0x1
	.byte	0x56
	.quad	.LVL86
	.quad	.LVL89
	.value	0x1
	.byte	0x56
	.quad	.LVL90
	.quad	.LFE90
	.value	0x1
	.byte	0x56
	.quad	0
	.quad	0
.LLST33:
	.quad	.LVL43
	.quad	.LVL75
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL77
	.quad	.LVL79
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL86
	.quad	.LVL89
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL90
	.quad	.LVL91
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL92
	.quad	.LFE90
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	0
	.quad	0
.LLST34:
	.quad	.LVL43
	.quad	.LVL75
	.value	0x4
	.byte	0x91
	.sleb128 -3144
	.byte	0x9f
	.quad	.LVL77
	.quad	.LVL79
	.value	0x4
	.byte	0x91
	.sleb128 -3144
	.byte	0x9f
	.quad	.LVL86
	.quad	.LVL89
	.value	0x4
	.byte	0x91
	.sleb128 -3144
	.byte	0x9f
	.quad	.LVL90
	.quad	.LVL91
	.value	0x4
	.byte	0x91
	.sleb128 -3144
	.byte	0x9f
	.quad	.LVL92
	.quad	.LFE90
	.value	0x4
	.byte	0x91
	.sleb128 -3144
	.byte	0x9f
	.quad	0
	.quad	0
.LLST35:
	.quad	.LVL43
	.quad	.LVL73
	.value	0x1
	.byte	0x53
	.quad	.LVL77
	.quad	.LVL79
	.value	0x1
	.byte	0x53
	.quad	.LVL86
	.quad	.LVL89
	.value	0x1
	.byte	0x53
	.quad	.LVL90
	.quad	.LVL91
	.value	0x1
	.byte	0x53
	.quad	.LVL92
	.quad	.LFE90
	.value	0x1
	.byte	0x53
	.quad	0
	.quad	0
.LLST36:
	.quad	.LVL45
	.quad	.LVL75
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL77
	.quad	.LVL79
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL87
	.quad	.LVL89
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL90
	.quad	.LVL91
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL92
	.quad	.LFE90
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	0
	.quad	0
.LLST37:
	.quad	.LVL45
	.quad	.LVL75
	.value	0x4
	.byte	0x91
	.sleb128 -3136
	.byte	0x9f
	.quad	.LVL77
	.quad	.LVL79
	.value	0x4
	.byte	0x91
	.sleb128 -3136
	.byte	0x9f
	.quad	.LVL87
	.quad	.LVL89
	.value	0x4
	.byte	0x91
	.sleb128 -3136
	.byte	0x9f
	.quad	.LVL90
	.quad	.LVL91
	.value	0x4
	.byte	0x91
	.sleb128 -3136
	.byte	0x9f
	.quad	.LVL92
	.quad	.LFE90
	.value	0x4
	.byte	0x91
	.sleb128 -3136
	.byte	0x9f
	.quad	0
	.quad	0
.LLST38:
	.quad	.LVL45
	.quad	.LVL75
	.value	0x1
	.byte	0x5f
	.quad	.LVL77
	.quad	.LVL79
	.value	0x1
	.byte	0x5f
	.quad	.LVL87
	.quad	.LVL89
	.value	0x1
	.byte	0x5f
	.quad	.LVL90
	.quad	.LVL91
	.value	0x1
	.byte	0x5f
	.quad	.LVL92
	.quad	.LFE90
	.value	0x1
	.byte	0x5f
	.quad	0
	.quad	0
.LLST39:
	.quad	.LVL47
	.quad	.LVL75
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL77
	.quad	.LVL79
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL87
	.quad	.LVL89
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL90
	.quad	.LVL91
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL92
	.quad	.LVL94
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	0
	.quad	0
.LLST40:
	.quad	.LVL47
	.quad	.LVL75
	.value	0x4
	.byte	0x91
	.sleb128 -3128
	.byte	0x9f
	.quad	.LVL77
	.quad	.LVL79
	.value	0x4
	.byte	0x91
	.sleb128 -3128
	.byte	0x9f
	.quad	.LVL87
	.quad	.LVL89
	.value	0x4
	.byte	0x91
	.sleb128 -3128
	.byte	0x9f
	.quad	.LVL90
	.quad	.LVL91
	.value	0x4
	.byte	0x91
	.sleb128 -3128
	.byte	0x9f
	.quad	.LVL92
	.quad	.LVL94
	.value	0x4
	.byte	0x91
	.sleb128 -3128
	.byte	0x9f
	.quad	0
	.quad	0
.LLST41:
	.quad	.LVL47
	.quad	.LVL75
	.value	0x3
	.byte	0x91
	.sleb128 -3172
	.quad	.LVL77
	.quad	.LVL79
	.value	0x3
	.byte	0x91
	.sleb128 -3172
	.quad	.LVL87
	.quad	.LVL89
	.value	0x3
	.byte	0x91
	.sleb128 -3172
	.quad	.LVL90
	.quad	.LVL91
	.value	0x3
	.byte	0x91
	.sleb128 -3172
	.quad	.LVL92
	.quad	.LVL94
	.value	0x3
	.byte	0x91
	.sleb128 -3172
	.quad	0
	.quad	0
.LLST42:
	.quad	.LVL52
	.quad	.LVL75
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL77
	.quad	.LVL79
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL87
	.quad	.LVL89
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL90
	.quad	.LVL91
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL92
	.quad	.LVL93
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	0
	.quad	0
.LLST43:
	.quad	.LVL52
	.quad	.LVL75
	.value	0x4
	.byte	0x91
	.sleb128 -3120
	.byte	0x9f
	.quad	.LVL77
	.quad	.LVL79
	.value	0x4
	.byte	0x91
	.sleb128 -3120
	.byte	0x9f
	.quad	.LVL87
	.quad	.LVL89
	.value	0x4
	.byte	0x91
	.sleb128 -3120
	.byte	0x9f
	.quad	.LVL90
	.quad	.LVL91
	.value	0x4
	.byte	0x91
	.sleb128 -3120
	.byte	0x9f
	.quad	.LVL92
	.quad	.LVL93
	.value	0x4
	.byte	0x91
	.sleb128 -3120
	.byte	0x9f
	.quad	0
	.quad	0
.LLST44:
	.quad	.LVL52
	.quad	.LVL72
	.value	0x1
	.byte	0x56
	.quad	.LVL77
	.quad	.LVL79
	.value	0x1
	.byte	0x56
	.quad	.LVL87
	.quad	.LVL89
	.value	0x1
	.byte	0x56
	.quad	.LVL90
	.quad	.LVL91
	.value	0x1
	.byte	0x56
	.quad	.LVL92
	.quad	.LVL93
	.value	0x1
	.byte	0x56
	.quad	0
	.quad	0
.LLST45:
	.quad	.LVL54
	.quad	.LVL75
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL77
	.quad	.LVL79
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL87
	.quad	.LVL89
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL92
	.quad	.LVL93
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	0
	.quad	0
.LLST46:
	.quad	.LVL54
	.quad	.LVL75
	.value	0x4
	.byte	0x91
	.sleb128 -3112
	.byte	0x9f
	.quad	.LVL77
	.quad	.LVL79
	.value	0x4
	.byte	0x91
	.sleb128 -3112
	.byte	0x9f
	.quad	.LVL87
	.quad	.LVL89
	.value	0x4
	.byte	0x91
	.sleb128 -3112
	.byte	0x9f
	.quad	.LVL92
	.quad	.LVL93
	.value	0x4
	.byte	0x91
	.sleb128 -3112
	.byte	0x9f
	.quad	0
	.quad	0
.LLST47:
	.quad	.LVL54
	.quad	.LVL73
	.value	0x1
	.byte	0x53
	.quad	.LVL77
	.quad	.LVL79
	.value	0x1
	.byte	0x53
	.quad	.LVL87
	.quad	.LVL89
	.value	0x1
	.byte	0x53
	.quad	.LVL92
	.quad	.LVL93
	.value	0x1
	.byte	0x53
	.quad	0
	.quad	0
.LLST48:
	.quad	.LVL56
	.quad	.LVL75
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL77
	.quad	.LVL79
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL88
	.quad	.LVL89
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL92
	.quad	.LVL93
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	0
	.quad	0
.LLST49:
	.quad	.LVL56
	.quad	.LVL75
	.value	0x4
	.byte	0x91
	.sleb128 -3104
	.byte	0x9f
	.quad	.LVL77
	.quad	.LVL79
	.value	0x4
	.byte	0x91
	.sleb128 -3104
	.byte	0x9f
	.quad	.LVL88
	.quad	.LVL89
	.value	0x4
	.byte	0x91
	.sleb128 -3104
	.byte	0x9f
	.quad	.LVL92
	.quad	.LVL93
	.value	0x4
	.byte	0x91
	.sleb128 -3104
	.byte	0x9f
	.quad	0
	.quad	0
.LLST50:
	.quad	.LVL56
	.quad	.LVL75
	.value	0x1
	.byte	0x5f
	.quad	.LVL77
	.quad	.LVL79
	.value	0x1
	.byte	0x5f
	.quad	.LVL88
	.quad	.LVL89
	.value	0x1
	.byte	0x5f
	.quad	.LVL92
	.quad	.LVL93
	.value	0x1
	.byte	0x5f
	.quad	0
	.quad	0
.LLST51:
	.quad	.LVL58
	.quad	.LVL75
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL77
	.quad	.LVL79
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL88
	.quad	.LVL89
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	0
	.quad	0
.LLST52:
	.quad	.LVL58
	.quad	.LVL75
	.value	0x4
	.byte	0x91
	.sleb128 -3096
	.byte	0x9f
	.quad	.LVL77
	.quad	.LVL79
	.value	0x4
	.byte	0x91
	.sleb128 -3096
	.byte	0x9f
	.quad	.LVL88
	.quad	.LVL89
	.value	0x4
	.byte	0x91
	.sleb128 -3096
	.byte	0x9f
	.quad	0
	.quad	0
.LLST53:
	.quad	.LVL58
	.quad	.LVL75
	.value	0x3
	.byte	0x91
	.sleb128 -3172
	.quad	.LVL77
	.quad	.LVL79
	.value	0x3
	.byte	0x91
	.sleb128 -3172
	.quad	.LVL88
	.quad	.LVL89
	.value	0x3
	.byte	0x91
	.sleb128 -3172
	.quad	0
	.quad	0
.LLST54:
	.quad	.LVL62
	.quad	.LVL75
	.value	0xa
	.byte	0x3
	.quad	.LC12
	.byte	0x9f
	.quad	.LVL77
	.quad	.LVL79
	.value	0xa
	.byte	0x3
	.quad	.LC12
	.byte	0x9f
	.quad	0
	.quad	0
.LLST55:
	.quad	.LVL63
	.quad	.LVL75
	.value	0xa
	.byte	0x3
	.quad	.LC13
	.byte	0x9f
	.quad	.LVL77
	.quad	.LVL79
	.value	0xa
	.byte	0x3
	.quad	.LC13
	.byte	0x9f
	.quad	0
	.quad	0
.LLST56:
	.quad	.LVL64
	.quad	.LVL75
	.value	0xa
	.byte	0x3
	.quad	.LC14
	.byte	0x9f
	.quad	.LVL77
	.quad	.LVL79
	.value	0xa
	.byte	0x3
	.quad	.LC14
	.byte	0x9f
	.quad	0
	.quad	0
.LLST57:
	.quad	.LVL65
	.quad	.LVL75
	.value	0xa
	.byte	0x3
	.quad	.LC15
	.byte	0x9f
	.quad	.LVL77
	.quad	.LVL79
	.value	0xa
	.byte	0x3
	.quad	.LC15
	.byte	0x9f
	.quad	0
	.quad	0
.LLST58:
	.quad	.LVL66
	.quad	.LVL75
	.value	0xa
	.byte	0x3
	.quad	.LC16
	.byte	0x9f
	.quad	.LVL78
	.quad	.LVL79
	.value	0xa
	.byte	0x3
	.quad	.LC16
	.byte	0x9f
	.quad	0
	.quad	0
.LLST59:
	.quad	.LVL67
	.quad	.LVL75
	.value	0xa
	.byte	0x3
	.quad	.LC17
	.byte	0x9f
	.quad	.LVL78
	.quad	.LVL79
	.value	0xa
	.byte	0x3
	.quad	.LC17
	.byte	0x9f
	.quad	0
	.quad	0
.LLST60:
	.quad	.LVL68
	.quad	.LVL75
	.value	0xa
	.byte	0x3
	.quad	.LC19
	.byte	0x9f
	.quad	.LVL78
	.quad	.LVL79
	.value	0xa
	.byte	0x3
	.quad	.LC19
	.byte	0x9f
	.quad	0
	.quad	0
.LLST61:
	.quad	.LVL70
	.quad	.LVL75
	.value	0xa
	.byte	0x3
	.quad	.LC21
	.byte	0x9f
	.quad	0
	.quad	0
.LLST62:
	.quad	.LVL71
	.quad	.LVL75
	.value	0xa
	.byte	0x3
	.quad	.LC22
	.byte	0x9f
	.quad	0
	.quad	0
.LLST63:
	.quad	.LVL73
	.quad	.LVL74
	.value	0xa
	.byte	0x3
	.quad	.LC25
	.byte	0x9f
	.quad	0
	.quad	0
.LLST64:
	.quad	.LVL73
	.quad	.LVL74
	.value	0x1
	.byte	0x56
	.quad	0
	.quad	0
	.section	.debug_aranges,"",@progbits
	.long	0x3c
	.value	0x2
	.long	.Ldebug_info0
	.byte	0x8
	.byte	0
	.value	0
	.value	0
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.quad	.LFB90
	.quad	.LFE90-.LFB90
	.quad	0
	.quad	0
	.section	.debug_ranges,"",@progbits
.Ldebug_ranges0:
	.quad	.LBB61
	.quad	.LBE61
	.quad	.LBB64
	.quad	.LBE64
	.quad	0
	.quad	0
	.quad	.LBB95
	.quad	.LBE95
	.quad	.LBB98
	.quad	.LBE98
	.quad	0
	.quad	0
	.quad	.Ltext0
	.quad	.Letext0
	.quad	.LFB90
	.quad	.LFE90
	.quad	0
	.quad	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF121:
	.string	"printf"
.LASF17:
	.string	"_IO_read_ptr"
.LASF142:
	.string	"iseed"
.LASF29:
	.string	"_chain"
.LASF109:
	.string	"mmap_data"
.LASF7:
	.string	"size_t"
.LASF82:
	.string	"wakeup_watermark"
.LASF35:
	.string	"_shortbuf"
.LASF96:
	.string	"exclusive"
.LASF50:
	.string	"ssize_t"
.LASF102:
	.string	"comm"
.LASF88:
	.string	"type"
.LASF77:
	.string	"PERF_COUNT_HW_STALLED_CYCLES_BACKEND"
.LASF23:
	.string	"_IO_buf_base"
.LASF92:
	.string	"read_format"
.LASF107:
	.string	"watermark"
.LASF72:
	.string	"PERF_COUNT_HW_CACHE_MISSES"
.LASF56:
	.string	"long long unsigned int"
.LASF80:
	.string	"sample_freq"
.LASF78:
	.string	"PERF_COUNT_HW_MAX"
.LASF73:
	.string	"PERF_COUNT_HW_BRANCH_INSTRUCTIONS"
.LASF100:
	.string	"exclude_idle"
.LASF51:
	.string	"long long int"
.LASF4:
	.string	"signed char"
.LASF103:
	.string	"freq"
.LASF143:
	.string	"__PRETTY_FUNCTION__"
.LASF131:
	.string	"selectivity"
.LASF30:
	.string	"_fileno"
.LASF81:
	.string	"wakeup_events"
.LASF18:
	.string	"_IO_read_end"
.LASF150:
	.string	"_IO_lock_t"
.LASF97:
	.string	"exclude_user"
.LASF6:
	.string	"long int"
.LASF16:
	.string	"_flags"
.LASF63:
	.string	"PERF_TYPE_HW_CACHE"
.LASF13:
	.string	"__ssize_t"
.LASF130:
	.string	"createData"
.LASF24:
	.string	"_IO_buf_end"
.LASF33:
	.string	"_cur_column"
.LASF91:
	.string	"sample_type"
.LASF101:
	.string	"mmap"
.LASF117:
	.string	"double"
.LASF67:
	.string	"perf_type_id"
.LASF62:
	.string	"PERF_TYPE_TRACEPOINT"
.LASF37:
	.string	"_offset"
.LASF129:
	.string	"perf_event_open"
.LASF84:
	.string	"config1"
.LASF86:
	.string	"config2"
.LASF70:
	.string	"PERF_COUNT_HW_INSTRUCTIONS"
.LASF38:
	.string	"__pad1"
.LASF66:
	.string	"PERF_TYPE_MAX"
.LASF125:
	.string	"__nbytes"
.LASF146:
	.string	"attr"
.LASF46:
	.string	"_IO_marker"
.LASF144:
	.string	"stdin"
.LASF113:
	.string	"__reserved_1"
.LASF3:
	.string	"unsigned int"
.LASF85:
	.string	"bp_len"
.LASF126:
	.string	"fprintf"
.LASF93:
	.string	"disabled"
.LASF94:
	.string	"inherit"
.LASF127:
	.string	"__stream"
.LASF0:
	.string	"long unsigned int"
.LASF58:
	.string	"__u32"
.LASF12:
	.string	"__suseconds_t"
.LASF21:
	.string	"_IO_write_ptr"
.LASF60:
	.string	"PERF_TYPE_HARDWARE"
.LASF99:
	.string	"exclude_hv"
.LASF48:
	.string	"_sbuf"
.LASF132:
	.string	"data"
.LASF89:
	.string	"size"
.LASF2:
	.string	"short unsigned int"
.LASF64:
	.string	"PERF_TYPE_RAW"
.LASF25:
	.string	"_IO_save_base"
.LASF36:
	.string	"_lock"
.LASF114:
	.string	"bp_type"
.LASF31:
	.string	"_flags2"
.LASF53:
	.string	"timeval"
.LASF145:
	.string	"stdout"
.LASF123:
	.string	"__fd"
.LASF116:
	.string	"start_time"
.LASF139:
	.string	"answer"
.LASF149:
	.string	"/home/fredrabelo/4112_project2/branch_mispred1"
.LASF54:
	.string	"tv_sec"
.LASF98:
	.string	"exclude_kernel"
.LASF22:
	.string	"_IO_write_end"
.LASF57:
	.string	"uint64_t"
.LASF61:
	.string	"PERF_TYPE_SOFTWARE"
.LASF79:
	.string	"sample_period"
.LASF140:
	.string	"val1"
.LASF141:
	.string	"val2"
.LASF45:
	.string	"_IO_FILE"
.LASF69:
	.string	"PERF_COUNT_HW_CPU_CYCLES"
.LASF124:
	.string	"__buf"
.LASF120:
	.string	"__nptr"
.LASF43:
	.string	"_mode"
.LASF133:
	.string	"createOffsets"
.LASF49:
	.string	"_pos"
.LASF32:
	.string	"_old_offset"
.LASF74:
	.string	"PERF_COUNT_HW_BRANCH_MISSES"
.LASF28:
	.string	"_markers"
.LASF110:
	.string	"sample_id_all"
.LASF119:
	.string	"atof"
.LASF83:
	.string	"bp_addr"
.LASF1:
	.string	"unsigned char"
.LASF71:
	.string	"PERF_COUNT_HW_CACHE_REFERENCES"
.LASF87:
	.string	"perf_event_attr"
.LASF10:
	.string	"__pid_t"
.LASF5:
	.string	"short int"
.LASF34:
	.string	"_vtable_offset"
.LASF15:
	.string	"FILE"
.LASF138:
	.string	"stop"
.LASF112:
	.string	"exclude_guest"
.LASF8:
	.string	"__off_t"
.LASF55:
	.string	"tv_usec"
.LASF147:
	.string	"GNU C 4.6.3"
.LASF59:
	.string	"__u64"
.LASF14:
	.string	"char"
.LASF90:
	.string	"config"
.LASF76:
	.string	"PERF_COUNT_HW_STALLED_CYCLES_FRONTEND"
.LASF68:
	.string	"perf_hw_id"
.LASF95:
	.string	"pinned"
.LASF47:
	.string	"_next"
.LASF9:
	.string	"__off64_t"
.LASF106:
	.string	"task"
.LASF19:
	.string	"_IO_read_base"
.LASF27:
	.string	"_IO_save_end"
.LASF115:
	.string	"__fmt"
.LASF118:
	.string	"get_timestamp"
.LASF39:
	.string	"__pad2"
.LASF40:
	.string	"__pad3"
.LASF41:
	.string	"__pad4"
.LASF42:
	.string	"__pad5"
.LASF11:
	.string	"__time_t"
.LASF108:
	.string	"precise_ip"
.LASF44:
	.string	"_unused2"
.LASF136:
	.string	"argv"
.LASF65:
	.string	"PERF_TYPE_BREAKPOINT"
.LASF148:
	.string	"branch_mispred.c"
.LASF26:
	.string	"_IO_backup_base"
.LASF128:
	.string	"flags"
.LASF75:
	.string	"PERF_COUNT_HW_BUS_CYCLES"
.LASF122:
	.string	"read"
.LASF135:
	.string	"argc"
.LASF52:
	.string	"pid_t"
.LASF137:
	.string	"start"
.LASF134:
	.string	"main"
.LASF20:
	.string	"_IO_write_base"
.LASF104:
	.string	"inherit_stat"
.LASF111:
	.string	"exclude_host"
.LASF105:
	.string	"enable_on_exec"
	.ident	"GCC: (Ubuntu/Linaro 4.6.3-1ubuntu5) 4.6.3"
	.section	.note.GNU-stack,"",@progbits
