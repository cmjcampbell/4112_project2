	.file	"branch_mispred.c"
	.text
.Ltext0:
	.p2align 4,,15
	.globl	perf_event_open
	.type	perf_event_open, @function
perf_event_open:
.LFB89:
	.file 1 "branch_mispred.c"
	.loc 1 39 0
	.cfi_startproc
.LVL0:
	subq	$8, %rsp
.LCFI0:
	.cfi_def_cfa_offset 16
	.loc 1 40 0
	movq	%r8, %r9
	xorl	%eax, %eax
	movl	%ecx, %r8d
.LVL1:
	movl	%edx, %ecx
.LVL2:
	movl	%esi, %edx
.LVL3:
	movq	%rdi, %rsi
.LVL4:
	movl	$298, %edi
.LVL5:
	call	syscall
.LVL6:
	.loc 1 41 0
	addq	$8, %rsp
.LCFI1:
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE89:
	.size	perf_event_open, .-perf_event_open
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"Failed to get time stamp"
	.text
	.p2align 4,,15
	.globl	get_timestamp
	.type	get_timestamp, @function
get_timestamp:
.LFB91:
	.loc 1 265 0
	.cfi_startproc
.LVL7:
	subq	$40, %rsp
.LCFI2:
	.cfi_def_cfa_offset 48
	.loc 1 268 0
	xorl	%esi, %esi
	leaq	16(%rsp), %rdi
	.loc 1 265 0
	movsd	%xmm0, 8(%rsp)
	.loc 1 268 0
	call	gettimeofday
.LVL8:
	testl	%eax, %eax
	jne	.L3
	.loc 1 269 0
	cvtsi2sdq	24(%rsp), %xmm2
	mulsd	.LC1(%rip), %xmm2
	cvtsi2sdq	16(%rsp), %xmm1
	addsd	%xmm2, %xmm1
	subsd	8(%rsp), %xmm1
	.loc 1 274 0
	addq	$40, %rsp
	.cfi_remember_state
.LCFI3:
	.cfi_def_cfa_offset 8
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L3:
.LCFI4:
	.cfi_restore_state
.LVL9:
.LBB74:
.LBB75:
.LBB76:
.LBB77:
	.file 2 "/usr/include/x86_64-linux-gnu/bits/stdio2.h"
	.loc 2 105 0
	movl	$.LC2, %esi
	movl	$1, %edi
	xorl	%eax, %eax
	call	__printf_chk
	movsd	.LC0(%rip), %xmm1
.LBE77:
.LBE76:
.LBE75:
.LBE74:
	.loc 1 274 0
	addq	$40, %rsp
.LCFI5:
	.cfi_def_cfa_offset 8
	movapd	%xmm1, %xmm0
	ret
	.cfi_endproc
.LFE91:
	.size	get_timestamp, .-get_timestamp
	.p2align 4,,15
	.globl	createData
	.type	createData, @function
createData:
.LFB92:
	.loc 1 282 0
	.cfi_startproc
.LVL10:
	pushq	%r12
.LCFI6:
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	movq	%rdi, %r12
	pushq	%rbp
.LCFI7:
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
	leaq	3000(%rdi), %rbp
	pushq	%rbx
.LCFI8:
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
.LCFI9:
	.cfi_def_cfa_offset 48
	.loc 1 282 0
	movsd	%xmm0, 8(%rsp)
.LVL11:
	.p2align 4,,10
	.p2align 3
.L7:
	.loc 1 287 0
	call	rand
.LVL12:
	cvtsi2sd	%eax, %xmm1
	.loc 1 289 0
	movsd	8(%rsp), %xmm0
	.loc 1 287 0
	mulsd	.LC3(%rip), %xmm1
	.loc 1 289 0
	ucomisd	%xmm1, %xmm0
	.loc 1 287 0
	seta	(%rbx)
	.loc 1 293 0
	addq	$1, %rbx
.LVL13:
	.loc 1 286 0
	cmpq	%rbp, %rbx
	jne	.L7
	.loc 1 295 0
	movb	$0, 3000(%r12)
	.loc 1 297 0
	addq	$16, %rsp
.LCFI10:
	.cfi_def_cfa_offset 32
	movq	%rbx, %rax
.LVL14:
	popq	%rbx
.LCFI11:
	.cfi_def_cfa_offset 24
.LVL15:
	popq	%rbp
.LCFI12:
	.cfi_def_cfa_offset 16
	popq	%r12
.LCFI13:
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE92:
	.size	createData, .-createData
	.p2align 4,,15
	.globl	createOffsets
	.type	createOffsets, @function
createOffsets:
.LFB93:
	.loc 1 305 0
	.cfi_startproc
.LVL16:
	pushq	%rbp
.LCFI14:
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rbp
	pushq	%rbx
.LCFI15:
	.cfi_def_cfa_offset 24
	.cfi_offset 3, -24
	xorl	%ebx, %ebx
	subq	$8, %rsp
.LCFI16:
	.cfi_def_cfa_offset 32
.LVL17:
	.p2align 4,,10
	.p2align 3
.L10:
.LBB78:
	.loc 1 308 0 discriminator 2
	call	rand
.LVL18:
	cvtsi2sd	%eax, %xmm0
	mulsd	.LC3(%rip), %xmm0
	.loc 1 309 0 discriminator 2
	mulsd	.LC4(%rip), %xmm0
	cvttsd2si	%xmm0, %eax
.LVL19:
	movl	%eax, 0(%rbp,%rbx)
	addq	$4, %rbx
.LBE78:
	.loc 1 307 0 discriminator 2
	cmpq	$400000000, %rbx
	jne	.L10
	.loc 1 312 0
	addq	$8, %rsp
.LCFI17:
	.cfi_def_cfa_offset 24
	xorl	%eax, %eax
	popq	%rbx
.LCFI18:
	.cfi_def_cfa_offset 16
	popq	%rbp
.LCFI19:
	.cfi_def_cfa_offset 8
.LVL20:
	ret
	.cfi_endproc
.LFE93:
	.size	createOffsets, .-createOffsets
	.section	.rodata.str1.1
.LC5:
	.string	"Opening performance counter"
.LC6:
	.string	"argument error!"
.LC7:
	.string	"Loop start!"
.LC9:
	.string	"branch_mispred.c"
.LC10:
	.string	"rc"
.LC11:
	.string	"Loop stop!"
.LC12:
	.string	"Elapsed time: %.9lf seconds\n"
.LC13:
	.string	"CPU Cycles:           %lu \n"
.LC14:
	.string	"Instructions:         %lu \n"
.LC15:
	.string	"IPC:                  %lf\n"
.LC16:
	.string	"Branch misses:        %lu \n"
.LC17:
	.string	"Branch instructions:  %lu \n"
.LC19:
	.string	"Branch mispred. rate: %lf%%\n"
.LC21:
	.string	"overall selectivity = %10.9f\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC22:
	.string	"theoretical selectivity = %10.9f\n"
	.section	.rodata.str1.1
.LC23:
	.string	"w"
.LC24:
	.string	"/dev/null"
.LC25:
	.string	"%d "
	.section	.text.startup,"ax",@progbits
	.p2align 4,,15
	.globl	main
	.type	main, @function
main:
.LFB90:
	.loc 1 52 0
	.cfi_startproc
.LVL21:
	pushq	%r15
.LCFI20:
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	pushq	%r14
.LCFI21:
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	pushq	%r13
.LCFI22:
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	pushq	%r12
.LCFI23:
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	pushq	%rbp
.LCFI24:
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	movl	%edi, %ebp
	pushq	%rbx
.LCFI25:
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$18296, %rsp
.LCFI26:
	.cfi_def_cfa_offset 18352
	.loc 1 64 0
	movl	$0, attr(%rip)
	.loc 1 66 0
	andb	$-2, attr+40(%rip)
	.loc 1 52 0
	movq	%fs:40, %rax
	movq	%rax, 18280(%rsp)
	xorl	%eax, %eax
.LVL22:
	.loc 1 65 0
	movq	$0, attr+8(%rip)
	.loc 1 67 0
	call	getpid
.LVL23:
	movl	$-1, %ecx
	xorl	%r8d, %r8d
	movl	%eax, %esi
	movl	%ecx, %edx
	movl	$attr, %edi
	call	perf_event_open
	.loc 1 68 0
	testl	%eax, %eax
	.loc 1 67 0
	movl	%eax, %r13d
.LVL24:
	.loc 1 68 0
	js	.L52
.LVL25:
.L13:
	.loc 1 74 0
	andb	$-2, attr+112(%rip)
	.loc 1 72 0
	movl	$0, attr+72(%rip)
	.loc 1 73 0
	movq	$5, attr+80(%rip)
	.loc 1 75 0
	call	getpid
	movl	$-1, %ecx
	xorl	%r8d, %r8d
	movl	%eax, %esi
	movl	%ecx, %edx
	movl	$attr+72, %edi
	call	perf_event_open
	.loc 1 76 0
	testl	%eax, %eax
	.loc 1 75 0
	movl	%eax, 16(%rsp)
.LVL26:
	.loc 1 76 0
	js	.L53
.LVL27:
.L14:
	.loc 1 82 0
	andb	$-2, attr+184(%rip)
	.loc 1 80 0
	movl	$0, attr+144(%rip)
	.loc 1 81 0
	movq	$4, attr+152(%rip)
	.loc 1 83 0
	call	getpid
	movl	$-1, %ecx
	xorl	%r8d, %r8d
	movl	%eax, %esi
	movl	%ecx, %edx
	movl	$attr+144, %edi
	call	perf_event_open
	.loc 1 84 0
	testl	%eax, %eax
	.loc 1 83 0
	movl	%eax, 24(%rsp)
.LVL28:
	.loc 1 84 0
	js	.L54
.LVL29:
.L15:
	.loc 1 90 0
	andb	$-2, attr+256(%rip)
	.loc 1 88 0
	movl	$0, attr+216(%rip)
	.loc 1 89 0
	movq	$1, attr+224(%rip)
	.loc 1 91 0
	call	getpid
	movl	$-1, %ecx
	xorl	%r8d, %r8d
	movl	%eax, %esi
	movl	%ecx, %edx
	movl	$attr+216, %edi
	call	perf_event_open
	.loc 1 92 0
	testl	%eax, %eax
	.loc 1 91 0
	movl	%eax, 36(%rsp)
.LVL30:
	.loc 1 92 0
	js	.L55
.LVL31:
.L16:
	.loc 1 98 0
	andb	$-2, attr+328(%rip)
	.loc 1 96 0
	movl	$0, attr+288(%rip)
	.loc 1 97 0
	movq	$1, attr+296(%rip)
	.loc 1 99 0
	call	getpid
	movl	$-1, %ecx
	xorl	%r8d, %r8d
	movl	%eax, %esi
	movl	%ecx, %edx
	movl	$attr+288, %edi
	call	perf_event_open
	.loc 1 100 0
	testl	%eax, %eax
	.loc 1 99 0
	movl	%eax, 40(%rsp)
.LVL32:
	.loc 1 100 0
	js	.L56
.LVL33:
.L17:
	.loc 1 106 0
	andb	$-2, attr+400(%rip)
	.loc 1 104 0
	movl	$0, attr+360(%rip)
	.loc 1 105 0
	movq	$1, attr+368(%rip)
	.loc 1 107 0
	call	getpid
	movl	$-1, %ecx
	xorl	%r8d, %r8d
	movl	%eax, %esi
	movl	%ecx, %edx
	movl	$attr+360, %edi
	call	perf_event_open
	.loc 1 108 0
	testl	%eax, %eax
	.loc 1 107 0
	movl	%eax, 44(%rsp)
.LVL34:
	.loc 1 108 0
	js	.L57
.LVL35:
.L18:
	.loc 1 113 0
	cmpl	$7, %ebp
	jne	.L58
.LVL36:
.LBB79:
.LBB80:
	.file 3 "/usr/include/stdlib.h"
	.loc 3 281 0
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	call	strtod
.LVL37:
.LBE80:
.LBE79:
.LBB82:
.LBB83:
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
.LBE83:
.LBE82:
.LBB86:
.LBB81:
	movsd	%xmm0, 72(%rsp)
.LVL38:
.LBE81:
.LBE86:
.LBB87:
.LBB84:
	call	strtod
.LVL39:
.LBE84:
.LBE87:
.LBB88:
.LBB89:
	movq	24(%rbx), %rdi
	xorl	%esi, %esi
.LBE89:
.LBE88:
.LBB92:
.LBB85:
	movsd	%xmm0, 80(%rsp)
.LVL40:
.LBE85:
.LBE92:
.LBB93:
.LBB90:
	call	strtod
.LVL41:
.LBE90:
.LBE93:
.LBB94:
.LBB95:
	movq	32(%rbx), %rdi
	xorl	%esi, %esi
.LBE95:
.LBE94:
.LBB98:
.LBB91:
	movsd	%xmm0, 88(%rsp)
.LVL42:
.LBE91:
.LBE98:
.LBB99:
.LBB96:
	call	strtod
.LVL43:
.LBE96:
.LBE99:
.LBB100:
.LBB101:
	movq	40(%rbx), %rdi
	xorl	%esi, %esi
.LBE101:
.LBE100:
.LBB104:
.LBB97:
	movsd	%xmm0, 96(%rsp)
.LVL44:
.LBE97:
.LBE104:
.LBB105:
.LBB102:
	call	strtod
.LVL45:
.LBE102:
.LBE105:
.LBB106:
.LBB107:
	movq	48(%rbx), %rdi
	xorl	%esi, %esi
.LBE107:
.LBE106:
.LBB110:
.LBB103:
	movsd	%xmm0, 104(%rsp)
.LVL46:
.LBE103:
.LBE110:
.LBB111:
.LBB108:
	call	strtod
.LVL47:
.LBE108:
.LBE111:
	.loc 1 127 0
	movl	$400000000, %edi
.LBB112:
.LBB109:
	.loc 3 281 0
	movsd	%xmm0, 112(%rsp)
.LBE109:
.LBE112:
	.loc 1 127 0
	call	malloc
	.loc 1 128 0
	movl	$400000000, %edi
	.loc 1 127 0
	movq	%rax, %r15
.LVL48:
	.loc 1 128 0
	call	malloc
.LVL49:
	.loc 1 129 0
	movl	$400000000, %edi
	.loc 1 128 0
	movq	%rax, 48(%rsp)
.LVL50:
	.loc 1 129 0
	call	malloc
.LVL51:
	.loc 1 130 0
	movl	$400000000, %edi
	.loc 1 129 0
	movq	%rax, 56(%rsp)
.LVL52:
	.loc 1 130 0
	call	malloc
.LVL53:
	.loc 1 131 0
	movl	$400000000, %edi
	.loc 1 130 0
	movq	%rax, 64(%rsp)
.LVL54:
	.loc 1 131 0
	call	malloc
.LVL55:
	.loc 1 132 0
	movl	$400000000, %edi
	.loc 1 131 0
	movq	%rax, %r12
.LVL56:
	.loc 1 132 0
	call	malloc
.LVL57:
	.loc 1 133 0
	movl	$400000000, %edi
	.loc 1 132 0
	movq	%rax, %rbx
.LVL58:
	.loc 1 133 0
	call	malloc
.LVL59:
	.loc 1 136 0
	xorl	%edi, %edi
	.loc 1 133 0
	movq	%rax, %r14
.LVL60:
	.loc 1 136 0
	call	time
.LVL61:
	.loc 1 137 0
	movl	%eax, %edi
	call	srand
.LVL62:
	.loc 1 140 0
	leaq	224(%rsp), %rdi
	movsd	72(%rsp), %xmm0
	call	createData
	.loc 1 141 0
	leaq	3232(%rsp), %rdi
	movsd	80(%rsp), %xmm0
	call	createData
	.loc 1 142 0
	leaq	6240(%rsp), %rdi
	movsd	88(%rsp), %xmm0
	call	createData
	.loc 1 143 0
	leaq	9248(%rsp), %rdi
	movsd	96(%rsp), %xmm0
	call	createData
	.loc 1 144 0
	leaq	12256(%rsp), %rdi
	movsd	104(%rsp), %xmm0
	call	createData
	.loc 1 145 0
	movsd	112(%rsp), %xmm0
	leaq	15264(%rsp), %rdi
	call	createData
	.loc 1 149 0
	movq	%r15, %rdi
	call	createOffsets
	.loc 1 150 0
	movq	48(%rsp), %rdi
	call	createOffsets
	.loc 1 151 0
	movq	56(%rsp), %rdi
	call	createOffsets
	.loc 1 152 0
	movq	64(%rsp), %rdi
	call	createOffsets
	.loc 1 153 0
	movq	%r12, %rdi
	call	createOffsets
	.loc 1 154 0
	movq	%rbx, %rdi
	call	createOffsets
.LVL63:
	.loc 2 105 0
	movl	$.LC7, %edi
	call	puts
	.loc 1 158 0
	movq	stdout(%rip), %rdi
	call	fflush
	.loc 1 159 0
	xorpd	%xmm0, %xmm0
	call	get_timestamp
	movsd	%xmm0, 120(%rsp)
.LVL64:
	.loc 1 162 0
#APP
# 162 "branch_mispred.c" 1
	nop;
# 0 "" 2
.LVL65:
#NO_APP
.LBB113:
.LBB114:
	.file 4 "/usr/include/x86_64-linux-gnu/bits/unistd.h"
	.loc 4 45 0
	leaq	128(%rsp), %rsi
	movl	$8, %edx
	movl	%r13d, %edi
	call	read
.LVL66:
.LBE114:
.LBE113:
	.loc 1 163 0
	testl	%eax, %eax
	je	.L59
.LVL67:
.LBB115:
.LBB116:
	.loc 4 45 0
	movl	16(%rsp), %edi
	.loc 1 164 0
	leaq	136(%rsp), %rsi
	.loc 4 45 0
	movl	$8, %edx
	call	read
.LVL68:
.LBE116:
.LBE115:
	.loc 1 164 0
	testl	%eax, %eax
	je	.L60
.LVL69:
.LBB117:
.LBB118:
	.loc 4 45 0
	movl	24(%rsp), %edi
	.loc 1 165 0
	leaq	144(%rsp), %rsi
	.loc 4 45 0
	movl	$8, %edx
	call	read
.LVL70:
.LBE118:
.LBE117:
	.loc 1 165 0
	testl	%eax, %eax
	je	.L61
.LVL71:
.LBB119:
.LBB120:
	.loc 4 45 0
	movl	36(%rsp), %edi
	.loc 1 166 0
	leaq	152(%rsp), %rsi
	.loc 4 45 0
	movl	$8, %edx
	call	read
.LVL72:
.LBE120:
.LBE119:
	.loc 1 166 0
	testl	%eax, %eax
	je	.L62
.LVL73:
.LBB121:
.LBB122:
	.loc 4 45 0
	movl	40(%rsp), %edi
	.loc 1 167 0
	leaq	160(%rsp), %rsi
	.loc 4 45 0
	movl	$8, %edx
	call	read
.LVL74:
.LBE122:
.LBE121:
	.loc 1 167 0
	testl	%eax, %eax
	je	.L63
.LVL75:
.LBB123:
.LBB124:
	.loc 4 45 0
	movl	44(%rsp), %edi
	.loc 1 168 0
	leaq	168(%rsp), %rsi
	.loc 4 45 0
	movl	$8, %edx
	call	read
.LVL76:
.LBE124:
.LBE123:
	.loc 1 168 0
	testl	%eax, %eax
	je	.L64
	.loc 1 169 0
#APP
# 169 "branch_mispred.c" 1
	nop;
# 0 "" 2
	.loc 1 55 0
#NO_APP
	movq	48(%rsp), %rsi
	movq	56(%rsp), %rdi
	.loc 1 169 0
	xorl	%eax, %eax
.LVL77:
	.loc 1 55 0
	movq	64(%rsp), %r8
	xorl	%ebp, %ebp
.LVL78:
	.p2align 4,,10
	.p2align 3
.L28:
	.loc 1 184 0
	movslq	(%rbx,%rax,4), %rdx
	cmpb	$0, 15264(%rsp,%rdx)
	je	.L27
	.loc 1 184 0 is_stmt 0 discriminator 1
	movslq	(%r12,%rax,4), %rdx
	cmpb	$0, 12256(%rsp,%rdx)
	je	.L27
	.loc 1 185 0 is_stmt 1
	movslq	%ebp, %rdx
	.loc 1 186 0
	movslq	(%r15,%rax,4), %rcx
	.loc 1 185 0
	movl	%eax, (%r14,%rdx,4)
	.loc 1 186 0
	movslq	(%rsi,%rax,4), %rdx
	movzbl	3232(%rsp,%rdx), %edx
	andb	224(%rsp,%rcx), %dl
	movslq	(%rdi,%rax,4), %rcx
	andb	6240(%rsp,%rcx), %dl
	movslq	(%r8,%rax,4), %rcx
	andb	9248(%rsp,%rcx), %dl
	movsbl	%dl, %edx
	addl	%edx, %ebp
.LVL79:
.L27:
	addq	$1, %rax
	.loc 1 174 0
	cmpq	$100000000, %rax
	jne	.L28
	.loc 1 207 0
#APP
# 207 "branch_mispred.c" 1
	nop;
# 0 "" 2
.LVL80:
#NO_APP
.LBB125:
.LBB126:
	.loc 4 45 0
	leaq	176(%rsp), %rsi
	movl	$8, %edx
	movl	%r13d, %edi
	call	read
.LVL81:
.LBE126:
.LBE125:
	.loc 1 208 0
	testl	%eax, %eax
	je	.L65
.LVL82:
.LBB127:
.LBB128:
	.loc 4 45 0
	movl	16(%rsp), %edi
	.loc 1 209 0
	leaq	184(%rsp), %rsi
	.loc 4 45 0
	movl	$8, %edx
	call	read
.LVL83:
.LBE128:
.LBE127:
	.loc 1 209 0
	testl	%eax, %eax
	je	.L66
.LVL84:
.LBB129:
.LBB130:
	.loc 4 45 0
	movl	24(%rsp), %edi
	.loc 1 210 0
	leaq	192(%rsp), %rsi
	.loc 4 45 0
	movl	$8, %edx
	call	read
.LVL85:
.LBE130:
.LBE129:
	.loc 1 210 0
	testl	%eax, %eax
	je	.L67
.LVL86:
.LBB131:
.LBB132:
	.loc 4 45 0
	movl	36(%rsp), %edi
	.loc 1 211 0
	leaq	200(%rsp), %rsi
	.loc 4 45 0
	movl	$8, %edx
	call	read
.LVL87:
.LBE132:
.LBE131:
	.loc 1 211 0
	testl	%eax, %eax
	je	.L68
.LVL88:
.LBB133:
.LBB134:
	.loc 4 45 0
	movl	40(%rsp), %edi
	.loc 1 212 0
	leaq	208(%rsp), %rsi
	.loc 4 45 0
	movl	$8, %edx
	call	read
.LVL89:
.LBE134:
.LBE133:
	.loc 1 212 0
	testl	%eax, %eax
	je	.L69
.LVL90:
.LBB135:
.LBB136:
	.loc 4 45 0
	movl	44(%rsp), %edi
	.loc 1 213 0
	leaq	216(%rsp), %rsi
	.loc 4 45 0
	movl	$8, %edx
	call	read
.LVL91:
.LBE136:
.LBE135:
	.loc 1 213 0
	testl	%eax, %eax
	je	.L70
	.loc 1 215 0
#APP
# 215 "branch_mispred.c" 1
	nop;
# 0 "" 2
	.loc 1 218 0
#NO_APP
	movl	%r13d, %edi
	call	close
.LVL92:
	.loc 1 219 0
	movl	16(%rsp), %edi
	call	close
	.loc 1 220 0
	movl	24(%rsp), %edi
	call	close
	.loc 1 221 0
	movl	36(%rsp), %edi
	call	close
	.loc 1 222 0
	movl	40(%rsp), %edi
	call	close
	.loc 1 223 0
	movl	44(%rsp), %edi
	call	close
	.loc 1 225 0
	movsd	120(%rsp), %xmm0
	call	get_timestamp
.LVL93:
	.loc 2 105 0
	movl	$.LC11, %edi
	movsd	%xmm0, (%rsp)
	call	puts
.LVL94:
.LBB137:
.LBB138:
	movsd	(%rsp), %xmm0
	movl	$.LC12, %esi
	movl	$1, %edi
	movl	$1, %eax
	call	__printf_chk
.LVL95:
.LBE138:
.LBE137:
	.loc 1 229 0
	movq	176(%rsp), %rdx
	subq	128(%rsp), %rdx
.LBB139:
.LBB140:
	.loc 2 105 0
	movl	$.LC13, %esi
	movl	$1, %edi
	xorl	%eax, %eax
	call	__printf_chk
.LVL96:
.LBE140:
.LBE139:
	.loc 1 230 0
	movq	200(%rsp), %rdx
	subq	152(%rsp), %rdx
.LBB141:
.LBB142:
	.loc 2 105 0
	xorl	%eax, %eax
	movl	$.LC14, %esi
	movl	$1, %edi
	call	__printf_chk
.LVL97:
.LBE142:
.LBE141:
	.loc 1 231 0
	movq	200(%rsp), %rax
	testq	%rax, %rax
	js	.L35
	cvtsi2sdq	%rax, %xmm0
.L36:
	movq	152(%rsp), %rax
	testq	%rax, %rax
	js	.L37
	cvtsi2sdq	%rax, %xmm1
.L38:
	movq	176(%rsp), %rax
	subq	128(%rsp), %rax
	subsd	%xmm1, %xmm0
	js	.L39
	cvtsi2sdq	%rax, %xmm1
.L40:
	divsd	%xmm1, %xmm0
.LBB143:
.LBB144:
	.loc 2 105 0
	movl	$.LC15, %esi
	movl	$1, %edi
	movl	$1, %eax
	call	__printf_chk
.LVL98:
.LBE144:
.LBE143:
	.loc 1 232 0
	movq	184(%rsp), %rdx
	subq	136(%rsp), %rdx
.LBB145:
.LBB146:
	.loc 2 105 0
	movl	$.LC16, %esi
	movl	$1, %edi
	xorl	%eax, %eax
	call	__printf_chk
.LVL99:
.LBE146:
.LBE145:
	.loc 1 233 0
	movq	192(%rsp), %rdx
	subq	144(%rsp), %rdx
.LBB147:
.LBB148:
	.loc 2 105 0
	xorl	%eax, %eax
	movl	$.LC17, %esi
	movl	$1, %edi
	call	__printf_chk
.LVL100:
.LBE148:
.LBE147:
	.loc 1 234 0
	movq	184(%rsp), %rax
	testq	%rax, %rax
	js	.L41
	cvtsi2sdq	%rax, %xmm0
.L42:
	movq	136(%rsp), %rax
	testq	%rax, %rax
	js	.L43
	cvtsi2sdq	%rax, %xmm1
.L44:
	subsd	%xmm1, %xmm0
	movq	192(%rsp), %rax
	subq	144(%rsp), %rax
	mulsd	.LC18(%rip), %xmm0
	js	.L45
	cvtsi2sdq	%rax, %xmm1
.L46:
	divsd	%xmm1, %xmm0
.LBB149:
.LBB150:
	.loc 2 105 0
	movl	$.LC19, %esi
	movl	$1, %edi
	movl	$1, %eax
	call	__printf_chk
.LVL101:
	movl	$10, %edi
	call	putchar
.LVL102:
.LBE150:
.LBE149:
	.loc 1 236 0
	cvtsi2sd	%ebp, %xmm0
.LBB151:
.LBB152:
	.loc 2 105 0
	movl	$.LC21, %esi
	movl	$1, %edi
	movl	$1, %eax
.LBE152:
.LBE151:
	.loc 1 236 0
	divsd	.LC20(%rip), %xmm0
.LBB154:
.LBB153:
	.loc 2 105 0
	call	__printf_chk
.LVL103:
.LBE153:
.LBE154:
	.loc 1 237 0
	movsd	72(%rsp), %xmm0
.LBB155:
.LBB156:
	.loc 2 105 0
	movl	$.LC22, %esi
	movl	$1, %edi
	movl	$1, %eax
.LBE156:
.LBE155:
	.loc 1 237 0
	mulsd	80(%rsp), %xmm0
	mulsd	88(%rsp), %xmm0
	mulsd	96(%rsp), %xmm0
	mulsd	104(%rsp), %xmm0
	mulsd	112(%rsp), %xmm0
.LBB158:
.LBB157:
	.loc 2 105 0
	call	__printf_chk
.LBE157:
.LBE158:
	.loc 1 241 0
	movl	$.LC23, %esi
	movl	$.LC24, %edi
	call	fopen
	.loc 1 242 0
	testl	%ebp, %ebp
	.loc 1 241 0
	movq	%rax, 16(%rsp)
.LVL104:
	.loc 1 242 0
	jle	.L47
	movq	%r15, 24(%rsp)
	xorl	%r13d, %r13d
.LVL105:
	movq	%r12, %r15
.LVL106:
	movq	%rax, %r12
.LVL107:
	.p2align 4,,10
	.p2align 3
.L48:
.LBB159:
.LBB160:
	.loc 2 98 0 discriminator 2
	movl	(%r14,%r13,4), %ecx
	xorl	%eax, %eax
	movl	$.LC25, %edx
	movl	$1, %esi
	movq	%r12, %rdi
	addq	$1, %r13
	call	__fprintf_chk
.LBE160:
.LBE159:
	.loc 1 242 0 discriminator 2
	cmpl	%r13d, %ebp
	jg	.L48
	movq	%r15, %r12
.LVL108:
	movq	24(%rsp), %r15
.LVL109:
.L47:
	.loc 1 245 0
	movq	16(%rsp), %rdi
	call	fclose
	.loc 1 248 0
	movq	%r15, %rdi
	call	free
	.loc 1 249 0
	movq	48(%rsp), %rdi
	call	free
	.loc 1 250 0
	movq	56(%rsp), %rdi
	call	free
	.loc 1 251 0
	movq	64(%rsp), %rdi
	call	free
	.loc 1 252 0
	movq	%r12, %rdi
	call	free
	.loc 1 253 0
	movq	%rbx, %rdi
	call	free
	.loc 1 255 0
	movq	%r14, %rdi
	call	free
	.loc 1 257 0
	xorl	%eax, %eax
.LVL110:
.L20:
	.loc 1 258 0
	movq	18280(%rsp), %rdx
	xorq	%fs:40, %rdx
	jne	.L71
	addq	$18296, %rsp
	.cfi_remember_state
.LCFI27:
	.cfi_def_cfa_offset 56
	popq	%rbx
.LCFI28:
	.cfi_def_cfa_offset 48
	popq	%rbp
.LCFI29:
	.cfi_def_cfa_offset 40
	popq	%r12
.LCFI30:
	.cfi_def_cfa_offset 32
	popq	%r13
.LCFI31:
	.cfi_def_cfa_offset 24
	popq	%r14
.LCFI32:
	.cfi_def_cfa_offset 16
	popq	%r15
.LCFI33:
	.cfi_def_cfa_offset 8
	ret
.LVL111:
.L35:
.LCFI34:
	.cfi_restore_state
	.loc 1 231 0
	movq	%rax, %rdx
	andl	$1, %eax
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L36
.L37:
	movq	%rax, %rdx
	andl	$1, %eax
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L38
.L39:
	movq	%rax, %rdx
	andl	$1, %eax
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L40
.LVL112:
.L41:
	.loc 1 234 0
	movq	%rax, %rdx
	andl	$1, %eax
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L42
.L43:
	movq	%rax, %rdx
	andl	$1, %eax
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L44
.L45:
	movq	%rax, %rdx
	andl	$1, %eax
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L46
.LVL113:
.L53:
	.loc 1 77 0
	movl	$.LC5, %edi
	call	perror
	jmp	.L14
.LVL114:
.L57:
	.loc 1 109 0
	movl	$.LC5, %edi
	call	perror
.LVL115:
	.p2align 4,,3
	jmp	.L18
.LVL116:
.L52:
	.loc 1 69 0
	movl	$.LC5, %edi
	call	perror
	.p2align 4,,3
	jmp	.L13
.LVL117:
.L54:
	.loc 1 85 0
	movl	$.LC5, %edi
	call	perror
.LVL118:
	.p2align 4,,3
	jmp	.L15
.LVL119:
.L55:
	.loc 1 93 0
	movl	$.LC5, %edi
	call	perror
.LVL120:
	.p2align 4,,3
	jmp	.L16
.LVL121:
.L56:
	.loc 1 101 0
	movl	$.LC5, %edi
	call	perror
.LVL122:
	.p2align 4,,3
	jmp	.L17
.LVL123:
.L58:
	.loc 2 105 0
	movl	$.LC6, %edi
	call	puts
	.loc 1 115 0
	orl	$-1, %eax
	jmp	.L20
.LVL124:
.L62:
	.loc 1 166 0 discriminator 1
	movl	$__PRETTY_FUNCTION__.4772, %ecx
	movl	$166, %edx
	movl	$.LC9, %esi
	movl	$.LC10, %edi
	call	__assert_fail
.LVL125:
.L60:
	.loc 1 164 0 discriminator 1
	movl	$__PRETTY_FUNCTION__.4772, %ecx
	movl	$164, %edx
	movl	$.LC9, %esi
	movl	$.LC10, %edi
	call	__assert_fail
.LVL126:
.L69:
	.loc 1 212 0 discriminator 1
	movl	$__PRETTY_FUNCTION__.4772, %ecx
	movl	$212, %edx
	movl	$.LC9, %esi
	movl	$.LC10, %edi
	call	__assert_fail
.LVL127:
.L68:
	.loc 1 211 0 discriminator 1
	movl	$__PRETTY_FUNCTION__.4772, %ecx
	movl	$211, %edx
	movl	$.LC9, %esi
	movl	$.LC10, %edi
	call	__assert_fail
.LVL128:
.L67:
	.loc 1 210 0 discriminator 1
	movl	$__PRETTY_FUNCTION__.4772, %ecx
	movl	$210, %edx
	movl	$.LC9, %esi
	movl	$.LC10, %edi
	call	__assert_fail
.LVL129:
.L66:
	.loc 1 209 0 discriminator 1
	movl	$__PRETTY_FUNCTION__.4772, %ecx
	movl	$209, %edx
	movl	$.LC9, %esi
	movl	$.LC10, %edi
	call	__assert_fail
.LVL130:
.L65:
	.loc 1 208 0 discriminator 1
	movl	$__PRETTY_FUNCTION__.4772, %ecx
	movl	$208, %edx
	movl	$.LC9, %esi
	movl	$.LC10, %edi
	call	__assert_fail
.LVL131:
.L64:
	.loc 1 168 0 discriminator 1
	movl	$__PRETTY_FUNCTION__.4772, %ecx
	movl	$168, %edx
	movl	$.LC9, %esi
	movl	$.LC10, %edi
	call	__assert_fail
.LVL132:
.L63:
	.loc 1 167 0 discriminator 1
	movl	$__PRETTY_FUNCTION__.4772, %ecx
	movl	$167, %edx
	movl	$.LC9, %esi
	movl	$.LC10, %edi
	call	__assert_fail
.LVL133:
.L59:
	.loc 1 163 0 discriminator 1
	movl	$__PRETTY_FUNCTION__.4772, %ecx
	movl	$163, %edx
	movl	$.LC9, %esi
	movl	$.LC10, %edi
	call	__assert_fail
.LVL134:
.L70:
	.loc 1 213 0 discriminator 1
	movl	$__PRETTY_FUNCTION__.4772, %ecx
	movl	$213, %edx
	movl	$.LC9, %esi
	movl	$.LC10, %edi
	call	__assert_fail
.LVL135:
.L71:
	.loc 1 258 0
	call	__stack_chk_fail
.LVL136:
.L61:
	.loc 1 165 0 discriminator 1
	movl	$__PRETTY_FUNCTION__.4772, %ecx
	movl	$165, %edx
	movl	$.LC9, %esi
	movl	$.LC10, %edi
	call	__assert_fail
.LVL137:
	.cfi_endproc
.LFE90:
	.size	main, .-main
	.comm	attr,432,32
	.section	.rodata
	.type	__PRETTY_FUNCTION__.4772, @object
	.size	__PRETTY_FUNCTION__.4772, 5
__PRETTY_FUNCTION__.4772:
	.string	"main"
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	-1074790400
	.align 8
.LC1:
	.long	2696277389
	.long	1051772663
	.align 8
.LC3:
	.long	0
	.long	1040187392
	.align 8
.LC4:
	.long	0
	.long	1084715008
	.align 8
.LC18:
	.long	0
	.long	1079574528
	.align 8
.LC20:
	.long	0
	.long	1100470148
	.text
.Letext0:
	.file 5 "/usr/lib/gcc/x86_64-linux-gnu/4.6/include/stddef.h"
	.file 6 "/usr/include/x86_64-linux-gnu/bits/types.h"
	.file 7 "/usr/include/stdio.h"
	.file 8 "/usr/include/libio.h"
	.file 9 "/usr/include/x86_64-linux-gnu/sys/types.h"
	.file 10 "/usr/include/x86_64-linux-gnu/bits/time.h"
	.file 11 "/usr/include/stdint.h"
	.file 12 "/lib/modules/3.2.0-77-generic/build/include/asm-generic/int-ll64.h"
	.file 13 "/lib/modules/3.2.0-77-generic/build/include/linux/perf_event.h"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.long	0x1057
	.value	0x2
	.long	.Ldebug_abbrev0
	.byte	0x8
	.uleb128 0x1
	.long	.LASF147
	.byte	0x1
	.long	.LASF148
	.long	.LASF149
	.quad	0
	.quad	0
	.long	.Ldebug_ranges0+0x1d0
	.long	.Ldebug_line0
	.uleb128 0x2
	.long	.LASF7
	.byte	0x5
	.byte	0xd4
	.long	0x3c
	.uleb128 0x3
	.byte	0x8
	.byte	0x7
	.long	.LASF0
	.uleb128 0x3
	.byte	0x1
	.byte	0x8
	.long	.LASF1
	.uleb128 0x3
	.byte	0x2
	.byte	0x7
	.long	.LASF2
	.uleb128 0x3
	.byte	0x4
	.byte	0x7
	.long	.LASF3
	.uleb128 0x3
	.byte	0x1
	.byte	0x6
	.long	.LASF4
	.uleb128 0x3
	.byte	0x2
	.byte	0x5
	.long	.LASF5
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.string	"int"
	.uleb128 0x3
	.byte	0x8
	.byte	0x5
	.long	.LASF6
	.uleb128 0x2
	.long	.LASF8
	.byte	0x6
	.byte	0x8d
	.long	0x6d
	.uleb128 0x2
	.long	.LASF9
	.byte	0x6
	.byte	0x8e
	.long	0x6d
	.uleb128 0x2
	.long	.LASF10
	.byte	0x6
	.byte	0x8f
	.long	0x66
	.uleb128 0x2
	.long	.LASF11
	.byte	0x6
	.byte	0x95
	.long	0x6d
	.uleb128 0x2
	.long	.LASF12
	.byte	0x6
	.byte	0x97
	.long	0x6d
	.uleb128 0x5
	.byte	0x8
	.uleb128 0x2
	.long	.LASF13
	.byte	0x6
	.byte	0xb4
	.long	0x6d
	.uleb128 0x6
	.byte	0x8
	.long	0xbe
	.uleb128 0x3
	.byte	0x1
	.byte	0x6
	.long	.LASF14
	.uleb128 0x2
	.long	.LASF15
	.byte	0x7
	.byte	0x31
	.long	0xd0
	.uleb128 0x7
	.long	.LASF45
	.byte	0xd8
	.byte	0x8
	.value	0x111
	.long	0x29d
	.uleb128 0x8
	.long	.LASF16
	.byte	0x8
	.value	0x112
	.long	0x66
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x8
	.long	.LASF17
	.byte	0x8
	.value	0x117
	.long	0xb8
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x8
	.long	.LASF18
	.byte	0x8
	.value	0x118
	.long	0xb8
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0x8
	.long	.LASF19
	.byte	0x8
	.value	0x119
	.long	0xb8
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0x8
	.long	.LASF20
	.byte	0x8
	.value	0x11a
	.long	0xb8
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x8
	.long	.LASF21
	.byte	0x8
	.value	0x11b
	.long	0xb8
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x8
	.long	.LASF22
	.byte	0x8
	.value	0x11c
	.long	0xb8
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0x8
	.long	.LASF23
	.byte	0x8
	.value	0x11d
	.long	0xb8
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0x8
	.long	.LASF24
	.byte	0x8
	.value	0x11e
	.long	0xb8
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.uleb128 0x8
	.long	.LASF25
	.byte	0x8
	.value	0x120
	.long	0xb8
	.byte	0x2
	.byte	0x23
	.uleb128 0x48
	.uleb128 0x8
	.long	.LASF26
	.byte	0x8
	.value	0x121
	.long	0xb8
	.byte	0x2
	.byte	0x23
	.uleb128 0x50
	.uleb128 0x8
	.long	.LASF27
	.byte	0x8
	.value	0x122
	.long	0xb8
	.byte	0x2
	.byte	0x23
	.uleb128 0x58
	.uleb128 0x8
	.long	.LASF28
	.byte	0x8
	.value	0x124
	.long	0x2db
	.byte	0x2
	.byte	0x23
	.uleb128 0x60
	.uleb128 0x8
	.long	.LASF29
	.byte	0x8
	.value	0x126
	.long	0x2e1
	.byte	0x2
	.byte	0x23
	.uleb128 0x68
	.uleb128 0x8
	.long	.LASF30
	.byte	0x8
	.value	0x128
	.long	0x66
	.byte	0x2
	.byte	0x23
	.uleb128 0x70
	.uleb128 0x8
	.long	.LASF31
	.byte	0x8
	.value	0x12c
	.long	0x66
	.byte	0x2
	.byte	0x23
	.uleb128 0x74
	.uleb128 0x8
	.long	.LASF32
	.byte	0x8
	.value	0x12e
	.long	0x74
	.byte	0x2
	.byte	0x23
	.uleb128 0x78
	.uleb128 0x8
	.long	.LASF33
	.byte	0x8
	.value	0x132
	.long	0x4a
	.byte	0x3
	.byte	0x23
	.uleb128 0x80
	.uleb128 0x8
	.long	.LASF34
	.byte	0x8
	.value	0x133
	.long	0x58
	.byte	0x3
	.byte	0x23
	.uleb128 0x82
	.uleb128 0x8
	.long	.LASF35
	.byte	0x8
	.value	0x134
	.long	0x2e7
	.byte	0x3
	.byte	0x23
	.uleb128 0x83
	.uleb128 0x8
	.long	.LASF36
	.byte	0x8
	.value	0x138
	.long	0x2f7
	.byte	0x3
	.byte	0x23
	.uleb128 0x88
	.uleb128 0x8
	.long	.LASF37
	.byte	0x8
	.value	0x141
	.long	0x7f
	.byte	0x3
	.byte	0x23
	.uleb128 0x90
	.uleb128 0x8
	.long	.LASF38
	.byte	0x8
	.value	0x14a
	.long	0xab
	.byte	0x3
	.byte	0x23
	.uleb128 0x98
	.uleb128 0x8
	.long	.LASF39
	.byte	0x8
	.value	0x14b
	.long	0xab
	.byte	0x3
	.byte	0x23
	.uleb128 0xa0
	.uleb128 0x8
	.long	.LASF40
	.byte	0x8
	.value	0x14c
	.long	0xab
	.byte	0x3
	.byte	0x23
	.uleb128 0xa8
	.uleb128 0x8
	.long	.LASF41
	.byte	0x8
	.value	0x14d
	.long	0xab
	.byte	0x3
	.byte	0x23
	.uleb128 0xb0
	.uleb128 0x8
	.long	.LASF42
	.byte	0x8
	.value	0x14e
	.long	0x31
	.byte	0x3
	.byte	0x23
	.uleb128 0xb8
	.uleb128 0x8
	.long	.LASF43
	.byte	0x8
	.value	0x150
	.long	0x66
	.byte	0x3
	.byte	0x23
	.uleb128 0xc0
	.uleb128 0x8
	.long	.LASF44
	.byte	0x8
	.value	0x152
	.long	0x2fd
	.byte	0x3
	.byte	0x23
	.uleb128 0xc4
	.byte	0
	.uleb128 0x9
	.long	.LASF150
	.byte	0x8
	.byte	0xb6
	.uleb128 0xa
	.long	.LASF46
	.byte	0x18
	.byte	0x8
	.byte	0xbc
	.long	0x2db
	.uleb128 0xb
	.long	.LASF47
	.byte	0x8
	.byte	0xbd
	.long	0x2db
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.long	.LASF48
	.byte	0x8
	.byte	0xbe
	.long	0x2e1
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0xb
	.long	.LASF49
	.byte	0x8
	.byte	0xc2
	.long	0x66
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0x2a4
	.uleb128 0x6
	.byte	0x8
	.long	0xd0
	.uleb128 0xc
	.long	0xbe
	.long	0x2f7
	.uleb128 0xd
	.long	0x3c
	.byte	0
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0x29d
	.uleb128 0xc
	.long	0xbe
	.long	0x30d
	.uleb128 0xd
	.long	0x3c
	.byte	0x13
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0x313
	.uleb128 0xe
	.long	0xbe
	.uleb128 0x2
	.long	.LASF50
	.byte	0x7
	.byte	0x67
	.long	0xad
	.uleb128 0x6
	.byte	0x8
	.long	0x66
	.uleb128 0x3
	.byte	0x8
	.byte	0x5
	.long	.LASF51
	.uleb128 0x2
	.long	.LASF52
	.byte	0x9
	.byte	0x63
	.long	0x8a
	.uleb128 0xa
	.long	.LASF53
	.byte	0x10
	.byte	0xa
	.byte	0x1f
	.long	0x364
	.uleb128 0xb
	.long	.LASF54
	.byte	0xa
	.byte	0x21
	.long	0x95
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.long	.LASF55
	.byte	0xa
	.byte	0x22
	.long	0xa0
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.byte	0x7
	.long	.LASF56
	.uleb128 0x2
	.long	.LASF57
	.byte	0xb
	.byte	0x38
	.long	0x3c
	.uleb128 0x2
	.long	.LASF58
	.byte	0xc
	.byte	0x1a
	.long	0x51
	.uleb128 0x2
	.long	.LASF59
	.byte	0xc
	.byte	0x1e
	.long	0x364
	.uleb128 0xf
	.long	.LASF67
	.byte	0x4
	.byte	0xd
	.byte	0x1c
	.long	0x3c3
	.uleb128 0x10
	.long	.LASF60
	.sleb128 0
	.uleb128 0x10
	.long	.LASF61
	.sleb128 1
	.uleb128 0x10
	.long	.LASF62
	.sleb128 2
	.uleb128 0x10
	.long	.LASF63
	.sleb128 3
	.uleb128 0x10
	.long	.LASF64
	.sleb128 4
	.uleb128 0x10
	.long	.LASF65
	.sleb128 5
	.uleb128 0x10
	.long	.LASF66
	.sleb128 6
	.byte	0
	.uleb128 0xf
	.long	.LASF68
	.byte	0x4
	.byte	0xd
	.byte	0x2c
	.long	0x40c
	.uleb128 0x10
	.long	.LASF69
	.sleb128 0
	.uleb128 0x10
	.long	.LASF70
	.sleb128 1
	.uleb128 0x10
	.long	.LASF71
	.sleb128 2
	.uleb128 0x10
	.long	.LASF72
	.sleb128 3
	.uleb128 0x10
	.long	.LASF73
	.sleb128 4
	.uleb128 0x10
	.long	.LASF74
	.sleb128 5
	.uleb128 0x10
	.long	.LASF75
	.sleb128 6
	.uleb128 0x10
	.long	.LASF76
	.sleb128 7
	.uleb128 0x10
	.long	.LASF77
	.sleb128 8
	.uleb128 0x10
	.long	.LASF78
	.sleb128 9
	.byte	0
	.uleb128 0x11
	.byte	0x8
	.byte	0xd
	.byte	0xba
	.long	0x42b
	.uleb128 0x12
	.long	.LASF79
	.byte	0xd
	.byte	0xbb
	.long	0x381
	.uleb128 0x12
	.long	.LASF80
	.byte	0xd
	.byte	0xbc
	.long	0x381
	.byte	0
	.uleb128 0x11
	.byte	0x4
	.byte	0xd
	.byte	0xe4
	.long	0x44a
	.uleb128 0x12
	.long	.LASF81
	.byte	0xd
	.byte	0xe5
	.long	0x376
	.uleb128 0x12
	.long	.LASF82
	.byte	0xd
	.byte	0xe6
	.long	0x376
	.byte	0
	.uleb128 0x11
	.byte	0x8
	.byte	0xd
	.byte	0xea
	.long	0x469
	.uleb128 0x12
	.long	.LASF83
	.byte	0xd
	.byte	0xeb
	.long	0x381
	.uleb128 0x12
	.long	.LASF84
	.byte	0xd
	.byte	0xec
	.long	0x381
	.byte	0
	.uleb128 0x11
	.byte	0x8
	.byte	0xd
	.byte	0xee
	.long	0x488
	.uleb128 0x12
	.long	.LASF85
	.byte	0xd
	.byte	0xef
	.long	0x381
	.uleb128 0x12
	.long	.LASF86
	.byte	0xd
	.byte	0xf0
	.long	0x381
	.byte	0
	.uleb128 0xa
	.long	.LASF87
	.byte	0x48
	.byte	0xd
	.byte	0xa9
	.long	0x66e
	.uleb128 0xb
	.long	.LASF88
	.byte	0xd
	.byte	0xae
	.long	0x376
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0xb
	.long	.LASF89
	.byte	0xd
	.byte	0xb3
	.long	0x376
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0xb
	.long	.LASF90
	.byte	0xd
	.byte	0xb8
	.long	0x381
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.uleb128 0x13
	.long	0x40c
	.byte	0x2
	.byte	0x23
	.uleb128 0x10
	.uleb128 0xb
	.long	.LASF91
	.byte	0xd
	.byte	0xbf
	.long	0x381
	.byte	0x2
	.byte	0x23
	.uleb128 0x18
	.uleb128 0xb
	.long	.LASF92
	.byte	0xd
	.byte	0xc0
	.long	0x381
	.byte	0x2
	.byte	0x23
	.uleb128 0x20
	.uleb128 0x14
	.long	.LASF93
	.byte	0xd
	.byte	0xc2
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x3f
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF94
	.byte	0xd
	.byte	0xc3
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x3e
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF95
	.byte	0xd
	.byte	0xc4
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x3d
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF96
	.byte	0xd
	.byte	0xc5
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x3c
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF97
	.byte	0xd
	.byte	0xc6
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x3b
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF98
	.byte	0xd
	.byte	0xc7
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x3a
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF99
	.byte	0xd
	.byte	0xc8
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x39
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF100
	.byte	0xd
	.byte	0xc9
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x38
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF101
	.byte	0xd
	.byte	0xca
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x37
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF102
	.byte	0xd
	.byte	0xcb
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x36
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF103
	.byte	0xd
	.byte	0xcc
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x35
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF104
	.byte	0xd
	.byte	0xcd
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x34
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF105
	.byte	0xd
	.byte	0xce
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x33
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF106
	.byte	0xd
	.byte	0xcf
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x32
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF107
	.byte	0xd
	.byte	0xd0
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x31
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF108
	.byte	0xd
	.byte	0xdb
	.long	0x381
	.byte	0x8
	.byte	0x2
	.byte	0x2f
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF109
	.byte	0xd
	.byte	0xdc
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x2e
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF110
	.byte	0xd
	.byte	0xdd
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x2d
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF111
	.byte	0xd
	.byte	0xdf
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x2c
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF112
	.byte	0xd
	.byte	0xe0
	.long	0x381
	.byte	0x8
	.byte	0x1
	.byte	0x2b
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x14
	.long	.LASF113
	.byte	0xd
	.byte	0xe2
	.long	0x381
	.byte	0x8
	.byte	0x2b
	.byte	0
	.byte	0x2
	.byte	0x23
	.uleb128 0x28
	.uleb128 0x13
	.long	0x42b
	.byte	0x2
	.byte	0x23
	.uleb128 0x30
	.uleb128 0xb
	.long	.LASF114
	.byte	0xd
	.byte	0xe9
	.long	0x376
	.byte	0x2
	.byte	0x23
	.uleb128 0x34
	.uleb128 0x13
	.long	0x44a
	.byte	0x2
	.byte	0x23
	.uleb128 0x38
	.uleb128 0x13
	.long	0x469
	.byte	0x2
	.byte	0x23
	.uleb128 0x40
	.byte	0
	.uleb128 0x15
	.byte	0x1
	.long	.LASF121
	.byte	0x2
	.byte	0x67
	.byte	0x1
	.long	0x66
	.byte	0x3
	.byte	0x1
	.long	0x68e
	.uleb128 0x16
	.long	.LASF115
	.byte	0x2
	.byte	0x67
	.long	0x30d
	.uleb128 0x17
	.byte	0
	.uleb128 0x18
	.byte	0x1
	.long	.LASF118
	.byte	0x1
	.value	0x109
	.byte	0x1
	.long	0x6b9
	.byte	0x1
	.long	0x6b9
	.uleb128 0x19
	.long	.LASF116
	.byte	0x1
	.value	0x109
	.long	0x6b9
	.uleb128 0x1a
	.string	"tp"
	.byte	0x1
	.value	0x10a
	.long	0x33b
	.byte	0
	.uleb128 0x3
	.byte	0x8
	.byte	0x4
	.long	.LASF117
	.uleb128 0x18
	.byte	0x1
	.long	.LASF119
	.byte	0x3
	.value	0x117
	.byte	0x1
	.long	0x6b9
	.byte	0x3
	.long	0x6e0
	.uleb128 0x19
	.long	.LASF120
	.byte	0x3
	.value	0x117
	.long	0x30d
	.byte	0
	.uleb128 0x15
	.byte	0x1
	.long	.LASF122
	.byte	0x4
	.byte	0x23
	.byte	0x1
	.long	0x318
	.byte	0x3
	.byte	0x1
	.long	0x715
	.uleb128 0x16
	.long	.LASF123
	.byte	0x4
	.byte	0x23
	.long	0x66
	.uleb128 0x16
	.long	.LASF124
	.byte	0x4
	.byte	0x23
	.long	0xab
	.uleb128 0x16
	.long	.LASF125
	.byte	0x4
	.byte	0x23
	.long	0x31
	.byte	0
	.uleb128 0x15
	.byte	0x1
	.long	.LASF126
	.byte	0x2
	.byte	0x60
	.byte	0x1
	.long	0x66
	.byte	0x3
	.byte	0x1
	.long	0x740
	.uleb128 0x16
	.long	.LASF127
	.byte	0x2
	.byte	0x60
	.long	0x740
	.uleb128 0x16
	.long	.LASF115
	.byte	0x2
	.byte	0x60
	.long	0x30d
	.uleb128 0x17
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0xc5
	.uleb128 0x1b
	.byte	0x1
	.long	.LASF129
	.byte	0x1
	.byte	0x25
	.byte	0x1
	.long	0x66
	.quad	.LFB89
	.quad	.LFE89
	.long	.LLST0
	.long	0x7b6
	.uleb128 0x1c
	.string	"hw"
	.byte	0x1
	.byte	0x25
	.long	0x7b6
	.long	.LLST1
	.uleb128 0x1c
	.string	"pid"
	.byte	0x1
	.byte	0x26
	.long	0x330
	.long	.LLST2
	.uleb128 0x1c
	.string	"cpu"
	.byte	0x1
	.byte	0x26
	.long	0x66
	.long	.LLST3
	.uleb128 0x1c
	.string	"grp"
	.byte	0x1
	.byte	0x26
	.long	0x66
	.long	.LLST4
	.uleb128 0x1d
	.long	.LASF128
	.byte	0x1
	.byte	0x26
	.long	0x3c
	.long	.LLST5
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0x488
	.uleb128 0x1e
	.long	0x68e
	.quad	.LFB91
	.quad	.LFE91
	.long	.LLST6
	.long	0x849
	.uleb128 0x1f
	.long	0x6a1
	.long	.LLST7
	.uleb128 0x20
	.long	0x6ad
	.byte	0x2
	.byte	0x91
	.sleb128 -32
	.uleb128 0x21
	.long	0x68e
	.quad	.LBB74
	.quad	.LBE74
	.byte	0x1
	.value	0x109
	.uleb128 0x22
	.quad	.LBB75
	.quad	.LBE75
	.uleb128 0x23
	.long	0x6ad
	.uleb128 0x24
	.long	0x6a1
	.uleb128 0x21
	.long	0x66e
	.quad	.LBB76
	.quad	.LBE76
	.byte	0x1
	.value	0x10f
	.uleb128 0x25
	.long	0x681
	.byte	0xa
	.byte	0x3
	.quad	.LC2
	.byte	0x9f
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x26
	.byte	0x1
	.long	.LASF130
	.byte	0x1
	.value	0x11a
	.byte	0x1
	.long	0xb8
	.quad	.LFB92
	.quad	.LFE92
	.long	.LLST8
	.long	0x8ac
	.uleb128 0x27
	.long	.LASF131
	.byte	0x1
	.value	0x11a
	.long	0x6b9
	.long	.LLST9
	.uleb128 0x27
	.long	.LASF132
	.byte	0x1
	.value	0x11a
	.long	0xb8
	.long	.LLST10
	.uleb128 0x28
	.string	"i"
	.byte	0x1
	.value	0x11b
	.long	0x66
	.long	.LLST11
	.uleb128 0x28
	.string	"r"
	.byte	0x1
	.value	0x11c
	.long	0x6b9
	.long	.LLST12
	.byte	0
	.uleb128 0x26
	.byte	0x1
	.long	.LASF133
	.byte	0x1
	.value	0x131
	.byte	0x1
	.long	0x66
	.quad	.LFB93
	.quad	.LFE93
	.long	.LLST13
	.long	0x90d
	.uleb128 0x29
	.string	"off"
	.byte	0x1
	.value	0x131
	.long	0x323
	.long	.LLST14
	.uleb128 0x28
	.string	"i"
	.byte	0x1
	.value	0x132
	.long	0x66
	.long	.LLST15
	.uleb128 0x22
	.quad	.LBB78
	.quad	.LBE78
	.uleb128 0x1a
	.string	"r"
	.byte	0x1
	.value	0x134
	.long	0x6b9
	.byte	0
	.byte	0
	.uleb128 0x1b
	.byte	0x1
	.long	.LASF134
	.byte	0x1
	.byte	0x34
	.byte	0x1
	.long	0x66
	.quad	.LFB90
	.quad	.LFE90
	.long	.LLST16
	.long	0xfce
	.uleb128 0x1d
	.long	.LASF135
	.byte	0x1
	.byte	0x34
	.long	0x66
	.long	.LLST17
	.uleb128 0x1d
	.long	.LASF136
	.byte	0x1
	.byte	0x34
	.long	0xfce
	.long	.LLST18
	.uleb128 0x2a
	.string	"t1"
	.byte	0x1
	.byte	0x35
	.long	0xfd4
	.byte	0x4
	.byte	0x91
	.sleb128 -18128
	.uleb128 0x2a
	.string	"t2"
	.byte	0x1
	.byte	0x35
	.long	0xfd4
	.byte	0x4
	.byte	0x91
	.sleb128 -15120
	.uleb128 0x2a
	.string	"t3"
	.byte	0x1
	.byte	0x35
	.long	0xfd4
	.byte	0x4
	.byte	0x91
	.sleb128 -12112
	.uleb128 0x2a
	.string	"t4"
	.byte	0x1
	.byte	0x35
	.long	0xfd4
	.byte	0x4
	.byte	0x91
	.sleb128 -9104
	.uleb128 0x2a
	.string	"t5"
	.byte	0x1
	.byte	0x35
	.long	0xfd4
	.byte	0x3
	.byte	0x91
	.sleb128 -6096
	.uleb128 0x2a
	.string	"t6"
	.byte	0x1
	.byte	0x35
	.long	0xfd4
	.byte	0x3
	.byte	0x91
	.sleb128 -3088
	.uleb128 0x2a
	.string	"s1"
	.byte	0x1
	.byte	0x36
	.long	0x6b9
	.byte	0x3
	.byte	0x77
	.sleb128 72
	.uleb128 0x2a
	.string	"s2"
	.byte	0x1
	.byte	0x36
	.long	0x6b9
	.byte	0x3
	.byte	0x77
	.sleb128 80
	.uleb128 0x2a
	.string	"s3"
	.byte	0x1
	.byte	0x36
	.long	0x6b9
	.byte	0x3
	.byte	0x77
	.sleb128 88
	.uleb128 0x2a
	.string	"s4"
	.byte	0x1
	.byte	0x36
	.long	0x6b9
	.byte	0x3
	.byte	0x77
	.sleb128 96
	.uleb128 0x2a
	.string	"s5"
	.byte	0x1
	.byte	0x36
	.long	0x6b9
	.byte	0x3
	.byte	0x77
	.sleb128 104
	.uleb128 0x2a
	.string	"s6"
	.byte	0x1
	.byte	0x36
	.long	0x6b9
	.byte	0x3
	.byte	0x77
	.sleb128 112
	.uleb128 0x2b
	.string	"i"
	.byte	0x1
	.byte	0x37
	.long	0x66
	.long	.LLST19
	.uleb128 0x2b
	.string	"j"
	.byte	0x1
	.byte	0x37
	.long	0x66
	.long	.LLST20
	.uleb128 0x2c
	.long	.LASF137
	.byte	0x1
	.byte	0x39
	.long	0x6b9
	.long	.LLST21
	.uleb128 0x2c
	.long	.LASF138
	.byte	0x1
	.byte	0x39
	.long	0x6b9
	.long	.LLST22
	.uleb128 0x2b
	.string	"fp"
	.byte	0x1
	.byte	0x3a
	.long	0x740
	.long	.LLST23
	.uleb128 0x2b
	.string	"o1"
	.byte	0x1
	.byte	0x3b
	.long	0x323
	.long	.LLST24
	.uleb128 0x2b
	.string	"o2"
	.byte	0x1
	.byte	0x3b
	.long	0x323
	.long	.LLST25
	.uleb128 0x2b
	.string	"o3"
	.byte	0x1
	.byte	0x3b
	.long	0x323
	.long	.LLST26
	.uleb128 0x2b
	.string	"o4"
	.byte	0x1
	.byte	0x3b
	.long	0x323
	.long	.LLST27
	.uleb128 0x2b
	.string	"o5"
	.byte	0x1
	.byte	0x3b
	.long	0x323
	.long	.LLST28
	.uleb128 0x2b
	.string	"o6"
	.byte	0x1
	.byte	0x3b
	.long	0x323
	.long	.LLST29
	.uleb128 0x2c
	.long	.LASF139
	.byte	0x1
	.byte	0x3b
	.long	0x323
	.long	.LLST30
	.uleb128 0x2d
	.long	.LASF140
	.byte	0x1
	.byte	0x3d
	.long	0xfe5
	.byte	0x4
	.byte	0x91
	.sleb128 -18224
	.uleb128 0x2d
	.long	.LASF141
	.byte	0x1
	.byte	0x3d
	.long	0xfe5
	.byte	0x4
	.byte	0x91
	.sleb128 -18176
	.uleb128 0x2b
	.string	"fd"
	.byte	0x1
	.byte	0x3e
	.long	0xff5
	.long	.LLST31
	.uleb128 0x2b
	.string	"rc"
	.byte	0x1
	.byte	0x3e
	.long	0x66
	.long	.LLST32
	.uleb128 0x2c
	.long	.LASF142
	.byte	0x1
	.byte	0x88
	.long	0x51
	.long	.LLST33
	.uleb128 0x2e
	.long	.LASF143
	.long	0x1015
	.byte	0x1
	.byte	0x9
	.byte	0x3
	.quad	__PRETTY_FUNCTION__.4772
	.uleb128 0x2f
	.long	0x6c0
	.quad	.LBB79
	.long	.Ldebug_ranges0+0
	.byte	0x1
	.byte	0x77
	.long	0xb25
	.uleb128 0x1f
	.long	0x6d3
	.long	.LLST34
	.byte	0
	.uleb128 0x2f
	.long	0x6c0
	.quad	.LBB82
	.long	.Ldebug_ranges0+0x30
	.byte	0x1
	.byte	0x78
	.long	0xb46
	.uleb128 0x1f
	.long	0x6d3
	.long	.LLST35
	.byte	0
	.uleb128 0x2f
	.long	0x6c0
	.quad	.LBB88
	.long	.Ldebug_ranges0+0x70
	.byte	0x1
	.byte	0x79
	.long	0xb67
	.uleb128 0x1f
	.long	0x6d3
	.long	.LLST36
	.byte	0
	.uleb128 0x2f
	.long	0x6c0
	.quad	.LBB94
	.long	.Ldebug_ranges0+0xb0
	.byte	0x1
	.byte	0x7a
	.long	0xb88
	.uleb128 0x1f
	.long	0x6d3
	.long	.LLST37
	.byte	0
	.uleb128 0x2f
	.long	0x6c0
	.quad	.LBB100
	.long	.Ldebug_ranges0+0xf0
	.byte	0x1
	.byte	0x7b
	.long	0xba9
	.uleb128 0x1f
	.long	0x6d3
	.long	.LLST38
	.byte	0
	.uleb128 0x2f
	.long	0x6c0
	.quad	.LBB106
	.long	.Ldebug_ranges0+0x130
	.byte	0x1
	.byte	0x7c
	.long	0xbca
	.uleb128 0x1f
	.long	0x6d3
	.long	.LLST39
	.byte	0
	.uleb128 0x30
	.long	0x6e0
	.quad	.LBB113
	.quad	.LBE113
	.byte	0x1
	.byte	0xa3
	.long	0xc01
	.uleb128 0x1f
	.long	0x709
	.long	.LLST40
	.uleb128 0x1f
	.long	0x6fe
	.long	.LLST41
	.uleb128 0x1f
	.long	0x6f3
	.long	.LLST42
	.byte	0
	.uleb128 0x30
	.long	0x6e0
	.quad	.LBB115
	.quad	.LBE115
	.byte	0x1
	.byte	0xa4
	.long	0xc38
	.uleb128 0x1f
	.long	0x709
	.long	.LLST43
	.uleb128 0x1f
	.long	0x6fe
	.long	.LLST44
	.uleb128 0x1f
	.long	0x6f3
	.long	.LLST45
	.byte	0
	.uleb128 0x30
	.long	0x6e0
	.quad	.LBB117
	.quad	.LBE117
	.byte	0x1
	.byte	0xa5
	.long	0xc6f
	.uleb128 0x1f
	.long	0x709
	.long	.LLST46
	.uleb128 0x1f
	.long	0x6fe
	.long	.LLST47
	.uleb128 0x1f
	.long	0x6f3
	.long	.LLST48
	.byte	0
	.uleb128 0x30
	.long	0x6e0
	.quad	.LBB119
	.quad	.LBE119
	.byte	0x1
	.byte	0xa6
	.long	0xca6
	.uleb128 0x1f
	.long	0x709
	.long	.LLST49
	.uleb128 0x1f
	.long	0x6fe
	.long	.LLST50
	.uleb128 0x1f
	.long	0x6f3
	.long	.LLST51
	.byte	0
	.uleb128 0x30
	.long	0x6e0
	.quad	.LBB121
	.quad	.LBE121
	.byte	0x1
	.byte	0xa7
	.long	0xcdd
	.uleb128 0x1f
	.long	0x709
	.long	.LLST52
	.uleb128 0x1f
	.long	0x6fe
	.long	.LLST53
	.uleb128 0x1f
	.long	0x6f3
	.long	.LLST54
	.byte	0
	.uleb128 0x30
	.long	0x6e0
	.quad	.LBB123
	.quad	.LBE123
	.byte	0x1
	.byte	0xa8
	.long	0xd14
	.uleb128 0x1f
	.long	0x709
	.long	.LLST55
	.uleb128 0x1f
	.long	0x6fe
	.long	.LLST56
	.uleb128 0x1f
	.long	0x6f3
	.long	.LLST57
	.byte	0
	.uleb128 0x30
	.long	0x6e0
	.quad	.LBB125
	.quad	.LBE125
	.byte	0x1
	.byte	0xd0
	.long	0xd4b
	.uleb128 0x1f
	.long	0x709
	.long	.LLST58
	.uleb128 0x1f
	.long	0x6fe
	.long	.LLST59
	.uleb128 0x1f
	.long	0x6f3
	.long	.LLST60
	.byte	0
	.uleb128 0x30
	.long	0x6e0
	.quad	.LBB127
	.quad	.LBE127
	.byte	0x1
	.byte	0xd1
	.long	0xd82
	.uleb128 0x1f
	.long	0x709
	.long	.LLST61
	.uleb128 0x1f
	.long	0x6fe
	.long	.LLST62
	.uleb128 0x1f
	.long	0x6f3
	.long	.LLST63
	.byte	0
	.uleb128 0x30
	.long	0x6e0
	.quad	.LBB129
	.quad	.LBE129
	.byte	0x1
	.byte	0xd2
	.long	0xdb9
	.uleb128 0x1f
	.long	0x709
	.long	.LLST64
	.uleb128 0x1f
	.long	0x6fe
	.long	.LLST65
	.uleb128 0x1f
	.long	0x6f3
	.long	.LLST66
	.byte	0
	.uleb128 0x30
	.long	0x6e0
	.quad	.LBB131
	.quad	.LBE131
	.byte	0x1
	.byte	0xd3
	.long	0xdf0
	.uleb128 0x1f
	.long	0x709
	.long	.LLST67
	.uleb128 0x1f
	.long	0x6fe
	.long	.LLST68
	.uleb128 0x1f
	.long	0x6f3
	.long	.LLST69
	.byte	0
	.uleb128 0x30
	.long	0x6e0
	.quad	.LBB133
	.quad	.LBE133
	.byte	0x1
	.byte	0xd4
	.long	0xe27
	.uleb128 0x1f
	.long	0x709
	.long	.LLST70
	.uleb128 0x1f
	.long	0x6fe
	.long	.LLST71
	.uleb128 0x1f
	.long	0x6f3
	.long	.LLST72
	.byte	0
	.uleb128 0x30
	.long	0x6e0
	.quad	.LBB135
	.quad	.LBE135
	.byte	0x1
	.byte	0xd5
	.long	0xe5e
	.uleb128 0x1f
	.long	0x709
	.long	.LLST73
	.uleb128 0x1f
	.long	0x6fe
	.long	.LLST74
	.uleb128 0x1f
	.long	0x6f3
	.long	.LLST75
	.byte	0
	.uleb128 0x30
	.long	0x66e
	.quad	.LBB137
	.quad	.LBE137
	.byte	0x1
	.byte	0xe4
	.long	0xe83
	.uleb128 0x1f
	.long	0x681
	.long	.LLST76
	.byte	0
	.uleb128 0x30
	.long	0x66e
	.quad	.LBB139
	.quad	.LBE139
	.byte	0x1
	.byte	0xe5
	.long	0xea8
	.uleb128 0x1f
	.long	0x681
	.long	.LLST77
	.byte	0
	.uleb128 0x30
	.long	0x66e
	.quad	.LBB141
	.quad	.LBE141
	.byte	0x1
	.byte	0xe6
	.long	0xecd
	.uleb128 0x1f
	.long	0x681
	.long	.LLST78
	.byte	0
	.uleb128 0x30
	.long	0x66e
	.quad	.LBB143
	.quad	.LBE143
	.byte	0x1
	.byte	0xe7
	.long	0xef2
	.uleb128 0x1f
	.long	0x681
	.long	.LLST79
	.byte	0
	.uleb128 0x30
	.long	0x66e
	.quad	.LBB145
	.quad	.LBE145
	.byte	0x1
	.byte	0xe8
	.long	0xf17
	.uleb128 0x1f
	.long	0x681
	.long	.LLST80
	.byte	0
	.uleb128 0x30
	.long	0x66e
	.quad	.LBB147
	.quad	.LBE147
	.byte	0x1
	.byte	0xe9
	.long	0xf3c
	.uleb128 0x1f
	.long	0x681
	.long	.LLST81
	.byte	0
	.uleb128 0x30
	.long	0x66e
	.quad	.LBB149
	.quad	.LBE149
	.byte	0x1
	.byte	0xea
	.long	0xf61
	.uleb128 0x1f
	.long	0x681
	.long	.LLST82
	.byte	0
	.uleb128 0x2f
	.long	0x66e
	.quad	.LBB151
	.long	.Ldebug_ranges0+0x170
	.byte	0x1
	.byte	0xec
	.long	0xf82
	.uleb128 0x1f
	.long	0x681
	.long	.LLST83
	.byte	0
	.uleb128 0x2f
	.long	0x66e
	.quad	.LBB155
	.long	.Ldebug_ranges0+0x1a0
	.byte	0x1
	.byte	0xed
	.long	0xfa3
	.uleb128 0x1f
	.long	0x681
	.long	.LLST84
	.byte	0
	.uleb128 0x31
	.long	0x715
	.quad	.LBB159
	.quad	.LBE159
	.byte	0x1
	.byte	0xf3
	.uleb128 0x1f
	.long	0x733
	.long	.LLST85
	.uleb128 0x1f
	.long	0x728
	.long	.LLST86
	.byte	0
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0xb8
	.uleb128 0xc
	.long	0xbe
	.long	0xfe5
	.uleb128 0x32
	.long	0x3c
	.value	0xbb8
	.byte	0
	.uleb128 0xc
	.long	0x36b
	.long	0xff5
	.uleb128 0xd
	.long	0x3c
	.byte	0x5
	.byte	0
	.uleb128 0xc
	.long	0x66
	.long	0x1005
	.uleb128 0xd
	.long	0x3c
	.byte	0x5
	.byte	0
	.uleb128 0xc
	.long	0xbe
	.long	0x1015
	.uleb128 0xd
	.long	0x3c
	.byte	0x4
	.byte	0
	.uleb128 0xe
	.long	0x1005
	.uleb128 0x33
	.long	.LASF144
	.byte	0x7
	.byte	0xa9
	.long	0x2e1
	.byte	0x1
	.byte	0x1
	.uleb128 0x33
	.long	.LASF145
	.byte	0x7
	.byte	0xaa
	.long	0x2e1
	.byte	0x1
	.byte	0x1
	.uleb128 0xc
	.long	0x488
	.long	0x1044
	.uleb128 0xd
	.long	0x3c
	.byte	0x5
	.byte	0
	.uleb128 0x34
	.long	.LASF146
	.byte	0x1
	.byte	0x31
	.long	0x1034
	.byte	0x1
	.byte	0x9
	.byte	0x3
	.quad	attr
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x4
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0xd
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xd
	.uleb128 0xb
	.uleb128 0xc
	.uleb128 0xb
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x34
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x34
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x34
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x26
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x27
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x28
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x29
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2a
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x2b
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2c
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2d
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x2e
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x34
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x2f
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x55
	.uleb128 0x6
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x30
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x31
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x32
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x33
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x34
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
.LLST0:
	.quad	.LFB89
	.quad	.LCFI0
	.value	0x2
	.byte	0x77
	.sleb128 8
	.quad	.LCFI0
	.quad	.LCFI1
	.value	0x2
	.byte	0x77
	.sleb128 16
	.quad	.LCFI1
	.quad	.LFE89
	.value	0x2
	.byte	0x77
	.sleb128 8
	.quad	0
	.quad	0
.LLST1:
	.quad	.LVL0
	.quad	.LVL5
	.value	0x1
	.byte	0x55
	.quad	.LVL5
	.quad	.LVL6-1
	.value	0x1
	.byte	0x54
	.quad	0
	.quad	0
.LLST2:
	.quad	.LVL0
	.quad	.LVL4
	.value	0x1
	.byte	0x54
	.quad	.LVL4
	.quad	.LVL6-1
	.value	0x1
	.byte	0x51
	.quad	0
	.quad	0
.LLST3:
	.quad	.LVL0
	.quad	.LVL3
	.value	0x1
	.byte	0x51
	.quad	.LVL3
	.quad	.LVL6-1
	.value	0x1
	.byte	0x52
	.quad	0
	.quad	0
.LLST4:
	.quad	.LVL0
	.quad	.LVL2
	.value	0x1
	.byte	0x52
	.quad	.LVL2
	.quad	.LVL6-1
	.value	0x1
	.byte	0x58
	.quad	0
	.quad	0
.LLST5:
	.quad	.LVL0
	.quad	.LVL1
	.value	0x1
	.byte	0x58
	.quad	.LVL1
	.quad	.LVL6-1
	.value	0x1
	.byte	0x59
	.quad	0
	.quad	0
.LLST6:
	.quad	.LFB91
	.quad	.LCFI2
	.value	0x2
	.byte	0x77
	.sleb128 8
	.quad	.LCFI2
	.quad	.LCFI3
	.value	0x2
	.byte	0x77
	.sleb128 48
	.quad	.LCFI3
	.quad	.LCFI4
	.value	0x2
	.byte	0x77
	.sleb128 8
	.quad	.LCFI4
	.quad	.LCFI5
	.value	0x2
	.byte	0x77
	.sleb128 48
	.quad	.LCFI5
	.quad	.LFE91
	.value	0x2
	.byte	0x77
	.sleb128 8
	.quad	0
	.quad	0
.LLST7:
	.quad	.LVL7
	.quad	.LVL8-1
	.value	0x1
	.byte	0x61
	.quad	.LVL8-1
	.quad	.LFE91
	.value	0x2
	.byte	0x91
	.sleb128 -40
	.quad	0
	.quad	0
.LLST8:
	.quad	.LFB92
	.quad	.LCFI6
	.value	0x2
	.byte	0x77
	.sleb128 8
	.quad	.LCFI6
	.quad	.LCFI7
	.value	0x2
	.byte	0x77
	.sleb128 16
	.quad	.LCFI7
	.quad	.LCFI8
	.value	0x2
	.byte	0x77
	.sleb128 24
	.quad	.LCFI8
	.quad	.LCFI9
	.value	0x2
	.byte	0x77
	.sleb128 32
	.quad	.LCFI9
	.quad	.LCFI10
	.value	0x2
	.byte	0x77
	.sleb128 48
	.quad	.LCFI10
	.quad	.LCFI11
	.value	0x2
	.byte	0x77
	.sleb128 32
	.quad	.LCFI11
	.quad	.LCFI12
	.value	0x2
	.byte	0x77
	.sleb128 24
	.quad	.LCFI12
	.quad	.LCFI13
	.value	0x2
	.byte	0x77
	.sleb128 16
	.quad	.LCFI13
	.quad	.LFE92
	.value	0x2
	.byte	0x77
	.sleb128 8
	.quad	0
	.quad	0
.LLST9:
	.quad	.LVL10
	.quad	.LVL11
	.value	0x1
	.byte	0x61
	.quad	.LVL11
	.quad	.LFE92
	.value	0x2
	.byte	0x91
	.sleb128 -40
	.quad	0
	.quad	0
.LLST10:
	.quad	.LVL10
	.quad	.LVL11
	.value	0x1
	.byte	0x55
	.quad	.LVL11
	.quad	.LVL15
	.value	0x1
	.byte	0x53
	.quad	.LVL15
	.quad	.LFE92
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LLST11:
	.quad	.LVL10
	.quad	.LVL11
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	0
	.quad	0
.LLST12:
	.quad	.LVL14
	.quad	.LFE92
	.value	0x1
	.byte	0x62
	.quad	0
	.quad	0
.LLST13:
	.quad	.LFB93
	.quad	.LCFI14
	.value	0x2
	.byte	0x77
	.sleb128 8
	.quad	.LCFI14
	.quad	.LCFI15
	.value	0x2
	.byte	0x77
	.sleb128 16
	.quad	.LCFI15
	.quad	.LCFI16
	.value	0x2
	.byte	0x77
	.sleb128 24
	.quad	.LCFI16
	.quad	.LCFI17
	.value	0x2
	.byte	0x77
	.sleb128 32
	.quad	.LCFI17
	.quad	.LCFI18
	.value	0x2
	.byte	0x77
	.sleb128 24
	.quad	.LCFI18
	.quad	.LCFI19
	.value	0x2
	.byte	0x77
	.sleb128 16
	.quad	.LCFI19
	.quad	.LFE93
	.value	0x2
	.byte	0x77
	.sleb128 8
	.quad	0
	.quad	0
.LLST14:
	.quad	.LVL16
	.quad	.LVL17
	.value	0x1
	.byte	0x55
	.quad	.LVL17
	.quad	.LVL20
	.value	0x1
	.byte	0x56
	.quad	0
	.quad	0
.LLST15:
	.quad	.LVL16
	.quad	.LVL17
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	0
	.quad	0
.LLST16:
	.quad	.LFB90
	.quad	.LCFI20
	.value	0x2
	.byte	0x77
	.sleb128 8
	.quad	.LCFI20
	.quad	.LCFI21
	.value	0x2
	.byte	0x77
	.sleb128 16
	.quad	.LCFI21
	.quad	.LCFI22
	.value	0x2
	.byte	0x77
	.sleb128 24
	.quad	.LCFI22
	.quad	.LCFI23
	.value	0x2
	.byte	0x77
	.sleb128 32
	.quad	.LCFI23
	.quad	.LCFI24
	.value	0x2
	.byte	0x77
	.sleb128 40
	.quad	.LCFI24
	.quad	.LCFI25
	.value	0x2
	.byte	0x77
	.sleb128 48
	.quad	.LCFI25
	.quad	.LCFI26
	.value	0x2
	.byte	0x77
	.sleb128 56
	.quad	.LCFI26
	.quad	.LCFI27
	.value	0x4
	.byte	0x77
	.sleb128 18352
	.quad	.LCFI27
	.quad	.LCFI28
	.value	0x2
	.byte	0x77
	.sleb128 56
	.quad	.LCFI28
	.quad	.LCFI29
	.value	0x2
	.byte	0x77
	.sleb128 48
	.quad	.LCFI29
	.quad	.LCFI30
	.value	0x2
	.byte	0x77
	.sleb128 40
	.quad	.LCFI30
	.quad	.LCFI31
	.value	0x2
	.byte	0x77
	.sleb128 32
	.quad	.LCFI31
	.quad	.LCFI32
	.value	0x2
	.byte	0x77
	.sleb128 24
	.quad	.LCFI32
	.quad	.LCFI33
	.value	0x2
	.byte	0x77
	.sleb128 16
	.quad	.LCFI33
	.quad	.LCFI34
	.value	0x2
	.byte	0x77
	.sleb128 8
	.quad	.LCFI34
	.quad	.LFE90
	.value	0x4
	.byte	0x77
	.sleb128 18352
	.quad	0
	.quad	0
.LLST17:
	.quad	.LVL21
	.quad	.LVL23-1
	.value	0x1
	.byte	0x55
	.quad	.LVL23-1
	.quad	.LVL78
	.value	0x1
	.byte	0x56
	.quad	.LVL113
	.quad	.LVL126
	.value	0x1
	.byte	0x56
	.quad	.LVL131
	.quad	.LVL134
	.value	0x1
	.byte	0x56
	.quad	.LVL136
	.quad	.LFE90
	.value	0x1
	.byte	0x56
	.quad	0
	.quad	0
.LLST18:
	.quad	.LVL21
	.quad	.LVL23-1
	.value	0x1
	.byte	0x54
	.quad	.LVL23-1
	.quad	.LVL58
	.value	0x1
	.byte	0x53
	.quad	.LVL113
	.quad	.LVL124
	.value	0x1
	.byte	0x53
	.quad	0
	.quad	0
.LLST19:
	.quad	.LVL22
	.quad	.LVL78
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL104
	.quad	.LVL107
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL113
	.quad	.LVL126
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL131
	.quad	.LVL134
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL136
	.quad	.LFE90
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	0
	.quad	0
.LLST20:
	.quad	.LVL22
	.quad	.LVL78
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL79
	.quad	.LVL110
	.value	0x1
	.byte	0x56
	.quad	.LVL111
	.quad	.LVL113
	.value	0x1
	.byte	0x56
	.quad	.LVL113
	.quad	.LVL126
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL126
	.quad	.LVL131
	.value	0x1
	.byte	0x56
	.quad	.LVL131
	.quad	.LVL134
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL134
	.quad	.LVL135
	.value	0x1
	.byte	0x56
	.quad	.LVL136
	.quad	.LFE90
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	0
	.quad	0
.LLST21:
	.quad	.LVL64
	.quad	.LVL66-1
	.value	0x1
	.byte	0x61
	.quad	.LVL66-1
	.quad	.LVL110
	.value	0x4
	.byte	0x91
	.sleb128 -18232
	.quad	.LVL111
	.quad	.LVL113
	.value	0x4
	.byte	0x91
	.sleb128 -18232
	.quad	.LVL124
	.quad	.LVL135
	.value	0x4
	.byte	0x91
	.sleb128 -18232
	.quad	.LVL136
	.quad	.LFE90
	.value	0x4
	.byte	0x91
	.sleb128 -18232
	.quad	0
	.quad	0
.LLST22:
	.quad	.LVL93
	.quad	.LVL94-1
	.value	0x1
	.byte	0x61
	.quad	0
	.quad	0
.LLST23:
	.quad	.LVL104
	.quad	.LVL107
	.value	0x1
	.byte	0x50
	.quad	.LVL107
	.quad	.LVL108
	.value	0x1
	.byte	0x5c
	.quad	.LVL108
	.quad	.LVL110
	.value	0x4
	.byte	0x91
	.sleb128 -18336
	.quad	0
	.quad	0
.LLST24:
	.quad	.LVL48
	.quad	.LVL49-1
	.value	0x1
	.byte	0x50
	.quad	.LVL49-1
	.quad	.LVL106
	.value	0x1
	.byte	0x5f
	.quad	.LVL106
	.quad	.LVL109
	.value	0x4
	.byte	0x91
	.sleb128 -18328
	.quad	.LVL111
	.quad	.LVL113
	.value	0x1
	.byte	0x5f
	.quad	.LVL124
	.quad	.LVL135
	.value	0x1
	.byte	0x5f
	.quad	.LVL136
	.quad	.LFE90
	.value	0x1
	.byte	0x5f
	.quad	0
	.quad	0
.LLST25:
	.quad	.LVL50
	.quad	.LVL51-1
	.value	0x1
	.byte	0x50
	.quad	.LVL51-1
	.quad	.LVL110
	.value	0x4
	.byte	0x91
	.sleb128 -18304
	.quad	.LVL111
	.quad	.LVL113
	.value	0x4
	.byte	0x91
	.sleb128 -18304
	.quad	.LVL124
	.quad	.LVL135
	.value	0x4
	.byte	0x91
	.sleb128 -18304
	.quad	.LVL136
	.quad	.LFE90
	.value	0x4
	.byte	0x91
	.sleb128 -18304
	.quad	0
	.quad	0
.LLST26:
	.quad	.LVL52
	.quad	.LVL53-1
	.value	0x1
	.byte	0x50
	.quad	.LVL53-1
	.quad	.LVL110
	.value	0x4
	.byte	0x91
	.sleb128 -18296
	.quad	.LVL111
	.quad	.LVL113
	.value	0x4
	.byte	0x91
	.sleb128 -18296
	.quad	.LVL124
	.quad	.LVL135
	.value	0x4
	.byte	0x91
	.sleb128 -18296
	.quad	.LVL136
	.quad	.LFE90
	.value	0x4
	.byte	0x91
	.sleb128 -18296
	.quad	0
	.quad	0
.LLST27:
	.quad	.LVL54
	.quad	.LVL55-1
	.value	0x1
	.byte	0x50
	.quad	.LVL55-1
	.quad	.LVL110
	.value	0x4
	.byte	0x91
	.sleb128 -18288
	.quad	.LVL111
	.quad	.LVL113
	.value	0x4
	.byte	0x91
	.sleb128 -18288
	.quad	.LVL124
	.quad	.LVL135
	.value	0x4
	.byte	0x91
	.sleb128 -18288
	.quad	.LVL136
	.quad	.LFE90
	.value	0x4
	.byte	0x91
	.sleb128 -18288
	.quad	0
	.quad	0
.LLST28:
	.quad	.LVL56
	.quad	.LVL57-1
	.value	0x1
	.byte	0x50
	.quad	.LVL57-1
	.quad	.LVL107
	.value	0x1
	.byte	0x5c
	.quad	.LVL107
	.quad	.LVL109
	.value	0x1
	.byte	0x5f
	.quad	.LVL109
	.quad	.LVL110
	.value	0x1
	.byte	0x5c
	.quad	.LVL111
	.quad	.LVL113
	.value	0x1
	.byte	0x5c
	.quad	.LVL124
	.quad	.LVL135
	.value	0x1
	.byte	0x5c
	.quad	.LVL136
	.quad	.LFE90
	.value	0x1
	.byte	0x5c
	.quad	0
	.quad	0
.LLST29:
	.quad	.LVL58
	.quad	.LVL59-1
	.value	0x1
	.byte	0x50
	.quad	.LVL59-1
	.quad	.LVL110
	.value	0x1
	.byte	0x53
	.quad	.LVL111
	.quad	.LVL113
	.value	0x1
	.byte	0x53
	.quad	.LVL124
	.quad	.LVL135
	.value	0x1
	.byte	0x53
	.quad	.LVL136
	.quad	.LFE90
	.value	0x1
	.byte	0x53
	.quad	0
	.quad	0
.LLST30:
	.quad	.LVL60
	.quad	.LVL61-1
	.value	0x1
	.byte	0x50
	.quad	.LVL61-1
	.quad	.LVL110
	.value	0x1
	.byte	0x5e
	.quad	.LVL111
	.quad	.LVL113
	.value	0x1
	.byte	0x5e
	.quad	.LVL124
	.quad	.LVL135
	.value	0x1
	.byte	0x5e
	.quad	.LVL136
	.quad	.LFE90
	.value	0x1
	.byte	0x5e
	.quad	0
	.quad	0
.LLST31:
	.quad	.LVL24
	.quad	.LVL25
	.value	0x5
	.byte	0x50
	.byte	0x93
	.uleb128 0x4
	.byte	0x93
	.uleb128 0x14
	.quad	.LVL25
	.quad	.LVL26
	.value	0x5
	.byte	0x5d
	.byte	0x93
	.uleb128 0x4
	.byte	0x93
	.uleb128 0x14
	.quad	.LVL26
	.quad	.LVL27
	.value	0x8
	.byte	0x5d
	.byte	0x93
	.uleb128 0x4
	.byte	0x50
	.byte	0x93
	.uleb128 0x4
	.byte	0x93
	.uleb128 0x10
	.quad	.LVL27
	.quad	.LVL28
	.value	0xb
	.byte	0x5d
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18336
	.byte	0x93
	.uleb128 0x4
	.byte	0x93
	.uleb128 0x10
	.quad	.LVL28
	.quad	.LVL29
	.value	0xe
	.byte	0x5d
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18336
	.byte	0x93
	.uleb128 0x4
	.byte	0x50
	.byte	0x93
	.uleb128 0x4
	.byte	0x93
	.uleb128 0xc
	.quad	.LVL29
	.quad	.LVL30
	.value	0x11
	.byte	0x5d
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18336
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18328
	.byte	0x93
	.uleb128 0x4
	.byte	0x93
	.uleb128 0xc
	.quad	.LVL30
	.quad	.LVL31
	.value	0x14
	.byte	0x5d
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18336
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18328
	.byte	0x93
	.uleb128 0x4
	.byte	0x50
	.byte	0x93
	.uleb128 0x4
	.byte	0x93
	.uleb128 0x8
	.quad	.LVL31
	.quad	.LVL32
	.value	0x17
	.byte	0x5d
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18336
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18328
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18316
	.byte	0x93
	.uleb128 0x4
	.byte	0x93
	.uleb128 0x8
	.quad	.LVL32
	.quad	.LVL33
	.value	0x1a
	.byte	0x5d
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18336
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18328
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18316
	.byte	0x93
	.uleb128 0x4
	.byte	0x50
	.byte	0x93
	.uleb128 0x4
	.byte	0x93
	.uleb128 0x4
	.quad	.LVL33
	.quad	.LVL34
	.value	0x1d
	.byte	0x5d
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18336
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18328
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18316
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18312
	.byte	0x93
	.uleb128 0x4
	.byte	0x93
	.uleb128 0x4
	.quad	.LVL34
	.quad	.LVL35
	.value	0x1e
	.byte	0x5d
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18336
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18328
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18316
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18312
	.byte	0x93
	.uleb128 0x4
	.byte	0x50
	.byte	0x93
	.uleb128 0x4
	.quad	.LVL35
	.quad	.LVL105
	.value	0x21
	.byte	0x5d
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18336
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18328
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18316
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18312
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18308
	.byte	0x93
	.uleb128 0x4
	.quad	.LVL105
	.quad	.LVL111
	.value	0x20
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18336
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18328
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18316
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18312
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18308
	.byte	0x93
	.uleb128 0x4
	.quad	.LVL111
	.quad	.LVL113
	.value	0x21
	.byte	0x5d
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18336
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18328
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18316
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18312
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18308
	.byte	0x93
	.uleb128 0x4
	.quad	.LVL113
	.quad	.LVL114
	.value	0xb
	.byte	0x5d
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18336
	.byte	0x93
	.uleb128 0x4
	.byte	0x93
	.uleb128 0x10
	.quad	.LVL114
	.quad	.LVL115-1
	.value	0x1e
	.byte	0x5d
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18336
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18328
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18316
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18312
	.byte	0x93
	.uleb128 0x4
	.byte	0x50
	.byte	0x93
	.uleb128 0x4
	.quad	.LVL115-1
	.quad	.LVL116
	.value	0x21
	.byte	0x5d
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18336
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18328
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18316
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18312
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18308
	.byte	0x93
	.uleb128 0x4
	.quad	.LVL116
	.quad	.LVL117
	.value	0x5
	.byte	0x5d
	.byte	0x93
	.uleb128 0x4
	.byte	0x93
	.uleb128 0x14
	.quad	.LVL117
	.quad	.LVL118-1
	.value	0xe
	.byte	0x5d
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18336
	.byte	0x93
	.uleb128 0x4
	.byte	0x50
	.byte	0x93
	.uleb128 0x4
	.byte	0x93
	.uleb128 0xc
	.quad	.LVL118-1
	.quad	.LVL119
	.value	0x11
	.byte	0x5d
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18336
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18328
	.byte	0x93
	.uleb128 0x4
	.byte	0x93
	.uleb128 0xc
	.quad	.LVL119
	.quad	.LVL120-1
	.value	0x14
	.byte	0x5d
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18336
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18328
	.byte	0x93
	.uleb128 0x4
	.byte	0x50
	.byte	0x93
	.uleb128 0x4
	.byte	0x93
	.uleb128 0x8
	.quad	.LVL120-1
	.quad	.LVL121
	.value	0x17
	.byte	0x5d
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18336
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18328
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18316
	.byte	0x93
	.uleb128 0x4
	.byte	0x93
	.uleb128 0x8
	.quad	.LVL121
	.quad	.LVL122-1
	.value	0x1a
	.byte	0x5d
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18336
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18328
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18316
	.byte	0x93
	.uleb128 0x4
	.byte	0x50
	.byte	0x93
	.uleb128 0x4
	.byte	0x93
	.uleb128 0x4
	.quad	.LVL122-1
	.quad	.LVL123
	.value	0x1d
	.byte	0x5d
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18336
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18328
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18316
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18312
	.byte	0x93
	.uleb128 0x4
	.byte	0x93
	.uleb128 0x4
	.quad	.LVL123
	.quad	.LVL135
	.value	0x21
	.byte	0x5d
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18336
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18328
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18316
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18312
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18308
	.byte	0x93
	.uleb128 0x4
	.quad	.LVL135
	.quad	.LVL136
	.value	0x20
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18336
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18328
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18316
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18312
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18308
	.byte	0x93
	.uleb128 0x4
	.quad	.LVL136
	.quad	.LFE90
	.value	0x21
	.byte	0x5d
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18336
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18328
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18316
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18312
	.byte	0x93
	.uleb128 0x4
	.byte	0x91
	.sleb128 -18308
	.byte	0x93
	.uleb128 0x4
	.quad	0
	.quad	0
.LLST32:
	.quad	.LVL66
	.quad	.LVL68-1
	.value	0x1
	.byte	0x50
	.quad	.LVL68
	.quad	.LVL70-1
	.value	0x1
	.byte	0x50
	.quad	.LVL70
	.quad	.LVL72-1
	.value	0x1
	.byte	0x50
	.quad	.LVL72
	.quad	.LVL74-1
	.value	0x1
	.byte	0x50
	.quad	.LVL74
	.quad	.LVL76-1
	.value	0x1
	.byte	0x50
	.quad	.LVL76
	.quad	.LVL77
	.value	0x1
	.byte	0x50
	.quad	.LVL81
	.quad	.LVL83-1
	.value	0x1
	.byte	0x50
	.quad	.LVL83
	.quad	.LVL85-1
	.value	0x1
	.byte	0x50
	.quad	.LVL85
	.quad	.LVL87-1
	.value	0x1
	.byte	0x50
	.quad	.LVL87
	.quad	.LVL89-1
	.value	0x1
	.byte	0x50
	.quad	.LVL89
	.quad	.LVL91-1
	.value	0x1
	.byte	0x50
	.quad	.LVL91
	.quad	.LVL92-1
	.value	0x1
	.byte	0x50
	.quad	.LVL124
	.quad	.LVL125-1
	.value	0x1
	.byte	0x50
	.quad	.LVL125
	.quad	.LVL126-1
	.value	0x1
	.byte	0x50
	.quad	.LVL126
	.quad	.LVL127-1
	.value	0x1
	.byte	0x50
	.quad	.LVL127
	.quad	.LVL128-1
	.value	0x1
	.byte	0x50
	.quad	.LVL128
	.quad	.LVL129-1
	.value	0x1
	.byte	0x50
	.quad	.LVL129
	.quad	.LVL130-1
	.value	0x1
	.byte	0x50
	.quad	.LVL130
	.quad	.LVL131-1
	.value	0x1
	.byte	0x50
	.quad	.LVL131
	.quad	.LVL132-1
	.value	0x1
	.byte	0x50
	.quad	.LVL132
	.quad	.LVL133-1
	.value	0x1
	.byte	0x50
	.quad	.LVL133
	.quad	.LVL134-1
	.value	0x1
	.byte	0x50
	.quad	.LVL134
	.quad	.LVL135-1
	.value	0x1
	.byte	0x50
	.quad	.LVL136
	.quad	.LVL137-1
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LLST33:
	.quad	.LVL61
	.quad	.LVL62-1
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LLST34:
	.quad	.LVL36
	.quad	.LVL37-1
	.value	0x2
	.byte	0x73
	.sleb128 8
	.quad	0
	.quad	0
.LLST35:
	.quad	.LVL38
	.quad	.LVL39-1
	.value	0x2
	.byte	0x73
	.sleb128 16
	.quad	0
	.quad	0
.LLST36:
	.quad	.LVL40
	.quad	.LVL41-1
	.value	0x2
	.byte	0x73
	.sleb128 24
	.quad	0
	.quad	0
.LLST37:
	.quad	.LVL42
	.quad	.LVL43-1
	.value	0x2
	.byte	0x73
	.sleb128 32
	.quad	0
	.quad	0
.LLST38:
	.quad	.LVL44
	.quad	.LVL45-1
	.value	0x2
	.byte	0x73
	.sleb128 40
	.quad	0
	.quad	0
.LLST39:
	.quad	.LVL46
	.quad	.LVL47-1
	.value	0x2
	.byte	0x73
	.sleb128 48
	.quad	0
	.quad	0
.LLST40:
	.quad	.LVL65
	.quad	.LVL110
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL111
	.quad	.LVL113
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL124
	.quad	.LVL135
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL136
	.quad	.LFE90
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	0
	.quad	0
.LLST41:
	.quad	.LVL65
	.quad	.LVL110
	.value	0x5
	.byte	0x91
	.sleb128 -18224
	.byte	0x9f
	.quad	.LVL111
	.quad	.LVL113
	.value	0x5
	.byte	0x91
	.sleb128 -18224
	.byte	0x9f
	.quad	.LVL124
	.quad	.LVL135
	.value	0x5
	.byte	0x91
	.sleb128 -18224
	.byte	0x9f
	.quad	.LVL136
	.quad	.LFE90
	.value	0x5
	.byte	0x91
	.sleb128 -18224
	.byte	0x9f
	.quad	0
	.quad	0
.LLST42:
	.quad	.LVL65
	.quad	.LVL105
	.value	0x1
	.byte	0x5d
	.quad	.LVL111
	.quad	.LVL113
	.value	0x1
	.byte	0x5d
	.quad	.LVL124
	.quad	.LVL135
	.value	0x1
	.byte	0x5d
	.quad	.LVL136
	.quad	.LFE90
	.value	0x1
	.byte	0x5d
	.quad	0
	.quad	0
.LLST43:
	.quad	.LVL67
	.quad	.LVL110
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL111
	.quad	.LVL113
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL124
	.quad	.LVL133
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL134
	.quad	.LVL135
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL136
	.quad	.LFE90
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	0
	.quad	0
.LLST44:
	.quad	.LVL67
	.quad	.LVL110
	.value	0x5
	.byte	0x91
	.sleb128 -18216
	.byte	0x9f
	.quad	.LVL111
	.quad	.LVL113
	.value	0x5
	.byte	0x91
	.sleb128 -18216
	.byte	0x9f
	.quad	.LVL124
	.quad	.LVL133
	.value	0x5
	.byte	0x91
	.sleb128 -18216
	.byte	0x9f
	.quad	.LVL134
	.quad	.LVL135
	.value	0x5
	.byte	0x91
	.sleb128 -18216
	.byte	0x9f
	.quad	.LVL136
	.quad	.LFE90
	.value	0x5
	.byte	0x91
	.sleb128 -18216
	.byte	0x9f
	.quad	0
	.quad	0
.LLST45:
	.quad	.LVL67
	.quad	.LVL110
	.value	0x4
	.byte	0x91
	.sleb128 -18336
	.quad	.LVL111
	.quad	.LVL113
	.value	0x4
	.byte	0x91
	.sleb128 -18336
	.quad	.LVL124
	.quad	.LVL133
	.value	0x4
	.byte	0x91
	.sleb128 -18336
	.quad	.LVL134
	.quad	.LVL135
	.value	0x4
	.byte	0x91
	.sleb128 -18336
	.quad	.LVL136
	.quad	.LFE90
	.value	0x4
	.byte	0x91
	.sleb128 -18336
	.quad	0
	.quad	0
.LLST46:
	.quad	.LVL69
	.quad	.LVL110
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL111
	.quad	.LVL113
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL124
	.quad	.LVL125
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL126
	.quad	.LVL133
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL134
	.quad	.LVL135
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL136
	.quad	.LFE90
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	0
	.quad	0
.LLST47:
	.quad	.LVL69
	.quad	.LVL110
	.value	0x5
	.byte	0x91
	.sleb128 -18208
	.byte	0x9f
	.quad	.LVL111
	.quad	.LVL113
	.value	0x5
	.byte	0x91
	.sleb128 -18208
	.byte	0x9f
	.quad	.LVL124
	.quad	.LVL125
	.value	0x5
	.byte	0x91
	.sleb128 -18208
	.byte	0x9f
	.quad	.LVL126
	.quad	.LVL133
	.value	0x5
	.byte	0x91
	.sleb128 -18208
	.byte	0x9f
	.quad	.LVL134
	.quad	.LVL135
	.value	0x5
	.byte	0x91
	.sleb128 -18208
	.byte	0x9f
	.quad	.LVL136
	.quad	.LFE90
	.value	0x5
	.byte	0x91
	.sleb128 -18208
	.byte	0x9f
	.quad	0
	.quad	0
.LLST48:
	.quad	.LVL69
	.quad	.LVL110
	.value	0x4
	.byte	0x91
	.sleb128 -18328
	.quad	.LVL111
	.quad	.LVL113
	.value	0x4
	.byte	0x91
	.sleb128 -18328
	.quad	.LVL124
	.quad	.LVL125
	.value	0x4
	.byte	0x91
	.sleb128 -18328
	.quad	.LVL126
	.quad	.LVL133
	.value	0x4
	.byte	0x91
	.sleb128 -18328
	.quad	.LVL134
	.quad	.LVL135
	.value	0x4
	.byte	0x91
	.sleb128 -18328
	.quad	.LVL136
	.quad	.LFE90
	.value	0x4
	.byte	0x91
	.sleb128 -18328
	.quad	0
	.quad	0
.LLST49:
	.quad	.LVL71
	.quad	.LVL110
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL111
	.quad	.LVL113
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL124
	.quad	.LVL125
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL126
	.quad	.LVL133
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL134
	.quad	.LVL135
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	0
	.quad	0
.LLST50:
	.quad	.LVL71
	.quad	.LVL110
	.value	0x5
	.byte	0x91
	.sleb128 -18200
	.byte	0x9f
	.quad	.LVL111
	.quad	.LVL113
	.value	0x5
	.byte	0x91
	.sleb128 -18200
	.byte	0x9f
	.quad	.LVL124
	.quad	.LVL125
	.value	0x5
	.byte	0x91
	.sleb128 -18200
	.byte	0x9f
	.quad	.LVL126
	.quad	.LVL133
	.value	0x5
	.byte	0x91
	.sleb128 -18200
	.byte	0x9f
	.quad	.LVL134
	.quad	.LVL135
	.value	0x5
	.byte	0x91
	.sleb128 -18200
	.byte	0x9f
	.quad	0
	.quad	0
.LLST51:
	.quad	.LVL71
	.quad	.LVL110
	.value	0x4
	.byte	0x91
	.sleb128 -18316
	.quad	.LVL111
	.quad	.LVL113
	.value	0x4
	.byte	0x91
	.sleb128 -18316
	.quad	.LVL124
	.quad	.LVL125
	.value	0x4
	.byte	0x91
	.sleb128 -18316
	.quad	.LVL126
	.quad	.LVL133
	.value	0x4
	.byte	0x91
	.sleb128 -18316
	.quad	.LVL134
	.quad	.LVL135
	.value	0x4
	.byte	0x91
	.sleb128 -18316
	.quad	0
	.quad	0
.LLST52:
	.quad	.LVL73
	.quad	.LVL110
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL111
	.quad	.LVL113
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL126
	.quad	.LVL133
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL134
	.quad	.LVL135
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	0
	.quad	0
.LLST53:
	.quad	.LVL73
	.quad	.LVL110
	.value	0x5
	.byte	0x91
	.sleb128 -18192
	.byte	0x9f
	.quad	.LVL111
	.quad	.LVL113
	.value	0x5
	.byte	0x91
	.sleb128 -18192
	.byte	0x9f
	.quad	.LVL126
	.quad	.LVL133
	.value	0x5
	.byte	0x91
	.sleb128 -18192
	.byte	0x9f
	.quad	.LVL134
	.quad	.LVL135
	.value	0x5
	.byte	0x91
	.sleb128 -18192
	.byte	0x9f
	.quad	0
	.quad	0
.LLST54:
	.quad	.LVL73
	.quad	.LVL110
	.value	0x4
	.byte	0x91
	.sleb128 -18312
	.quad	.LVL111
	.quad	.LVL113
	.value	0x4
	.byte	0x91
	.sleb128 -18312
	.quad	.LVL126
	.quad	.LVL133
	.value	0x4
	.byte	0x91
	.sleb128 -18312
	.quad	.LVL134
	.quad	.LVL135
	.value	0x4
	.byte	0x91
	.sleb128 -18312
	.quad	0
	.quad	0
.LLST55:
	.quad	.LVL75
	.quad	.LVL110
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL111
	.quad	.LVL113
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL126
	.quad	.LVL132
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL134
	.quad	.LVL135
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	0
	.quad	0
.LLST56:
	.quad	.LVL75
	.quad	.LVL110
	.value	0x5
	.byte	0x91
	.sleb128 -18184
	.byte	0x9f
	.quad	.LVL111
	.quad	.LVL113
	.value	0x5
	.byte	0x91
	.sleb128 -18184
	.byte	0x9f
	.quad	.LVL126
	.quad	.LVL132
	.value	0x5
	.byte	0x91
	.sleb128 -18184
	.byte	0x9f
	.quad	.LVL134
	.quad	.LVL135
	.value	0x5
	.byte	0x91
	.sleb128 -18184
	.byte	0x9f
	.quad	0
	.quad	0
.LLST57:
	.quad	.LVL75
	.quad	.LVL110
	.value	0x4
	.byte	0x91
	.sleb128 -18308
	.quad	.LVL111
	.quad	.LVL113
	.value	0x4
	.byte	0x91
	.sleb128 -18308
	.quad	.LVL126
	.quad	.LVL132
	.value	0x4
	.byte	0x91
	.sleb128 -18308
	.quad	.LVL134
	.quad	.LVL135
	.value	0x4
	.byte	0x91
	.sleb128 -18308
	.quad	0
	.quad	0
.LLST58:
	.quad	.LVL80
	.quad	.LVL110
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL111
	.quad	.LVL113
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL126
	.quad	.LVL131
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL134
	.quad	.LVL135
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	0
	.quad	0
.LLST59:
	.quad	.LVL80
	.quad	.LVL110
	.value	0x5
	.byte	0x91
	.sleb128 -18176
	.byte	0x9f
	.quad	.LVL111
	.quad	.LVL113
	.value	0x5
	.byte	0x91
	.sleb128 -18176
	.byte	0x9f
	.quad	.LVL126
	.quad	.LVL131
	.value	0x5
	.byte	0x91
	.sleb128 -18176
	.byte	0x9f
	.quad	.LVL134
	.quad	.LVL135
	.value	0x5
	.byte	0x91
	.sleb128 -18176
	.byte	0x9f
	.quad	0
	.quad	0
.LLST60:
	.quad	.LVL80
	.quad	.LVL105
	.value	0x1
	.byte	0x5d
	.quad	.LVL111
	.quad	.LVL113
	.value	0x1
	.byte	0x5d
	.quad	.LVL126
	.quad	.LVL131
	.value	0x1
	.byte	0x5d
	.quad	.LVL134
	.quad	.LVL135
	.value	0x1
	.byte	0x5d
	.quad	0
	.quad	0
.LLST61:
	.quad	.LVL82
	.quad	.LVL110
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL111
	.quad	.LVL113
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL126
	.quad	.LVL130
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL134
	.quad	.LVL135
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	0
	.quad	0
.LLST62:
	.quad	.LVL82
	.quad	.LVL110
	.value	0x5
	.byte	0x91
	.sleb128 -18168
	.byte	0x9f
	.quad	.LVL111
	.quad	.LVL113
	.value	0x5
	.byte	0x91
	.sleb128 -18168
	.byte	0x9f
	.quad	.LVL126
	.quad	.LVL130
	.value	0x5
	.byte	0x91
	.sleb128 -18168
	.byte	0x9f
	.quad	.LVL134
	.quad	.LVL135
	.value	0x5
	.byte	0x91
	.sleb128 -18168
	.byte	0x9f
	.quad	0
	.quad	0
.LLST63:
	.quad	.LVL82
	.quad	.LVL110
	.value	0x4
	.byte	0x91
	.sleb128 -18336
	.quad	.LVL111
	.quad	.LVL113
	.value	0x4
	.byte	0x91
	.sleb128 -18336
	.quad	.LVL126
	.quad	.LVL130
	.value	0x4
	.byte	0x91
	.sleb128 -18336
	.quad	.LVL134
	.quad	.LVL135
	.value	0x4
	.byte	0x91
	.sleb128 -18336
	.quad	0
	.quad	0
.LLST64:
	.quad	.LVL84
	.quad	.LVL110
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL111
	.quad	.LVL113
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL126
	.quad	.LVL129
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL134
	.quad	.LVL135
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	0
	.quad	0
.LLST65:
	.quad	.LVL84
	.quad	.LVL110
	.value	0x5
	.byte	0x91
	.sleb128 -18160
	.byte	0x9f
	.quad	.LVL111
	.quad	.LVL113
	.value	0x5
	.byte	0x91
	.sleb128 -18160
	.byte	0x9f
	.quad	.LVL126
	.quad	.LVL129
	.value	0x5
	.byte	0x91
	.sleb128 -18160
	.byte	0x9f
	.quad	.LVL134
	.quad	.LVL135
	.value	0x5
	.byte	0x91
	.sleb128 -18160
	.byte	0x9f
	.quad	0
	.quad	0
.LLST66:
	.quad	.LVL84
	.quad	.LVL110
	.value	0x4
	.byte	0x91
	.sleb128 -18328
	.quad	.LVL111
	.quad	.LVL113
	.value	0x4
	.byte	0x91
	.sleb128 -18328
	.quad	.LVL126
	.quad	.LVL129
	.value	0x4
	.byte	0x91
	.sleb128 -18328
	.quad	.LVL134
	.quad	.LVL135
	.value	0x4
	.byte	0x91
	.sleb128 -18328
	.quad	0
	.quad	0
.LLST67:
	.quad	.LVL86
	.quad	.LVL110
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL111
	.quad	.LVL113
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL126
	.quad	.LVL128
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL134
	.quad	.LVL135
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	0
	.quad	0
.LLST68:
	.quad	.LVL86
	.quad	.LVL110
	.value	0x5
	.byte	0x91
	.sleb128 -18152
	.byte	0x9f
	.quad	.LVL111
	.quad	.LVL113
	.value	0x5
	.byte	0x91
	.sleb128 -18152
	.byte	0x9f
	.quad	.LVL126
	.quad	.LVL128
	.value	0x5
	.byte	0x91
	.sleb128 -18152
	.byte	0x9f
	.quad	.LVL134
	.quad	.LVL135
	.value	0x5
	.byte	0x91
	.sleb128 -18152
	.byte	0x9f
	.quad	0
	.quad	0
.LLST69:
	.quad	.LVL86
	.quad	.LVL110
	.value	0x4
	.byte	0x91
	.sleb128 -18316
	.quad	.LVL111
	.quad	.LVL113
	.value	0x4
	.byte	0x91
	.sleb128 -18316
	.quad	.LVL126
	.quad	.LVL128
	.value	0x4
	.byte	0x91
	.sleb128 -18316
	.quad	.LVL134
	.quad	.LVL135
	.value	0x4
	.byte	0x91
	.sleb128 -18316
	.quad	0
	.quad	0
.LLST70:
	.quad	.LVL88
	.quad	.LVL110
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL111
	.quad	.LVL113
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL126
	.quad	.LVL127
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL134
	.quad	.LVL135
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	0
	.quad	0
.LLST71:
	.quad	.LVL88
	.quad	.LVL110
	.value	0x5
	.byte	0x91
	.sleb128 -18144
	.byte	0x9f
	.quad	.LVL111
	.quad	.LVL113
	.value	0x5
	.byte	0x91
	.sleb128 -18144
	.byte	0x9f
	.quad	.LVL126
	.quad	.LVL127
	.value	0x5
	.byte	0x91
	.sleb128 -18144
	.byte	0x9f
	.quad	.LVL134
	.quad	.LVL135
	.value	0x5
	.byte	0x91
	.sleb128 -18144
	.byte	0x9f
	.quad	0
	.quad	0
.LLST72:
	.quad	.LVL88
	.quad	.LVL110
	.value	0x4
	.byte	0x91
	.sleb128 -18312
	.quad	.LVL111
	.quad	.LVL113
	.value	0x4
	.byte	0x91
	.sleb128 -18312
	.quad	.LVL126
	.quad	.LVL127
	.value	0x4
	.byte	0x91
	.sleb128 -18312
	.quad	.LVL134
	.quad	.LVL135
	.value	0x4
	.byte	0x91
	.sleb128 -18312
	.quad	0
	.quad	0
.LLST73:
	.quad	.LVL90
	.quad	.LVL110
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL111
	.quad	.LVL113
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	.LVL134
	.quad	.LVL135
	.value	0x2
	.byte	0x38
	.byte	0x9f
	.quad	0
	.quad	0
.LLST74:
	.quad	.LVL90
	.quad	.LVL110
	.value	0x5
	.byte	0x91
	.sleb128 -18136
	.byte	0x9f
	.quad	.LVL111
	.quad	.LVL113
	.value	0x5
	.byte	0x91
	.sleb128 -18136
	.byte	0x9f
	.quad	.LVL134
	.quad	.LVL135
	.value	0x5
	.byte	0x91
	.sleb128 -18136
	.byte	0x9f
	.quad	0
	.quad	0
.LLST75:
	.quad	.LVL90
	.quad	.LVL110
	.value	0x4
	.byte	0x91
	.sleb128 -18308
	.quad	.LVL111
	.quad	.LVL113
	.value	0x4
	.byte	0x91
	.sleb128 -18308
	.quad	.LVL134
	.quad	.LVL135
	.value	0x4
	.byte	0x91
	.sleb128 -18308
	.quad	0
	.quad	0
.LLST76:
	.quad	.LVL94
	.quad	.LVL110
	.value	0xa
	.byte	0x3
	.quad	.LC12
	.byte	0x9f
	.quad	.LVL111
	.quad	.LVL113
	.value	0xa
	.byte	0x3
	.quad	.LC12
	.byte	0x9f
	.quad	0
	.quad	0
.LLST77:
	.quad	.LVL95
	.quad	.LVL110
	.value	0xa
	.byte	0x3
	.quad	.LC13
	.byte	0x9f
	.quad	.LVL111
	.quad	.LVL113
	.value	0xa
	.byte	0x3
	.quad	.LC13
	.byte	0x9f
	.quad	0
	.quad	0
.LLST78:
	.quad	.LVL96
	.quad	.LVL110
	.value	0xa
	.byte	0x3
	.quad	.LC14
	.byte	0x9f
	.quad	.LVL111
	.quad	.LVL113
	.value	0xa
	.byte	0x3
	.quad	.LC14
	.byte	0x9f
	.quad	0
	.quad	0
.LLST79:
	.quad	.LVL97
	.quad	.LVL110
	.value	0xa
	.byte	0x3
	.quad	.LC15
	.byte	0x9f
	.quad	.LVL111
	.quad	.LVL113
	.value	0xa
	.byte	0x3
	.quad	.LC15
	.byte	0x9f
	.quad	0
	.quad	0
.LLST80:
	.quad	.LVL98
	.quad	.LVL110
	.value	0xa
	.byte	0x3
	.quad	.LC16
	.byte	0x9f
	.quad	.LVL112
	.quad	.LVL113
	.value	0xa
	.byte	0x3
	.quad	.LC16
	.byte	0x9f
	.quad	0
	.quad	0
.LLST81:
	.quad	.LVL99
	.quad	.LVL110
	.value	0xa
	.byte	0x3
	.quad	.LC17
	.byte	0x9f
	.quad	.LVL112
	.quad	.LVL113
	.value	0xa
	.byte	0x3
	.quad	.LC17
	.byte	0x9f
	.quad	0
	.quad	0
.LLST82:
	.quad	.LVL100
	.quad	.LVL110
	.value	0xa
	.byte	0x3
	.quad	.LC19
	.byte	0x9f
	.quad	.LVL112
	.quad	.LVL113
	.value	0xa
	.byte	0x3
	.quad	.LC19
	.byte	0x9f
	.quad	0
	.quad	0
.LLST83:
	.quad	.LVL102
	.quad	.LVL110
	.value	0xa
	.byte	0x3
	.quad	.LC21
	.byte	0x9f
	.quad	0
	.quad	0
.LLST84:
	.quad	.LVL103
	.quad	.LVL110
	.value	0xa
	.byte	0x3
	.quad	.LC22
	.byte	0x9f
	.quad	0
	.quad	0
.LLST85:
	.quad	.LVL107
	.quad	.LVL109
	.value	0xa
	.byte	0x3
	.quad	.LC25
	.byte	0x9f
	.quad	0
	.quad	0
.LLST86:
	.quad	.LVL107
	.quad	.LVL108
	.value	0x1
	.byte	0x5c
	.quad	.LVL108
	.quad	.LVL109
	.value	0x4
	.byte	0x91
	.sleb128 -18336
	.quad	0
	.quad	0
	.section	.debug_aranges,"",@progbits
	.long	0x3c
	.value	0x2
	.long	.Ldebug_info0
	.byte	0x8
	.byte	0
	.value	0
	.value	0
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.quad	.LFB90
	.quad	.LFE90-.LFB90
	.quad	0
	.quad	0
	.section	.debug_ranges,"",@progbits
.Ldebug_ranges0:
	.quad	.LBB79
	.quad	.LBE79
	.quad	.LBB86
	.quad	.LBE86
	.quad	0
	.quad	0
	.quad	.LBB82
	.quad	.LBE82
	.quad	.LBB87
	.quad	.LBE87
	.quad	.LBB92
	.quad	.LBE92
	.quad	0
	.quad	0
	.quad	.LBB88
	.quad	.LBE88
	.quad	.LBB93
	.quad	.LBE93
	.quad	.LBB98
	.quad	.LBE98
	.quad	0
	.quad	0
	.quad	.LBB94
	.quad	.LBE94
	.quad	.LBB99
	.quad	.LBE99
	.quad	.LBB104
	.quad	.LBE104
	.quad	0
	.quad	0
	.quad	.LBB100
	.quad	.LBE100
	.quad	.LBB105
	.quad	.LBE105
	.quad	.LBB110
	.quad	.LBE110
	.quad	0
	.quad	0
	.quad	.LBB106
	.quad	.LBE106
	.quad	.LBB111
	.quad	.LBE111
	.quad	.LBB112
	.quad	.LBE112
	.quad	0
	.quad	0
	.quad	.LBB151
	.quad	.LBE151
	.quad	.LBB154
	.quad	.LBE154
	.quad	0
	.quad	0
	.quad	.LBB155
	.quad	.LBE155
	.quad	.LBB158
	.quad	.LBE158
	.quad	0
	.quad	0
	.quad	.Ltext0
	.quad	.Letext0
	.quad	.LFB90
	.quad	.LFE90
	.quad	0
	.quad	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF121:
	.string	"printf"
.LASF17:
	.string	"_IO_read_ptr"
.LASF142:
	.string	"iseed"
.LASF29:
	.string	"_chain"
.LASF109:
	.string	"mmap_data"
.LASF7:
	.string	"size_t"
.LASF82:
	.string	"wakeup_watermark"
.LASF35:
	.string	"_shortbuf"
.LASF96:
	.string	"exclusive"
.LASF4:
	.string	"signed char"
.LASF50:
	.string	"ssize_t"
.LASF102:
	.string	"comm"
.LASF88:
	.string	"type"
.LASF77:
	.string	"PERF_COUNT_HW_STALLED_CYCLES_BACKEND"
.LASF23:
	.string	"_IO_buf_base"
.LASF92:
	.string	"read_format"
.LASF107:
	.string	"watermark"
.LASF72:
	.string	"PERF_COUNT_HW_CACHE_MISSES"
.LASF56:
	.string	"long long unsigned int"
.LASF80:
	.string	"sample_freq"
.LASF78:
	.string	"PERF_COUNT_HW_MAX"
.LASF73:
	.string	"PERF_COUNT_HW_BRANCH_INSTRUCTIONS"
.LASF100:
	.string	"exclude_idle"
.LASF143:
	.string	"__PRETTY_FUNCTION__"
.LASF131:
	.string	"selectivity"
.LASF30:
	.string	"_fileno"
.LASF81:
	.string	"wakeup_events"
.LASF18:
	.string	"_IO_read_end"
.LASF150:
	.string	"_IO_lock_t"
.LASF97:
	.string	"exclude_user"
.LASF6:
	.string	"long int"
.LASF16:
	.string	"_flags"
.LASF63:
	.string	"PERF_TYPE_HW_CACHE"
.LASF13:
	.string	"__ssize_t"
.LASF130:
	.string	"createData"
.LASF24:
	.string	"_IO_buf_end"
.LASF33:
	.string	"_cur_column"
.LASF91:
	.string	"sample_type"
.LASF101:
	.string	"mmap"
.LASF117:
	.string	"double"
.LASF67:
	.string	"perf_type_id"
.LASF62:
	.string	"PERF_TYPE_TRACEPOINT"
.LASF37:
	.string	"_offset"
.LASF129:
	.string	"perf_event_open"
.LASF70:
	.string	"PERF_COUNT_HW_INSTRUCTIONS"
.LASF38:
	.string	"__pad1"
.LASF47:
	.string	"_next"
.LASF66:
	.string	"PERF_TYPE_MAX"
.LASF125:
	.string	"__nbytes"
.LASF51:
	.string	"long long int"
.LASF146:
	.string	"attr"
.LASF46:
	.string	"_IO_marker"
.LASF144:
	.string	"stdin"
.LASF113:
	.string	"__reserved_1"
.LASF3:
	.string	"unsigned int"
.LASF103:
	.string	"freq"
.LASF85:
	.string	"bp_len"
.LASF126:
	.string	"fprintf"
.LASF93:
	.string	"disabled"
.LASF94:
	.string	"inherit"
.LASF127:
	.string	"__stream"
.LASF0:
	.string	"long unsigned int"
.LASF58:
	.string	"__u32"
.LASF12:
	.string	"__suseconds_t"
.LASF21:
	.string	"_IO_write_ptr"
.LASF60:
	.string	"PERF_TYPE_HARDWARE"
.LASF99:
	.string	"exclude_hv"
.LASF48:
	.string	"_sbuf"
.LASF132:
	.string	"data"
.LASF89:
	.string	"size"
.LASF2:
	.string	"short unsigned int"
.LASF64:
	.string	"PERF_TYPE_RAW"
.LASF25:
	.string	"_IO_save_base"
.LASF36:
	.string	"_lock"
.LASF114:
	.string	"bp_type"
.LASF31:
	.string	"_flags2"
.LASF53:
	.string	"timeval"
.LASF145:
	.string	"stdout"
.LASF123:
	.string	"__fd"
.LASF116:
	.string	"start_time"
.LASF139:
	.string	"answer"
.LASF149:
	.string	"/home/fredrabelo/4112_project2/branch_mispred6"
.LASF54:
	.string	"tv_sec"
.LASF98:
	.string	"exclude_kernel"
.LASF22:
	.string	"_IO_write_end"
.LASF57:
	.string	"uint64_t"
.LASF61:
	.string	"PERF_TYPE_SOFTWARE"
.LASF79:
	.string	"sample_period"
.LASF140:
	.string	"val1"
.LASF141:
	.string	"val2"
.LASF45:
	.string	"_IO_FILE"
.LASF69:
	.string	"PERF_COUNT_HW_CPU_CYCLES"
.LASF124:
	.string	"__buf"
.LASF120:
	.string	"__nptr"
.LASF43:
	.string	"_mode"
.LASF133:
	.string	"createOffsets"
.LASF49:
	.string	"_pos"
.LASF32:
	.string	"_old_offset"
.LASF74:
	.string	"PERF_COUNT_HW_BRANCH_MISSES"
.LASF28:
	.string	"_markers"
.LASF110:
	.string	"sample_id_all"
.LASF119:
	.string	"atof"
.LASF83:
	.string	"bp_addr"
.LASF1:
	.string	"unsigned char"
.LASF71:
	.string	"PERF_COUNT_HW_CACHE_REFERENCES"
.LASF87:
	.string	"perf_event_attr"
.LASF10:
	.string	"__pid_t"
.LASF5:
	.string	"short int"
.LASF34:
	.string	"_vtable_offset"
.LASF15:
	.string	"FILE"
.LASF138:
	.string	"stop"
.LASF112:
	.string	"exclude_guest"
.LASF8:
	.string	"__off_t"
.LASF55:
	.string	"tv_usec"
.LASF147:
	.string	"GNU C 4.6.3"
.LASF59:
	.string	"__u64"
.LASF14:
	.string	"char"
.LASF90:
	.string	"config"
.LASF76:
	.string	"PERF_COUNT_HW_STALLED_CYCLES_FRONTEND"
.LASF84:
	.string	"config1"
.LASF86:
	.string	"config2"
.LASF68:
	.string	"perf_hw_id"
.LASF95:
	.string	"pinned"
.LASF9:
	.string	"__off64_t"
.LASF106:
	.string	"task"
.LASF19:
	.string	"_IO_read_base"
.LASF27:
	.string	"_IO_save_end"
.LASF115:
	.string	"__fmt"
.LASF118:
	.string	"get_timestamp"
.LASF39:
	.string	"__pad2"
.LASF40:
	.string	"__pad3"
.LASF41:
	.string	"__pad4"
.LASF42:
	.string	"__pad5"
.LASF11:
	.string	"__time_t"
.LASF108:
	.string	"precise_ip"
.LASF44:
	.string	"_unused2"
.LASF136:
	.string	"argv"
.LASF65:
	.string	"PERF_TYPE_BREAKPOINT"
.LASF148:
	.string	"branch_mispred.c"
.LASF26:
	.string	"_IO_backup_base"
.LASF128:
	.string	"flags"
.LASF75:
	.string	"PERF_COUNT_HW_BUS_CYCLES"
.LASF122:
	.string	"read"
.LASF135:
	.string	"argc"
.LASF52:
	.string	"pid_t"
.LASF137:
	.string	"start"
.LASF134:
	.string	"main"
.LASF20:
	.string	"_IO_write_base"
.LASF104:
	.string	"inherit_stat"
.LASF111:
	.string	"exclude_host"
.LASF105:
	.string	"enable_on_exec"
	.ident	"GCC: (Ubuntu/Linaro 4.6.3-1ubuntu5) 4.6.3"
	.section	.note.GNU-stack,"",@progbits
