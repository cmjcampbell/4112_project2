#! /usr/bin/python

import random

NUM_TERMS = 6

def main():

	random.seed();

	lists = [[(random.randrange(9) * 1.) / 10] for x in range(6)]

	# +- 0.0	
	num = 0;
	while len(lists[num]) < NUM_TERMS:
		lists[num].append(lists[num][0])
	# +- 0.1
	num = 1;
	while len(lists[num]) < NUM_TERMS:
		curr_int = random.randrange(9)
		curr_float = curr_int / 10.
		if ((int(lists[num][0] * 10)  - num) <= curr_int and 
				curr_int <= (int(lists[num][0] * 10))):
			lists[num].append(curr_float)

	# +- 0.2
	num = 2;
	while len(lists[num]) < NUM_TERMS:
		curr_int = random.randrange(9)
		curr_float = curr_int / 10.
                if ((int(lists[num][0] * 10)  - num) <= curr_int and
				curr_int <= (int(lists[num][0] * 10))):
			lists[num].append(curr_float)

	# +- 0.3
	num = 3;
	while len(lists[num]) < NUM_TERMS:
		curr_int = random.randrange(9)
		curr_float = curr_int / 10.
                if ((int(lists[num][0] * 10)  - num) <= curr_int and
			curr_int <= (int(lists[num][0] * 10))):
			lists[num].append(curr_float)

	# +- 0.4
	num = 4;
	while len(lists[num]) < NUM_TERMS:
		curr_int = random.randrange(9)
		curr_float = curr_int / 10.
                if ((int(lists[num][0] * 10)  - num) <= curr_int and
			curr_int <= (int(lists[num][0] * 10))):
			lists[num].append(curr_float)

	# +- 0.5
	num = 5;
	while len(lists[num]) < NUM_TERMS:
		curr_int = random.randrange(9)
		curr_float = curr_int / 10.
                if ((int(lists[num][0] * 10)  - num) <= curr_int and 
			curr_int <= (int(lists[num][0]     * 10))):
			lists[num].append(curr_float)

	for curr_list in lists:
		curr_len = len(curr_list)
		for i in range(curr_len - 1):
			print str(curr_list[i]) + " ",
		print str(curr_list[i+1])

if __name__ == '__main__':
	main()
