CC = gcc
DEBUG = -g
CFLAGS = -Wall -Wshadow -Wunreachable-code -Wredundant-decls -Wmissing-declarations -Wold-style-definition -Wmissing-prototypes -Wdeclaration-after-statement
PROG = optimizer
SRC = $(PROG).c
OBJ = $(PROG).o

all: $(PROG)

optimizer: optimizer.o
	$(CC) -o $(PROG) $(OBJ) -lm 

optimizer.o: optimizer.h optimizer.c
	$(CC) $(CFLAGS) $(DEBUG) -c $(SRC)

test:
	# insert tests here
clean:
	rm -f $(PROG) $(OBJ) $(SRC)~
