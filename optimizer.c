#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <math.h>
#include <limits.h>
#include "optimizer.h"

#define BITS_PER_WORD sizeof(int) * CHAR_BIT
#define WORD_OFFSET(b) ((b) / BITS_PER_WORD)
#define BIT_OFFSET(b) ((b) % BITS_PER_WORD)
#define GET_BIT(X,Y) ((X >> Y) & 1)

static int num_cases = 0;
static int curr_case = 0;
static struct case_info **cases = NULL;

COST_INFO(cost);

int main(int argc, const char *argv[])
{
	int i;
	FILE *query_file, *config_file;

	// Checks command line arguments
	if (argv[1] == NULL || argv[2] == NULL) {
		printf("optimizer: illegal option\nusage: \
			optimizer [query file] [config file]\n");
		exit(EXIT_FAILURE);
	}

	// Opens files
	query_file = fopen(argv[1], "r");
	if (query_file == NULL) {
		perror("Unable to open query file");
		exit(EXIT_FAILURE);
	}
	config_file = fopen(argv[2], "r");
	if (config_file == NULL) {
		perror("Unable to open config file");
		exit(EXIT_FAILURE);
	}

	// Parses files
	parse_config_file(config_file);
	parse_query_file(query_file);

	// Closes files
	fclose(config_file);
	fclose(query_file);

	// Get plan for each case
	for (i = 0; i < num_cases; i++) {
		get_plan();
		curr_case++;
	}

	free_cases();

	return 0;
}

/*
 * Parses config file
 * 
 * Params:
 * 	config_file: pointer to the config FILE
 * 
 * Returns:
 * 	None
 * 
 * Pre:
 * 	config_file is null
 * 
 * Post:
 *	the cost struct has been populated
 */
void parse_config_file(FILE * config_file)
{
	fscanf(config_file, "r = %d\nt = %d\nl = %d\nm = %d\na = %d\nf = %d",
	       &cost.r, &cost.t, &cost.l, &cost.m, &cost.a, &cost.f);
}

/*
 * Parses query file
 * 
 * Params:
 *	query_file: pointer to the query FILE
 * 
 * Returns:
 *	None
 * 
 * Pre:
 *	query_file is not null
 * 
 * Post: 
 * 	the cases struct has been populated
 */
void parse_query_file(FILE * query_file)
{
	int i, num_terms, curr_val;
	char *token;
	struct case_info *new_case, **new_cases;

	int more_lines = 0;
	char delim = ' ', *line = NULL;
	size_t line_cap = 0;

	/* Allocates initial buffer for cases */
	cases = (struct case_info **)malloc(sizeof(struct case_info *));
	if (cases == NULL) {
		perror("Unable to allocate cases");
		exit(EXIT_FAILURE);
	}
	memset(cases, '\0', sizeof(struct case_info *));

	// Parse each line (case) of the query file
	if (getline(&line, &line_cap, query_file) > 0)
		more_lines = 1;
	while (more_lines) {

		// Count number of terms in case
		i = 0;
		num_terms = 1;
		while (line[i] != '\0') {
			if (line[i++] == ' ')
				num_terms++;
		}

		// Allocate and initialize case_info struct
		new_case = (struct case_info *)malloc(sizeof(struct case_info));
		if (cases == NULL) {
			perror("Unable to allocate curr_case");
			exit(EXIT_FAILURE);
		}
		memset(new_case, '\0', sizeof(struct case_info));
		new_case->p_vals = (float *)malloc(num_terms * sizeof(float));
		if (cases == NULL) {
			perror("Unable to allocate new_case->p_vals");
			exit(EXIT_FAILURE);
		}
		memset(new_case->p_vals, '\0', num_terms * sizeof(float));
		new_case->num_terms = num_terms;
		cases[num_cases++] = new_case;

		// Parse case for selectivities
		curr_val = 0;
		token = strtok(line, &delim);
		while (token != NULL) {
			sscanf(token, "%f", &new_case->p_vals[curr_val++]);
			token = strtok(NULL, &delim);
		}

		// Determine if there is another line
		line = NULL;
		line_cap = 0;
		if (getline(&line, &line_cap, query_file) > 0) {
			// Expand size of cases
			new_cases = realloc(cases, num_cases * sizeof(*cases) + \
							sizeof(struct case_info *));
			if (new_cases == NULL) {
				printf("error reallocating cases\n");
				free_cases();
				exit(EXIT_FAILURE);
			} else {
				cases = new_cases;
			}
		} else {
			more_lines = 0;
		}
	}
}

/*
 * Get the plan for the current case
 * 
 * Params:
 * 	None
 * 
 * Returns:
 *	None
 * 
 * Pre:
 * 	None
 * 
 * Post: 
 * 	the plan was output to stdout
 */
void get_plan(void)
{
	int num_subsets = (int)pow(2.0, cases[curr_case]->num_terms);
	struct subset	subsets[num_subsets];

	// Generate &-plans
	and_plans(subsets, num_subsets);

	// Generate combined plans
	combined_plans(subsets, num_subsets);	

	// Derive optimal cost plan
	derive_optimal_plan(subsets, num_subsets, cases[curr_case]->num_terms);
}


/*
 * Generate all &-plans
 * 
 * Params:
 *	subsets: pointer to the subsets array
 * 	num_subsets: the number of subsets
 * 
 * Returns:
 *	None
 * 
 * Pre:
 *	subsets is not null
 * 	num_subsets is valid
 * 
 * Post:
 *	subsets has been modified according to &-plans
 */
void and_plans(struct subset subsets[], int num_subsets)
{
	int i, j;
	struct subset *curr_subset;

	int num_terms = cases[curr_case]->num_terms;

	// For all subsets
	for (i = 1; i < num_subsets; i++) {

		curr_subset = &subsets[i];
		init_subset(curr_subset);
		
		// Determine the members of the subset
		for (j = 0; j < num_terms; j++) {

			// If true, term j is a memeber of the subset
			if (GET_BIT(i,j)) {

				// Update n and p
				curr_subset->n++;
				curr_subset->p *= cases[curr_case]->p_vals[j];
			}
		}

		// Update c
		and_cost(curr_subset);
	}
}

/*
 * Calculates the cost of a logical plan
 * 
 * Params:
 *	subset: pointer to the subset
 * 
 * Returns:
 *	None
 * 
 * Pre:
 * 	subset is not null
 * 
 * Post: 
 * 	the subset's best cost is set
 */
void and_cost(struct subset *sub)
{
	int q;

	int k = sub->n;
	float branch_cost = 0, no_branch_cost = 0;

	// Determines q
	q = (sub->p < (1 - sub->p)) ? sub->p : (1 - sub->p);

	// Determines branch cost
	branch_cost = (k * cost.r + (k - 1) * cost.l + \
			k * cost.f + cost.t + \
			cost.m * q + sub->p * cost.a);

	// Use branch cost
	sub->c = branch_cost;

	// Determines no bracnh cost
	no_branch_cost = (k * cost.r + (k - 1) * cost.l + \
			  k * cost.f + cost.a);

	// If no branch cost is cheaper, use no branch cost
	if (no_branch_cost < branch_cost) {
		sub->c = no_branch_cost;
		sub->b = 1;
	}

	
}

/*
 * Generate combined plans (generate &&-plans using &-plans)
 *
 * Params:
 * 	subsets: a pointer to the subsets array
 *	num_subsets: the number of subsets
 *
 * Returns
 *	None
 *
 * Pre:
 * 	subets is not null
 * 	num_subsets is valid
 *
 * Post:
 * 	subsets have been modified according to combined plans
 */
void combined_plans(struct subset subsets[], int num_subsets)
{
	int sL_idx, sR_idx;
	float new_cost;
	struct subset *sL, *sR;

	// For each subset
	for (sR_idx = 1; sR_idx < num_subsets; sR_idx++) {

		sR = &subsets[sR_idx];

		// For each subet
		for (sL_idx = 1; sL_idx < num_subsets; sL_idx++) {
			sL = &subsets[sL_idx]; 

			// If sL n sR is empty
			if(!(sL_idx & sR_idx)) {

			// If the c-metric and d-metric of sL aren't dominated
			if (c_metric_test(sL, sR) && d_metric_test(sL, sR)) {

				// Calculate the cost of the combined plan 
				new_cost = combined_cost(sL, sR);

				// Update sL U sR if combined plan is cheaper
				replace_subset_union(subsets, sL, sR, sL_idx,
							sR_idx, new_cost);
				}

			}
		}
	}
}

/*
 * Calculates the cost of a combined plan
 * 
 * Params:
 *	sL: a pointer to the left child of the plan
 * 	sR: a pointer to the right child of the plan
 * 
 * Returns:
 *	the cost of the combined plan
 * 
 * Pre:
 *	sL is not null
 * 	sR is not null
 * 
 * Post:
 * 	None
 */
float combined_cost(struct subset *sL, struct subset *sR)
{
	float q;

	float f_cost, total_cost = 0;

	f_cost = fcost(sL);

	// Determines q
	q = (sL->p < (1 - sL->p)) ? sL->p : (1 - sL->p);

	// Determines total cost 
	total_cost = f_cost + cost.m * q + sL->p * sR->c;

	return total_cost;
}

/*
 * Compares two subsets using the c-metric
 * 
 * Params: 
 * 	sub1: the left subset 
 * 	sub2: the right subset
 * 
 * Returns:
 * 	0 if the c-metric of sub1 is dominated by the
 * 	c-metric of the leftmost &-term in sub2, 1 otherwise
 * 
 * Pre:
 * 	sub1 is not null
 * 	sub2 is not null
 *
 * Post:
 * 	None
 */
int c_metric_test(struct subset *sub1, struct subset *sub2)
{
	float p1, p2, metric1, metric2;
	struct subset *sub2_lm_term;

	// Get leftmost &-term of sub2
	sub2_lm_term = leftmost_child(sub2);

	p1 = sub1->p;
	p2 = sub2_lm_term->p;

	// Carry out c-metric test between sub1 and the leftmost &-term of sub2
	if (p2 <= p1) {
		metric1 = (p1 - 1) / fcost(sub1);
		metric2 = (p2 - 1) / fcost(sub2);

		if (metric2 < metric1)
			return 0;
	}

	return 1;
}

/*
 * Finds the leftmost child (&-term) of a subset
 *
 * Params:
 * 	parent_sub: a pointer to the parent subset
 *
 * Returns:
 * 	a pointer to the leftmost child of parent_sub
 *
 * Pre:
 * 	parent_sub is not null
 *
 * Post:
 * 	None
 */
struct subset *leftmost_child(struct subset *parent_sub)
{
	struct subset *curr_sub = parent_sub;

	while(curr_sub->L != NULL) {
		curr_sub = curr_sub->L;
	}

	return curr_sub;
}

/*
 * Compares two subsets using the d-metric
 * 
 * Params:
 * 	sub1: the left subset
 *	sub2: the right subset
 * 
 * Returns: 
 *	0 if the d-metric of sub1 is dominated by the
 *	d-metric of any other term in sub2, 1 otherwise
 * 
 * Pre: 
 * 	sub1 is not null
 * 	sub2 is not null
 * 
 * Post:
 * 	None
 */
int d_metric_test(struct subset *sub1, struct subset *sub2)
{
	int i, and_term_idx;
	float p1, p2, metric1, metric2;
	struct subset *sub2_and_term;

	struct subset *and_terms[(int)(pow(2.0, \
		cases[curr_case]->num_terms)-1.)];

	p1 = sub1->p;

	if (p1 > 0.5)
		return 1;
	
	// Get &-terms of sub2
	and_term_idx = 0;
	subset_dfs(sub2, and_terms, &and_term_idx);

	// Carry out d_metric_test between sub1 and &-terms of sub2
	for (i = 0; i < and_term_idx; i++) {

		sub2_and_term = and_terms[i];
		p2 = sub2_and_term->p;

		if (p2 < p1) {

			metric1 = fcost(sub1);
			metric2 = fcost(sub2_and_term);

			if (metric2 < metric1)
				return 0;
		}
	}

	return 1;
}

/*
 * Uses DFS to find all leaf nodes (&-terms) of a subset
 *
 * Params:
 * 	curr_subset: a pointer to the subset
 * 	and_terms: a pointer to an array to store pointers to the &-terms
 * 	and_term_idx: keeps track of the next empty slot in and_terms
 * 
 * Returns:
 * 	None
 *
 * Pre:
 * 	curr_subset is not null
 * 	and_terms is not null
 * 	and_term_idx is not null
 *
 * Post:
 * 	and_terms is populated with pointers to
 * 	all of the &-terms of the original curr_subet
 *
 */
void subset_dfs(struct subset *curr_subset, struct subset *and_terms[], int *and_term_idx)
{
	// We are at a leaf node
	if (curr_subset->L == NULL && curr_subset->R == NULL) {
		and_terms[(*and_term_idx)++] = curr_subset;
		return;
	}

	subset_dfs(curr_subset->L, and_terms, and_term_idx);
	subset_dfs(curr_subset->R, and_terms, and_term_idx);
}

/*
 * Calculates the fcost of a subset (&-term)
 *
 * Params:
 * 	sub: the subset associated with the &-term
 *
 * Returns:
 * 	the fcost of the given &-term
 *
 * Pre:
 * 	sub is not null
 *
 * Post:
 * 	None
 */
float fcost(struct subset *sub)
{
	return  sub->n * cost.r + (sub->n - 1) * cost.l + sub->n * \
							cost.f + cost.t;
}

/*
 * Frees the memory associated with cases
 * 
 * Params:
 * 	None
 * 
 * Returns: 
 * 	None
 * 
 * Pre:
 *	cases has been allocated
 * 	num_cases is valid
 * 
 * Post:
 *	cases has been freed
 */
void free_cases(void)
{
	int i = 0;

	while (i < num_cases) {
		free(cases[i++]);
	}

	free(cases);
}


/*
 *
 * Initializes a subset struct
 *
 * Params:
 * 	sub: a pointer to the subset struct
 *
 * Returns:
 * 	None
 *
 * Pre:
 * 	sub is not null
 *
 * Post:
 * 	the subsets has been initialized with the following values:
 *	n = 0, b = 0, p = 1, c = 0, L = NULL, R = NULL
 */
void init_subset(struct subset *sub)
{
	sub->n = 0;
	sub->b = 0;
	sub->p = 1;
	sub->c = 0;;
	sub->L = NULL;
	sub->R = NULL;
}

/*
 * Determines if an &&-plan of two subsets is cheaper than their union (&-plan)
 * (if the &&-plan is cheaper, it replaces the union)
 *
 * Parms:
 * 	subsets: a pointer to the subsets array
 * 	sL: the left subset
 * 	sR: the right subset
 * 	sL_idx: the index of the left subset
 * 	sR_idx: the index of the right subset
 * 	new_cost: the cost of sL && sR
 *
 * Returns:
 * 	None
 *
 * Pre:
 * 	subsets is not null
 * 	sL is not null
 * 	sR is not null
 * 	sL_idx is valid
 * 	sR_idx is valid 
 * 	new_cost is valid
 *
 * Post:
 * 	if the &&-plan is cheaper than the union,
 * 		A[s′ ∪ s].c = new_cost
 * 		A[s′ ∪ s].L = sL
 * 		A[s′ ∪ s].R = sR
 * 	else
 *		None
 */
void replace_subset_union(struct subset subsets[], struct subset *sL,
		struct subset *sR, int sL_idx, int sR_idx, float new_cost)
{
	struct subset *sub_union;

	// Get sL U sR
	sub_union = &subsets[(sL_idx | sR_idx)];

	// If new cost is less than sL U sR cost
	if(new_cost < sub_union->c) {
		sub_union->c = new_cost;
		sub_union->L = sL;
		sub_union->R = sR;
	}	
}
/*
 * Gets the index (in the subsets array) of a given subset
 *
 * Params:
 * 	subsets: a pointer to the subsets array
 * 	num_subsets: the number of subsets
 * 	sub: the subset
 *	
 * Returns:
 *     None
 *
 * Pre:
 * 	num_subsets is valid
 * 	num_subsets is valid
 *	sub is not null
 *	
 * Post:
 * 	None
 */
int get_sub_idx(struct subset subsets[], int num_subsets, struct subset *sub)
{
	int i = 0;
	
	while(&subsets[i] != sub && i < num_subsets) {
 		i++;
	}

 	return i;
}

/*
 * Derives optimal cost plan and prints associated c code
 *
 * Params:
 * 	subsets: a pointer to the subsets array
 *	num_subsets: the number of subsets
 *	num_terms: the number of basic terms
 *
 * Returns:
 *	None
 *
 * Pre:
 * 	num_subsets is valid
 *	num_terms is valid
 *
 * Post:
 * 	c code for the optimal plan has been output to stdout
 */
void derive_optimal_plan(struct subset subsets[], int num_subsets, int num_terms)
{
	int i = 0;
	int j = 0;
	struct subset *curr = &subsets[num_subsets - 1];
	float optimal_cost = curr->c;
	int nothing_printed_in_if = 1;
	int need_to_close_parenthesis = 0;

	printf("==================================================================\n");
	while(j < cases[curr_case]->num_terms) {
    		printf("%.2f ",cases[curr_case]->p_vals[j++]);
	}
	printf("\n------------------------------------------------------------------\n");

	printf("if(");

	i = 0;
	// While we have not gone past the rightmost node
	while(curr != NULL) {
		i++;
		// If we are at the rightmost node
		if (curr->R == NULL) {
			// If the no branch bit is set
			if (curr->b) {

				if(nothing_printed_in_if)
					printf("1");

				while(need_to_close_parenthesis)
				{
					need_to_close_parenthesis--;
					printf(")");
				}
				printf(") {\n");
				printf("\tanswer[j] = i;\n");
				printf("\tj += (");
				print_terms(get_sub_idx(subsets,
					num_subsets, curr),num_terms);
			 	printf(");\n");
			} else {
				nothing_printed_in_if = 0;
			 	//printf(" & ");
				print_terms(get_sub_idx(subsets,
					num_subsets, curr),num_terms);
				while(need_to_close_parenthesis)
				{
					need_to_close_parenthesis--;
					printf(")");
				}
				printf(") {\n");
				printf("\tanswer[j++] = i;\n");
			}
		} else {
			nothing_printed_in_if = 0;
			print_terms(get_sub_idx(subsets, num_subsets, curr->L),num_terms);
			
			// Concatenates left child (&-term) with right child (arbitrary term)
			// (if last term is no branch, do not concatenate)
			if (curr->R->R != NULL) {
				printf(" && (");
					need_to_close_parenthesis++;
        		} else if (curr->R != NULL && curr->R->b != 1) {
				printf(" && ");
			}
		}
       
		curr = curr->R;
    }

	printf("}\n");
	printf("------------------------------------------------------------------\n");
	printf("cost: %.2f\n", optimal_cost);
	printf("==================================================================\n");
}

/*
 * Prints c code for a term (basic term or &-term)
 *
 * Params:
 *      idx: the index of the current subset (term)
 *      num_terms: the number of basic terms (for the current case)
 *
 * Returns:
 *      None
 *
 * Pre:
 *      idx is valid
 *      num_terms is valid
 *
 * Post:
 *      c code for the term has been output to stdout
 */ 
void print_terms(int idx, int num_terms)
{
	int i;
	
	int printedbefore = 0; 
	int num_to_print = 0;

	for(i = 0 ; i < num_terms; i++) {
		if(GET_BIT(idx,i))
			num_to_print++;
	}

	if(num_to_print>1)
		printf("(");

	// For each term
	for(i = 0 ; i < num_terms; i++) {
		 // If bit corresponding to term is set
		if(GET_BIT(idx,i)) {
			// if printed before, concatenate & for next print
			if(printedbefore) {
				printf(" & t%d[o%d[i]]",i+1,i+1);
			} else {
				printf("t%d[o%d[i]]",i+1,i+1);
				printedbefore = 1;
			}
        	}
    	}

	if(num_to_print>1)
		printf(")");
}

/********************************* DEBUGGING FUNCTIONS ********************* *************/

/* Prints cases */
void print_cases(void)
{
	int i = 0;
	int j = 0;

	printf("* DEBUG (PRINT CASES) *\n");

	printf("num_cases: %d\n\n", num_cases);

	while (i < num_cases) {
		printf("case %d\n", i);
		printf("num_terms: %d\n", cases[i]->num_terms);
		printf("p_vals: ");

		j = 0;
		while (j < cases[i]->num_terms) {
			printf("%f ", cases[i]->p_vals[j++]);
		}
		i++;
		printf("\n\n");
	}
}

/* Print subsets */
void print_subsets(struct subset subsets[], int num_subsets)
{
	int i;

	printf("* DEBUG (PRINT SUBSETS) *\n");

	for (i = 0; i < num_subsets; i++) {
		print_subset(&subsets[i]);
	}
}

/* Print subset */
void print_subset(struct subset *sub)
{
	printf("n: %d\n", sub->n);
	printf("b: %d\n", sub->b);
	printf("p: %f\n", sub->p);
	printf("c: %f\n", sub->c);

	printf("l_child: ");
	if (sub->L != NULL) {
		printf("L->p:  %f\n", sub->L->p);
	} else {
		printf("NULL\n");
	}

	printf("r_child: ");
	if (sub->R != NULL) {
		printf("R->p: %f\n\n", sub->R->p);
	} else {
		printf("NULL\n\n");
	}
}
